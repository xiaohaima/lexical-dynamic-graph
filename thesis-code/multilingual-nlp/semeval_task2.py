import numpy as np
import utils
import rundata as rd
from scipy.spatial import distance
from cloudprior import CloudCluster, get_cloud_data, neighbor_similarity


# class CloudCluster:
#     def __init__(self, cpoint, nbs, ids, k=0, sim2c=None):
#         self.k = k
#         self.point_num = len(ids)
#         self.points = ids
#         self.center = cpoint
#         self.c_neighbors = nbs
#         self.sim2c = sim2c


def evaluate_semeval_task2(target_results: dict, language: str):
    targets = './corpus/{}/semeval_task/targets.txt'.format(language)
    with open(targets, "r", encoding="utf-8") as targets_data:
        targets_words = targets_data.read().split('\n')
    import semeval_official as seo
    # truth_path = "./corpus/semeval2020_ulscd_eng/truth/binary.txt"
    # pred_path = "./corpus/en/semevalSC_r/semantic_change_results.txt"
    # target_results = utils.load_from_disk('./corpus/en/semevalSC_r/semantic_change_results')
    truth_path = './corpus/{}/semeval_task/graded.txt'.format(language)
    pred_path = './corpus/{}/semeval_task/semantic_change_graded.txt'.format(language)
    results_str = ""
    for t_word in target_results:
        save_word = ""
        for targets_word in targets_words:
            if t_word in targets_word:
                save_word = targets_word
                break
        results_str = results_str + save_word + "\t" + str(target_results[t_word]) + "\n"
    res_file = open(pred_path, 'w', encoding='utf-8')
    res_file.write(results_str)
    res_file.close()
    acc = seo.spearman_official(truth_path, pred_path)
    print(" Sem eval task 2 spearman correlation = ", acc)
    return acc


def sc_grade(hf_semantic1, hf_semantic2, hf_sims, static_embeds):
    sm1 = [cl.center for cl in hf_semantic1]
    sm2 = [cl.center for cl in hf_semantic2]
    # if keyword == 'bit':
    #     print("sm1 shape", len(sm1))
    #     print("sm2 shape", len(sm2))
    avg1 = np.mean(np.array(sm1), axis=0)
    avg2 = np.mean(np.array(sm2), axis=0)
    grade_emb = 1 - utils.cosine_similarity(avg1, avg2)

    index_voc, voc_index, embeddings = static_embeds
    # sm1 = [embeddings[voc_index[nb]] for cl in hf_semantic1 for nb in cl.c_neighbors]
    # sm2 = [embeddings[voc_index[nb]] for cl in hf_semantic2 for nb in cl.c_neighbors]
    sm1 = []
    for cl in hf_semantic1:
        ssm_nbs = [embeddings[voc_index[nb]] for nb in cl.c_neighbors]
        ssm_avg = np.mean(np.array(ssm_nbs), axis=0)
        sm1.append(ssm_avg)
    sm2 = []
    for cl in hf_semantic2:
        ssm_nbs = [embeddings[voc_index[nb]] for nb in cl.c_neighbors]
        ssm_avg = np.mean(np.array(ssm_nbs), axis=0)
        sm2.append(ssm_avg)
    avg1 = np.mean(np.array(sm1), axis=0)
    avg2 = np.mean(np.array(sm2), axis=0)
    grade_nb = 1 - utils.cosine_similarity(avg1, avg2)

    grade_nb_hfs = [np.max(hf_sims[i,:]) for i in range(hf_sims.shape[0])]
    grade_nb_hfs += [np.max(hf_sims[:,i]) for i in range(hf_sims.shape[1])]
    grade_nb_hf = 1 - np.mean(grade_nb_hfs)
    return grade_nb, grade_emb, grade_nb_hf


def load_clusters(language, rt, sct, k, word):
    hf_path1 = './data/{}/words/{}/cloud_sim_matrix/sct{}/rt{}/k{}_cloud_hfsc_t{}.pkl'. \
        format(language, word, sct, rt, k, 1)
    lf_path1 = './data/{}/words/{}/cloud_sim_matrix/sct{}/rt{}/k{}_cloud_lfsc_t{}.pkl'. \
        format(language, word, sct, rt, k, 1)
    hf_path2 = './data/{}/words/{}/cloud_sim_matrix/sct{}/rt{}/k{}_cloud_hfsc_t{}.pkl'. \
        format(language, word, sct, rt, k, 2)
    lf_path2 = './data/{}/words/{}/cloud_sim_matrix/sct{}/rt{}/k{}_cloud_lfsc_t{}.pkl'. \
        format(language, word, sct, rt, k, 2)
    rt_clusters_pth1 = './data/{}/words/{}/cloud_sim_matrix/rt{}/k{}_rt_clusters_t{}.pkl'. \
        format(language, word, rt, k, 1)
    rt_clusters1 = utils.load_from_disk(rt_clusters_pth1)
    rt_clusters_pth2 = './data/{}/words/{}/cloud_sim_matrix/rt{}/k{}_rt_clusters_t{}.pkl'. \
        format(language, word, rt, k, 2)
    rt_clusters2 = utils.load_from_disk(rt_clusters_pth2)
    hf1 = utils.load_from_disk(hf_path1)
    hf2 = utils.load_from_disk(hf_path2)
    lf1 = utils.load_from_disk(lf_path1)
    lf2 = utils.load_from_disk(lf_path2)
    return hf1, hf2, rt_clusters1, rt_clusters2, lf1, lf2


def eva_hf_with_scenes(hf, time, scenes, sct, static_embeds):
    # scene_clus = []
    nbs = hf.c_neighbors
    has_scene = False
    for scene in scenes:
        clus = scene['clus']
        for clu_tuple in clus:
            _, clu = clu_tuple
            nbs_ = clu.c_neighbors
            sim = neighbor_similarity(nbs, nbs_, static_embeds)
            if sim > sct:
                clus.append((time, hf))
                scene['clus'] = clus
                has_scene = True
                break
        if has_scene:
            break
    if not has_scene:
        new_scene = {'clus': [(time, hf)]}
        scenes.append(new_scene)


def sc_ranking(language, rt, sct, k, usenf=True, uself=False):
    keywords_file = './corpus/{}/semeval_task/keywords.pkl'.format(language)
    keywords = utils.load_from_disk(keywords_file)
    # keywords = ['bit']
    separate = None if language == 'en' or language == 'sv' else True
    corpus1 = './corpus/{}/semeval_task/data1.pkl'.format(language)
    corpus2 = './corpus/{}/semeval_task/data2.pkl'.format(language)
    f = rd.gen_run_data(keywords, "", corpus1, corpus2, language, lan_emb=None, separate=separate)
    rank_results = {}
    for run_data in f:
        word, cloud1, cloud2, static_embeds1, static_embeds2, static_embeds = \
            get_cloud_data(run_data)
        print("exec word ", word)
        hf1, hf2, rt_clusters1, rt_clusters2, lf1, lf2 = load_clusters(language, rt, sct, k, word)

        point_num1 = 0
        point_num2 = 0
        for hf in rt_clusters1:
            point_num1 += hf.point_num
        for hf in rt_clusters2:
            point_num2 += hf.point_num
        print("point_num1 {} point_num2 {}".format(point_num1, point_num2))

        hflf1_num = 0
        hflf2_num = 0
        scenes = []
        for hf in hf1:
            hflf1_num += hf.point_num
            eva_hf_with_scenes(hf, 1, scenes, sct, static_embeds)

        for hf in hf2:
            hflf2_num += hf.point_num
            eva_hf_with_scenes(hf, 2, scenes, sct, static_embeds)
        
        if uself:
            for lf in lf1:
                hflf1_num += lf.point_num
                eva_hf_with_scenes(lf, 1, scenes, sct, static_embeds)

            for lf in lf2:
                hflf2_num += lf.point_num
                eva_hf_with_scenes(lf, 2, scenes, sct, static_embeds)
        print("hf1_num {} hf2_num {}".format(hflf1_num, hflf2_num))
        print("scenes: ", scenes)
        if usenf:
            p_nums = [point_num1, point_num2]
            p = np.zeros((2, len(scenes) + 1))
            p[0, len(scenes)] = (point_num1 - hflf1_num) / point_num1
            p[1, len(scenes)] = (point_num2 - hflf2_num) / point_num2
        else:
            p_nums = [hflf1_num, hflf2_num]
            p = np.zeros((2, len(scenes)))
        for scene_id, scene in enumerate(scenes):
            clus = scene['clus']
            s = [0, 0]
            for clu_tuple in clus:
                time, clu = clu_tuple
                timeid = time-1
                s[timeid] += clu.point_num
            p[0, scene_id] = s[0] / p_nums[0]
            p[1, scene_id] = s[1] / p_nums[1]
        print("probability distribution ", p)
        jsd = distance.jensenshannon(p[0, :], p[1, :])
        print("jensenshannon divergence ", jsd)
        rank_results[word] = jsd
    acc = evaluate_semeval_task2(rank_results, language)
    acc_rho, p = acc
    return acc_rho


if __name__ == '__main__':

    # language, rt, sct, k = ('en', 0.69, 0.60, 14)
    # language, rt, sct, k = ('de', 0.82, 0.63, 14)
    # language, rt, sct, k = ('la', 0.85, 0.85, 10)
    # language, rt, sct, k = ('sv', 0.85, 0.71, 10)
    # sc_ranking(language, rt, sct, k)

    sample_k = [10, 12, 14]
    languages = ['en', 'de', 'la', 'sv']
    # sample_k = [14]
    # languages = ['en']
    maxranks = {'en':[], 'de':[], 'la':[], 'sv':[]}
    usenf=False
    uself=True
    for language in languages:
        if language in ['en', 'de', 'sv']:
            # rt, sct configuration for language ['en', 'de', 'sv']
            sample_rt = [0.65, 0.66, 0.67, 0.68, 0.69, 0.70, 0.71, 0.72, 0.73, 0.74, 0.75,
                         0.75, 0.76, 0.77, 0.78, 0.79, 0.80, 0.81, 0.82, 0.83, 0.84, 0.85]
            sample_sct = [0.55, 0.56, 0.57, 0.58, 0.59, 0.60, 0.61, 0.62, 0.63, 0.64, 0.65,
                          0.65, 0.66, 0.67, 0.68, 0.69, 0.70, 0.71, 0.72, 0.73, 0.74, 0.75,]
        else:
            # rt, sct configuration for language ['la']
            sample_rt = [0.75, 0.76, 0.77, 0.78, 0.79, 0.80, 0.81, 0.82, 0.83, 0.84, 0.85,
                         0.86, 0.87, 0.88, 0.89, 0.90,]
            sample_sct = [0.75, 0.76, 0.77, 0.78, 0.79, 0.80, 0.81, 0.82, 0.83, 0.84, 0.85,
                          0.86, 0.87, 0.88, 0.89, 0.90,]
        for k in sample_k:
            sc_ranking_results = []
            for rt in sample_rt:
                for sct in sample_sct:
                    if sct > rt:
                        continue
                    correlation = sc_ranking(language, rt, sct, k, usenf=usenf, uself=uself)
                    rank = {"correlation":correlation, "sct": sct, "rt":rt}
                    sc_ranking_results.append(rank)
            save_path = "./data/semevalrank/{}/k{}/usenf_{}-uself_{}/".format(language, k, usenf, uself)
            utils.save_as_txt(save_path+'sc_ranking_results.txt',str(sc_ranking_results))
            utils.save_to_disk(save_path+'sc_ranking_results.pkl',sc_ranking_results)
            ranks = [rank['correlation'] for rank in sc_ranking_results]
            print("language {} k {} max correlation {}".format(language, k, np.max(ranks)))
            maxranks[language].append(np.max(ranks))
    print("maxranks : ", maxranks)
    utils.save_as_txt('./data/semevalrank/usenf_{}-uself_{}/maxranks.txt'.format(usenf, uself),
                      str(maxranks))

