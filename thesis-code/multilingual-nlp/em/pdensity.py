import utils
import simplemma
import bertcloud as bc
import cloudprior as cp
import numpy as np
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from simplemma import text_lemmatizer
from sklearn.metrics.cluster import normalized_mutual_info_score, adjusted_mutual_info_score, mutual_info_score
from tqdm import tqdm
import cloudprior_ft as cft
import semeval as sev
import codecs
import semeval_task2 as st2
import rundata as rd
import semantic_tree as st
from scipy.spatial import distance
from scipy.stats import gaussian_kde
from transformers import BertTokenizer, AutoModel, AutoTokenizer
from sklearn.cluster import AffinityPropagation
from sklearn.datasets import make_blobs
from sklearn import metrics
from sklearn.cluster import DBSCAN, HDBSCAN
from sklearn.preprocessing import StandardScaler
from sklearn.mixture import GaussianMixture
# 启用latex绘制文字
# import matplotlib
# matplotlib.rcParams['text.usetex'] = True
plt.rcParams.update({
    "text.usetex": True,
    "font.family": "sans-serif",
    "font.size": 16,
    'text.latex.preamble': r'\usepackage{amsmath}'
})

def plt_cloud(language):
    word = 'mouse' # 'mouse', book, bank # crane, light
    # language='en'
    datapath = './data/sysdata/{}/words/{}/'.format(language, word)
    cloud1 = utils.load_from_disk(datapath+'cloud1.pkl')
    cloud2 = utils.load_from_disk(datapath+'cloud2.pkl')

    cloud = cloud1 + cloud2
    X = np.array(cloud)
    pca = PCA(n_components=2)
    pca = pca.fit(X)
    X_dr = pca.transform(X)

    plt.figure(figsize=(10, 8))
    plt.scatter(X_dr[0:len(cloud1), 0], X_dr[0:len(cloud1), 1], c='g', s=8)
    plt.scatter(X_dr[len(cloud1):, 0], X_dr[len(cloud1):, 1], c='r', s=8)
    # plt.legend()
    # plt.title('title')
    plt.savefig(datapath+'cloud.pdf')
    plt.savefig(datapath+'cloud.png')
    plt.show()

prbert_model = 'bert-base-multilingual-cased'
tokenizer = BertTokenizer.from_pretrained(prbert_model)
def tokenize_word(word):
    tokens = tokenizer.tokenize(word)
    return tokens


def test_plt_density():
    word='mouse'
    language = 'en'
    datapath = './data/sysdata/{}/words/{}/'.format(language, word)
    rng = np.random.default_rng(19680801)

    # example data
    mu = 106  # mean of distribution
    sigma = 17  # standard deviation of distribution
    x = rng.normal(loc=mu, scale=sigma, size=420)

    num_bins = 42

    fig, ax = plt.subplots()

    # the histogram of the data
    # n, bins, patches = ax.hist(x, num_bins, density=True)
    n, bins, patches = ax.hist(x, num_bins, density=True)

    # add a 'best fit' line
    y = ((1 / (np.sqrt(2 * np.pi) * sigma)) *
         np.exp(-0.5 * (1 / sigma * (bins - mu))**2))
    ax.plot(bins, y, '--')
    ax.set_xlabel('Value')
    ax.set_ylabel('Probability density')
    ax.set_title('Histogram of normal distribution sample: '
                 fr'$\mu={mu:.0f}$, $\sigma={sigma:.0f}$')

    # Tweak spacing to prevent clipping of ylabel
    fig.tight_layout()
    plt.savefig(datapath+'hist/test_density.png')
    plt.show()


def test_plt_density2():
    word='mouse'
    language = 'en'
    datapath = './data/sysdata/{}/words/{}/'.format(language, word)
    data = np.random.randn(1000)  # Generate 1000 random data points with a standard normal distribution.
    plt.hist(data, bins=20, density=True, alpha=0.6, color='g')
    plt.xlabel('X-Axis Label')
    plt.ylabel('Y-Axis Label')
    plt.title('Histogram and Probability Density')

    kde = gaussian_kde(data)
    x_vals = np.linspace(min(data), max(data), 1000)
    y_vals = kde(x_vals)
    plt.plot(x_vals, y_vals, 'r-', lw=2)
    plt.savefig(datapath+'hist/test_density.png')


def cloud_probability_density_test(language, word = 'mouse'):
    # word = 'mouse' # 'mouse', book, bank # crane, light
    # language='en'
    datapath = './data/sysdata/{}/words/{}/'.format(language, word)
    cloud1 = utils.load_from_disk(datapath+'cloud1.pkl')
    cloud2 = utils.load_from_disk(datapath+'cloud2.pkl')

    c1 = np.mean(cloud1, axis=0)
    c2 = np.mean(cloud2, axis=0)
    sims1 = [utils.cosine_similarity(c1, e) for e in cloud1]
    sims2 = [utils.cosine_similarity(c2, e) for e in cloud2]
    # plt.figure(figsize=(10, 8))
    fig, ax = plt.subplots()
    mu = 106  # mean of distribution
    sigma = 17  # standard deviation of distribution
    rng = np.random.default_rng(19680801)
    x = rng.normal(loc=mu, scale=sigma, size=420)
    counts1, bins1 = np.histogram(x)
    counts2, bins2 = np.histogram(sims1+sims2+sims1+sims2)
    # , range=(0,1)
    data = sims1+sims2
    n, bins, patches = ax.hist(np.array(data), bins=30, density=True)
    x_vals = np.linspace(min(data), max(data), 1000)
    kde = gaussian_kde(data)
    y_vals = kde(x_vals)
    ax.plot(x_vals, y_vals, 'r-', lw=2)
    # np.sum(n * np.diff(bins)) # when density=True, this should be 1
    # plt.hist(sims1, bins=20, density=True)
    plt.savefig(datapath+'hist/hist_density1.png')
    plt.show()

    # plt.figure(figsize=(10, 8))
    # plt.hist(sims2, bins=20, density=True)
    # plt.savefig(datapath+'hist/hist_density2.png')

def plt_density_for_data(data, savepath, xlabel="", sim2dist=True, mu=None, sigma=None, cdist=None):
    if sim2dist:
        data = 1-np.array(data)
    alpha = 0.95 if mu is not None and sigma is not None else 1
    fig, ax = plt.subplots()
    # counts2, bins2 = np.histogram(sims1+sims2+sims1+sims2)
    # data = sims1+sims2
    n, bins, patches = ax.hist(np.array(data), bins=40, density=True, alpha=alpha)
    x_vals = np.linspace(min(data), max(data), 100)
    kde = gaussian_kde(data)
    y_vals = kde(x_vals)
    ax.plot(x_vals, y_vals, 'r-', lw=2, alpha=alpha-0.1, zorder=1)
    if mu is not None and sigma is not None:
        mu_id = np.argmin(np.abs(x_vals-mu))
        mu_p = np.sum(y_vals[0:mu_id]) / np.sum(y_vals)
  
        b,t = ax.get_ylim()
        y = 0.08*(t-b)
        ax.text(mu, y, fr"$p={mu_p:.2f}$"+'\n'+fr"$\mu={mu:.2f}$", fontsize=14,
                verticalalignment='center', horizontalalignment='center', alpha=1, zorder=2)
        ax.plot([mu, mu], [0, 0.018*(t-b)], alpha=1, lw=2,
                c='black', zorder=2)
        ax.plot([mu-sigma, mu-sigma], [0, 0.01*(t-b)], alpha=1, lw=2,
                c='black', zorder=2)
        ax.plot([mu+sigma, mu+sigma], [0, 0.01*(t-b)], alpha=1, lw=2,
                c='black', zorder=2)
        ax.text(mu-sigma, 0.5*y, fr"$\mu-\sigma$", fontsize=12, verticalalignment='center', horizontalalignment='center', alpha=1, zorder=2)
        ax.text(mu+sigma, 0.5*y, fr"$\mu+\sigma$", fontsize=12, verticalalignment='center', horizontalalignment='center', alpha=1, zorder=2)
        #          verticalalignment='center', horizontalalignment='center', alpha=1, zorder=2)
        # ax.text(mu-sigma, y, fr"$p={mu_p:.2f}$"+'\n'+fr"$\mu-\sigma={mu:.2f}$", fontsize=16,
        #          verticalalignment='center', horizontalalignment='center', alpha=1, zorder=2)
        # ax.text(mu+sigma, y, fr"$p={mu_p:.2f}$"+'\n'+fr"$\mu+\sigma={mu:.2f}$", fontsize=16,
        #          verticalalignment='center', horizontalalignment='center', alpha=1, zorder=2)
        # 第二x轴
        # ix = ax.twinx()
        # ix.tick_params(direction='in')
        # ix.set_xticks([mu])
        # ax.tick_params(direction='out')
        # ax.set_xticks(fontsize=12)

    #     # plt.xticks([mu], [fr"$\mu={mu:.2f}$"], position=(0, 0.1))
    #     plt.xticks([mu], ['%.2f' % mu_p+'\n'+fr"$\mu={mu:.2f}$"], position=(0, 0.087), zorder=2)
    if cdist is not None:
        mu_id = np.argmin(np.abs(x_vals-cdist))
        mu_p = np.sum(y_vals[0:mu_id]) / np.sum(y_vals)
  
        b,t = ax.get_ylim()
        y = 0.08*(t-b)
        ax.plot([cdist, cdist], [0, 0.018*(t-b)], alpha=1, lw=2,
                c='black', zorder=2)
        ax.text(cdist, y, fr"$p={mu_p:.2f}$"+'\n'+fr"$d={cdist:.2f}$", fontsize=14,
                verticalalignment='center', horizontalalignment='center', alpha=1, zorder=2)

    plt.yticks([])
    plt.xticks(fontsize=12)
    plt.xlabel(xlabel)
    plt.ylabel("Density")
    plt.savefig(savepath)
    plt.savefig(savepath+'.pdf')


def plt_density_for_dim_dist(data, savepath, xlabel="Whitened embedding"):
    # if '$' in xlabel:
    #     # plt.rc('test', usetex=True)
    #     plt.rcParams['text.usetex'] = True
    #     plt.rcParams['text.latex.unicode'] = True
    #     # plt.rcParams['text.latex.preamble'] = r'''
    fig, ax = plt.subplots()
    n, bins, patches = ax.hist(np.array(data), bins=200, density=True)
    x_vals = np.linspace(min(data), max(data), 200)
    kde = gaussian_kde(data)
    y_vals = kde(x_vals)
    ax.plot(x_vals, y_vals, 'r-', lw=2)
    plt.yticks([])
    plt.xlabel(xlabel)
    plt.ylabel("Density")
    plt.savefig(savepath)


def cloud_dim_distribution(cloud, savepath):
    cloud = np.array(cloud)
    dim_ndist_set = []
    for i in range(cloud.shape[1]):
        dim_dist = cloud[:,i]
        mu = np.mean(dim_dist)
        sigma = np.std(dim_dist)
        dim_ndist = (dim_dist - mu)/sigma
        dim_ndist_set += dim_ndist.tolist()
    # savepath = ''
    plt_density_for_dim_dist(dim_ndist_set, savepath)
    return dim_ndist_set


def stat_euclidean_dist(cloud1, cloud2, datapath):
    c1 = np.mean(cloud1, axis=0)
    c2 = np.mean(cloud2, axis=0)

    e_dist1 = [np.sqrt(np.square(c - c1).sum()) for c in cloud1]
    e_dist2 = [np.sqrt(np.square(c - c2).sum()) for c in cloud2]

    # euclidean近似等价cos distance， 尤其是当normalize之后完全等价。
    # 所以也是类似卡方分布，标准正太化也没有也不影响卡方分布构型。
    # cloud_dim_distribution的统计本来就是正太分布的，结果也是比较完美的标准正太分布。
    # e_dist1 = np.array(e_dist1)
    # mu = np.mean(e_dist1)
    # sigma = np.std(e_dist1)
    # e_dist1 = (e_dist1 - mu)/sigma
    # e_dist1 = e_dist1.tolist()

    # e_dist2 = np.array(e_dist2)
    # mu = np.mean(e_dist2)
    # sigma = np.std(e_dist2)
    # e_dist2 = (e_dist2 - mu)/sigma
    # e_dist2 = e_dist2.tolist()
    ce_dist = np.sqrt(np.square(c1 - c2).sum())
    plt_density_for_data(e_dist1+e_dist2, datapath+'/hist_euclidean_dist.png',
                         xlabel="Euclidean distance to semantic center", sim2dist=False, cdist=ce_dist)


    return e_dist1+e_dist2, ce_dist


def cloud_probability_density(language, word = 'mouse'):
    cloud_nbs = load_cloud_nbs(language, word)

    datapath = './data/sysdata/{}/words/{}/'.format(language, word)
    cloud1 = utils.load_from_disk(datapath+'cloud1.pkl')
    cloud2 = utils.load_from_disk(datapath+'cloud2.pkl')
    ed_dist, ced_dist = stat_euclidean_dist(cloud1, cloud2, datapath)
    # eva_clustering_model(cloud1, cloud2)
    dim_ndist_set1 = cloud_dim_distribution(cloud1, datapath+'/dim_dist1.png')
    dim_ndist_set2 = cloud_dim_distribution(cloud2, datapath+'/dim_dist2.png')
    # if word == 'bank':
    #     plt_density_for_dim_dist(np.array(cloud1)[:,0], datapath+'/dim_dist_onedim.png', xlabel=r"$x_{i}$")
    dim_ndist = dim_ndist_set1 + dim_ndist_set2
    cov1 = np.cov(np.array(cloud1).T)
    cov2 = np.cov(np.array(cloud2).T)
    # for i in range(cov2.shape[0]):
    #     cov1[i,i] = 0
    #     cov2[i,i] = 0
    print("cov1 mean {} min {} max {},  cov2 mean {}  min {} max {}".format(
        np.mean(cov1), np.min(cov1), np.max(cov1), np.mean(cov2), np.min(cov2), np.max(cov2)))
    plt_weights(cov1, datapath+'/cov1.png')
    plt_weights(cov2, datapath+'/cov2.png')
    assert len(cloud_nbs) == len(cloud1) + len(cloud2)
    static_embeds = utils.load_from_disk(datapath+'static_embeds.pkl')
    k = 14
    c1 = np.mean(cloud1, axis=0)
    c2 = np.mean(cloud2, axis=0)
    nbs1 = cp.clear_sim_words(word, c1, k, static_embeds)
    nbs2 = cp.clear_sim_words(word, c2, k, static_embeds)
    c_nbs_sim = cp.neighbor_similarity(nbs1, nbs2, static_embeds)
    c_sim = utils.cosine_similarity(c1, c2)
    sims1 = [utils.cosine_similarity(c1, e) for e in cloud1]
    sims2 = [utils.cosine_similarity(c2, e) for e in cloud2]
    plt_density_for_data(sims1+sims2, datapath+'/hist_density.png',
                         xlabel="Cosine distance to semantic center", cdist=1-c_sim)
    nbs_sims1 = [cp.neighbor_similarity(nbs1, cloud_nbs[i], static_embeds) for i in range(len(cloud1))]
    nbs_sims2 = [cp.neighbor_similarity(nbs2, cloud_nbs[i+len(cloud1)], static_embeds) for i in range(len(cloud2))]
    plt_density_for_data(nbs_sims1+nbs_sims2, datapath+'/hist_density_nbs.png',
                         xlabel="Neighbor-based cosine distance to semantic center", cdist=1-c_nbs_sim)
    # plt.figure(figsize=(10, 8))
    # fig, ax = plt.subplots()
    # # counts2, bins2 = np.histogram(sims1+sims2+sims1+sims2)
    # data = sims1+sims2
    # n, bins, patches = ax.hist(np.array(data), bins=40, density=True)
    # x_vals = np.linspace(min(data), max(data), 100)
    # kde = gaussian_kde(data)
    # y_vals = kde(x_vals)
    # ax.plot(x_vals, y_vals, 'r-', lw=2)
    # plt.savefig(datapath+'/hist_density.png')
    # plt.show()
    simdata = sims1+sims2
    nbs_simdata = nbs_sims1+nbs_sims2
    return {'simdata':simdata, 'nbs_simdata':nbs_simdata, 'c_sim':c_sim,
            'c_nbs_sim':c_nbs_sim, 'nbs1':nbs1, 'nbs2':nbs2, 'dim_ndist':dim_ndist,
            'ed_dist':ed_dist, 'ced_dist':ced_dist}


def plt_weights(weights, savepath):
    # print("weights", weights)
    plt.matshow(weights)
    plt.colorbar()
    # plt.title(title +" similarity matrix")
    plt.savefig(savepath)
    # plt.show()


def load_corpus(language):
    datapath = './data/sysdata/{}/corpus.pkl'.format(language)
    data = utils.load_from_disk(datapath)
    return data


def load_cloud_nbs(language, keyword):
    syn = ''
    k = 14
    time = 1
    usecache = True
    matrix_path = './data/sysdata/{}/words/{}/cloud_sim_matrix/{}k{}_cloud_matrix_t{}.npy'.format(language, keyword, syn, k, time)
    nbs_path = './data/sysdata/{}/words/{}/cloud_sim_matrix/{}k{}_cloud_nbs_t{}.pkl'.format(language, keyword, syn, k, time)
    if usecache and utils.exists(matrix_path):
        print(" cloud points distance matrix exists ")
        _, cloud_nbs = cp.load_cloud_prior(matrix_path, nbs_path, load_nbs=True)
        return cloud_nbs


def clustering_affinity_propagation(cloud):
    # af = AffinityPropagation(preference=-50, random_state=0).fit(X)
    step = -100
    preference = 0
    n_clusters_ = 3
    while(n_clusters_ != 2):
        af = AffinityPropagation(preference=preference, random_state=0).fit(cloud)
        cluster_centers_indices = af.cluster_centers_indices_
        labels = af.labels_
        n_clusters_ = len(cluster_centers_indices)
        print("Estimated number of clusters: %d" % n_clusters_)
        print("Estimated number of len labels: %d" % len(labels))
        if n_clusters_ == 1:
            step = 10
        preference += step
    print("Choosing preference ", preference)
    return labels


def clustering_HDBSCAN(cloud):
    # cloud = StandardScaler().fit_transform(cloud)
    # db = HDBSCAN(cluster_selection_epsilon=0.0, min_samples=10).fit(X)
    # db = HDBSCAN(cluster_selection_epsilon=0, min_samples=2).fit(cloud)
    db = HDBSCAN().fit(cloud)
    # db = HDBSCAN().fit(cloud)
    labels = db.labels_

    # Number of clusters in labels, ignoring noise if present.
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
    n_noise_ = list(labels).count(-1)

    print("Estimated number of clusters: %d" % n_clusters_)
    print("Estimated number of noise points: %d" % n_noise_)
    # print("Estimated number of noise rate: %d" % n_noise_/cloud.shape[0])
    assert n_clusters_ == 2
    return labels


def clustering_DBSCAN(cloud):
    cloud = StandardScaler().fit_transform(cloud)
    step = 2
    eps = 25
    n_clusters_ = 3
    while(n_clusters_ != 2):
        print("eps: ", eps)
        db = DBSCAN(eps=eps, min_samples=30).fit(cloud)
        labels = db.labels_

        # Number of clusters in labels, ignoring noise if present.
        n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
        n_noise_ = list(labels).count(-1)

        print("Estimated number of clusters: %d" % n_clusters_)
        print("Estimated number of noise points: %d" % n_noise_)
        print("Estimated number of noise rate", n_noise_/cloud.shape[0])
        if n_clusters_ < 2:
            step = -0.1
        eps += step
    assert n_clusters_ == 2
    return labels


def clustering_GaussianMixture(cloud):
    # X = np.array([[1, 2], [1, 4], [1, 0], [10, 2], [10, 4], [10, 0]])
    gm = GaussianMixture(n_components=2, random_state=0).fit(cloud)
    gm.means_
    
    # print(gm.means_)
    # print(gm.predict([[0, 0], [12, 3]]))
    # print(gm.predict(cloud))
    labels = gm.predict(cloud)
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)

    print("Estimated number of clusters: %d" % n_clusters_)
    assert n_clusters_ == 2

    return labels

def eva_cluster_result(labels, cloud1, cloud2):
    cloud = cloud1 + cloud2
    c0 = np.mean(cloud1, axis=0)
    c1 = np.mean(cloud2, axis=0)
    l_0 = []
    l_1 = []
    for id, cid in enumerate(labels):
        if cid == 0:
            l_0 += [id]
        if cid == 1:
            l_1 += [id]
    
    c_l_0s = [cloud[id] for id in l_0]
    c_l_1s = [cloud[id] for id in l_1]
    c_l_0 = np.mean(c_l_0s, axis=0)
    c_l_1 = np.mean(c_l_1s, axis=0)

    # l_0 assign 0, l_1 assign 1
    c_diff0 = 1 - utils.cosine_similarity(c0, c_l_0)
    c_diff1 = 1 - utils.cosine_similarity(c1, c_l_1)
    c_diff = [c_diff0, c_diff1]
    cou_0_in_l_0 = [1 if id < len(cloud1) else 0 for id in l_0]
    cou_1_in_l_1 = [1 if id >= len(cloud1) else 0 for id in l_1]
    cou_0_in_l_0 = sum(cou_0_in_l_0)
    cou_1_in_l_1 = sum(cou_1_in_l_1)
    purity_score = (cou_0_in_l_0 + cou_1_in_l_1)/(len(l_0) + len(l_1))

    # l_0 assign 1, l_1 assign 0
    c_diff0 = 1 - utils.cosine_similarity(c0, c_l_1)
    c_diff1 = 1 - utils.cosine_similarity(c1, c_l_0)
    xc_diff = [c_diff0, c_diff1]
    cou_1_in_l_0 = len(l_0) - cou_0_in_l_0
    cou_0_in_l_1 = len(l_1) - cou_1_in_l_1
    xpurity_score = (cou_1_in_l_0 + cou_0_in_l_1)/(len(l_0) + len(l_1))

    c_diffs = [np.mean(c_diff), np.mean(xc_diff)]
    purity_scores = [purity_score, xpurity_score]

    return np.min(c_diffs), np.max(purity_scores)


def eva_clustering_model(cloud1, cloud2):
    cloud = np.array(cloud1 + cloud2)
    # labels = clustering_affinity_propagation(cloud)
    # labels = clustering_HDBSCAN(cloud)
    labels = clustering_DBSCAN(cloud)
    # labels = clustering_GaussianMixture(cloud)
    pred_c_diff, purity_s = eva_cluster_result(labels, cloud1, cloud2)
    print("Estimated pred_c_diff:", pred_c_diff)
    print("Estimated purity_s:", purity_s)
    return pred_c_diff, purity_s


def plt_densities(language='en'):
    corpus = load_corpus(language)
    stat = {}
    simdata = []
    nbs_simdata = []
    dim_ndist = []
    ed_dist = []
    datapath = './data/sysdata/{}/'.format(language)
    # corpus = ['bank']
    for word in corpus:
        wstat = cloud_probability_density(language, word = word)
        stat[word] = wstat
        simdata += wstat['simdata']
        nbs_simdata += wstat['nbs_simdata']
        dim_ndist += wstat['dim_ndist']
        ed_dist += wstat['ed_dist']
        print('{}: avg sim to center stat mean {}, std {}'.format(word,np.mean(wstat['simdata']), np.std(wstat['simdata'])))
        print('{}: avg nbs sim to center stat mean {}, std {}'.format(word,np.mean(wstat['nbs_simdata']), np.std(wstat['nbs_simdata'])))
    utils.save_to_disk(datapath+"cloud_densities_stat.pkl", stat)
    # print(stat)
    c_dist = [1-stat[w]['c_sim'] for w in stat]
    c_nbs_dist = [1-stat[w]['c_nbs_sim'] for w in stat]
    c_ed_dist = [stat[w]['ced_dist'] for w in stat]
    print('Avg center cosine distance stat mean, std', np.mean(c_dist), np.std(c_dist))
    print('Avg nbs center cosine distance stat mean, std', np.mean(c_nbs_dist), np.std(c_nbs_dist))
    print('Avg center euclidean distance stat mean, std', np.mean(c_ed_dist), np.std(c_ed_dist))

    plt_density_for_data(simdata, datapath+'/hist_density.png',
                         xlabel="Cosine distance to semantic center", mu=np.mean(c_dist), sigma=np.std(c_dist))
    plt_density_for_data(nbs_simdata, datapath+'/hist_density_nbs.png',
                         xlabel="Neighbor-based cosine distance to semantic center", mu=np.mean(c_nbs_dist), sigma=np.std(c_nbs_dist))
    plt_density_for_data(ed_dist, datapath+'/euclidean_dist.png',
                         xlabel="Euclidean distance to semantic center", sim2dist=False, mu=np.mean(c_ed_dist), sigma=np.std(c_ed_dist))

    plt_density_for_data(simdata, datapath+'/neat_hist_density.png',
                         xlabel="Cosine distance to semantic center")
    plt_density_for_data(nbs_simdata, datapath+'/neat_hist_density_nbs.png',
                         xlabel="Neighbor-based cosine distance to semantic center")
    plt_density_for_data(ed_dist, datapath+'/neat_euclidean_dist.png',
                         xlabel="Euclidean distance to semantic center")
    
    plt_density_for_dim_dist(dim_ndist, datapath+'/dim_ndist.png')
    simdata = 1-np.array(simdata)
    nbs_simdata = 1-np.array(nbs_simdata)
    print('Avg cloud sim to center stat mean {}, std {}'.format(np.mean(simdata), np.std(simdata)))
    print('Avg cloud nbs sim to center stat mean {}, std {}'.format(np.mean(nbs_simdata), np.std(nbs_simdata)))

    # data = 1-np.array(data)
    # fig, ax = plt.subplots()
    # n, bins, patches = ax.hist(np.array(data), bins=40, density=True)
    # x_vals = np.linspace(min(data), max(data), 100)
    # kde = gaussian_kde(data)
    # y_vals = kde(x_vals)
    # ax.plot(x_vals, y_vals, 'r-', lw=2)
    # plt.savefig(datapath+'/hist_density.png')


if __name__ == '__main__':
    language='sv'
    # load_raw_corpus(language=language)
    # cloud_probability_density(language=language)
    # test_plt_density2()
    plt_densities(language=language)




