import utils
import simplemma
import bertcloud as bc
import cloudprior as cp
import numpy as np
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from simplemma import text_lemmatizer
from sklearn.metrics.cluster import normalized_mutual_info_score, adjusted_mutual_info_score, mutual_info_score
from tqdm import tqdm
import cloudprior_ft as cft
import semeval as sev
import codecs
import semeval_task2 as st2
import rundata as rd
import semantic_tree as st
from scipy.spatial import distance
from scipy.stats import gaussian_kde
from transformers import BertTokenizer, AutoModel, AutoTokenizer
from sklearn.cluster import AffinityPropagation, KMeans
from sklearn.datasets import make_blobs
from sklearn import metrics
from sklearn.cluster import DBSCAN, HDBSCAN
from sklearn.preprocessing import StandardScaler
from sklearn.mixture import GaussianMixture
from finetuning import fine_tuning_cloud_t0
import seaborn as sns
from semantic_tree import find_k_elbow_rule
# 启用latex绘制文字
# import matplotlib
# matplotlib.rcParams['text.usetex'] = True
plt.rcParams.update({
    "text.usetex": True,
    "font.family": "sans-serif",
    "font.size": 13,
    'text.latex.preamble': r'\usepackage{amsmath}'
})
prbert_model = 'bert-base-multilingual-cased'
tokenizer = BertTokenizer.from_pretrained(prbert_model)
def tokenize_word(word):
    tokens = tokenizer.tokenize(word)
    return tokens


def load_corpus(language):
    datapath = './data/sysdata/{}/corpus.pkl'.format(language)
    data = utils.load_from_disk(datapath)
    return data


def load_cloud_nbs(language, keyword):
    syn = ''
    k = 14
    time = 1
    usecache = True
    matrix_path = './data/sysdata/{}/words/{}/cloud_sim_matrix/{}k{}_cloud_matrix_t{}.npy'.format(language, keyword, syn, k, time)
    nbs_path = './data/sysdata/{}/words/{}/cloud_sim_matrix/{}k{}_cloud_nbs_t{}.pkl'.format(language, keyword, syn, k, time)
    if usecache and utils.exists(matrix_path):
        print(" cloud points distance matrix exists ")
        _, cloud_nbs = cp.load_cloud_prior(matrix_path, nbs_path, load_nbs=True)
        return cloud_nbs


def clustering_affinity_propagation(cloud):
    # af = AffinityPropagation(preference=-50, random_state=0).fit(X)
    step = -100
    preference = 0
    n_clusters_ = 3
    while(n_clusters_ != 2):
        af = AffinityPropagation(preference=preference, random_state=0).fit(cloud)
        cluster_centers_indices = af.cluster_centers_indices_
        labels = af.labels_
        n_clusters_ = len(cluster_centers_indices)
        print("Estimated number of clusters: %d" % n_clusters_)
        print("Estimated number of len labels: %d" % len(labels))
        if n_clusters_ == 1:
            step = 10
        preference += step
    print("Choosing preference ", preference)
    return labels


def clustering_HDBSCAN(cloud):
    # cloud = StandardScaler().fit_transform(cloud)
    # db = HDBSCAN(cluster_selection_epsilon=0.0, min_samples=10).fit(X)
    # db = HDBSCAN(cluster_selection_epsilon=0, min_samples=2).fit(cloud)
    db = HDBSCAN().fit(cloud)
    # db = HDBSCAN().fit(cloud)
    labels = db.labels_

    # Number of clusters in labels, ignoring noise if present.
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
    n_noise_ = list(labels).count(-1)

    print("Estimated number of clusters: %d" % n_clusters_)
    print("Estimated number of noise points: %d" % n_noise_)
    # print("Estimated number of noise rate: %d" % n_noise_/cloud.shape[0])
    assert n_clusters_ == 2
    return labels


def clustering_DBSCAN(cloud):
    cloud = StandardScaler().fit_transform(cloud)
    step = 2
    eps = 25
    n_clusters_ = 3
    while(n_clusters_ != 2):
        print("eps: ", eps)
        db = DBSCAN(eps=eps, min_samples=30).fit(cloud)
        labels = db.labels_

        # Number of clusters in labels, ignoring noise if present.
        n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
        n_noise_ = list(labels).count(-1)

        print("Estimated number of clusters: %d" % n_clusters_)
        print("Estimated number of noise points: %d" % n_noise_)
        print("Estimated number of noise rate", n_noise_/cloud.shape[0])
        if n_clusters_ < 2:
            step = -0.1
        eps += step
    assert n_clusters_ == 2
    return labels


def clustering_GaussianMixture(cloud):
    # X = np.array([[1, 2], [1, 4], [1, 0], [10, 2], [10, 4], [10, 0]])
    gm = GaussianMixture(n_components=2, random_state=0).fit(cloud)
    gm.means_
    
    # print(gm.means_)
    # print(gm.predict([[0, 0], [12, 3]]))
    # print(gm.predict(cloud))
    labels = gm.predict(cloud)
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)

    print("Estimated number of clusters: %d" % n_clusters_)
    assert n_clusters_ == 2

    return labels


def clustering_kmeans(cloud):
    bestk = find_k_elbow_rule(cloud)
    print("optimal k = ", bestk)
    kmeans = KMeans(n_clusters=2).fit(np.array(cloud))
    centers = kmeans.cluster_centers_
    labels = kmeans.labels_
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)

    print("Estimated number of clusters: %d" % n_clusters_)
    assert n_clusters_ == 2

    return labels

def clustering_graph_hierarchical(cloud, keyword, language, rt=0.73, sct=0.6):
    datapath = './data/sysdata/{}/words/{}/'.format(language, keyword)
    cloud1 = utils.load_from_disk(datapath+'cloud1.pkl')
    cloud2 = utils.load_from_disk(datapath+'cloud2.pkl')
    static_embeds = utils.load_from_disk(datapath+'static_embeds.pkl')
    time=2
    k=14
    hf_semantic, lf_semantic, t_ub, t_lb = \
        fine_tuning_cloud_t0(cloud, static_embeds, keyword, language, time, k,
                             usecache=False, subsample=True, rt=rt, sct=sct, syn='', drop=False)
    labels = [-1 for c in cloud]
    assert len(hf_semantic) == 2
    for id, hf in enumerate(hf_semantic):
        # label = 0 if np.mean(hf.points) < len(cloud1) else 1
        sum_cl1 = np.sum((np.array(hf.points)<len(cloud1)).astype(np.int32))
        label = 0 if sum_cl1 > len(hf.points)*3/5 else 1
        for p in hf.points:
            labels[p] = label
    if np.sum((np.array(labels)==1).astype(np.int32)) == 0:
        labels = [-1 for c in cloud]
        label0 = 0 if len(hf_semantic[0].points)>len(hf_semantic[1].points) else 1
        for p in hf_semantic[0].points:
            labels[p] = label0
        for p in hf_semantic[1].points:
            labels[p] = 1 - label0
    return labels

def eva_cluster_result(labels, cloud1, cloud2, lsize):
    cloud = cloud1 + cloud2
    c0 = np.mean(cloud1, axis=0)
    c1 = np.mean(cloud2[0:lsize], axis=0)
    l_0 = []
    l_1 = []
    for id, cid in enumerate(labels):
        if cid == 0:
            l_0 += [id]
        if cid == 1:
            l_1 += [id]
    
    c_l_0s = [cloud[id] for id in l_0]
    c_l_1s = [cloud[id] for id in l_1]
    c_l_0 = np.mean(c_l_0s, axis=0)
    c_l_1 = np.mean(c_l_1s, axis=0)

    # l_0 assign 0, l_1 assign 1
    c_diff0 = 1 - utils.cosine_similarity(c0, c_l_0)
    c_diff1 = 1 - utils.cosine_similarity(c1, c_l_1)
    c_diff = [c_diff0, c_diff1]
    cou_0_in_l_0 = [1 if id < len(cloud1) else 0 for id in l_0]
    cou_1_in_l_1 = [1 if id >= len(cloud1) else 0 for id in l_1]
    cou_0_in_l_0 = sum(cou_0_in_l_0)
    cou_1_in_l_1 = sum(cou_1_in_l_1)
    purity_score = (cou_0_in_l_0 + cou_1_in_l_1)/(len(l_0) + len(l_1))

    # l_0 assign 1, l_1 assign 0
    c_diff0 = 1 - utils.cosine_similarity(c0, c_l_1)
    c_diff1 = 1 - utils.cosine_similarity(c1, c_l_0)
    xc_diff = [c_diff0, c_diff1]
    cou_1_in_l_0 = len(l_0) - cou_0_in_l_0
    cou_0_in_l_1 = len(l_1) - cou_1_in_l_1
    xpurity_score = (cou_1_in_l_0 + cou_0_in_l_1)/(len(l_0) + len(l_1))

    c_diffs = [np.mean(c_diff), np.mean(xc_diff)]
    purity_scores = [purity_score, xpurity_score]

    return np.min(c_diffs), np.max(purity_scores)

def eva_gh(cloud1, cloud2, lsize, cloud, word, language):
    rts = None
    if language == 'la':
        if word == 'ratio' or word == 'cursus':
            rt=0.85 # ratio, cursus
        elif word == 'labor':
            rt=0.86 # labor
        elif word == 'arbor':
            rt=0.75 # arbor
        else:
            rt=0.80 # modus
        rt=0.88
        sct=0.70
    if language == 'en':
        rt=0.70
        sct=0.6
    if language == 'de':
        # rts = [0.7,0.72, 0.75, 0.78, 0.8, 0.82, 0.85]
        rt=0.78
        if word == 'Bau':
            rt=0.85
        if word == 'Schloss':
            rt=0.81
        if word == 'Maus':
            rt=0.73
        sct=0.6
    if language == 'sv':
        rt=0.80
        if word == 'kort':
            rt=0.70 # rt=0.70
        if word == 'bark':
            rt=0.75 # rt=0.75
        sct=0.6
    # if rts is not None:
    #     purity = 0
    #     rt_labels = None
    #     for rt in rts:
    #         labels = clustering_graph_hierarchical(cloud, word, language, rt=rt, sct=sct)
    #         pred_c_diff, purity_s = eva_cluster_result(labels, cloud1, cloud2, lsize)
    #         if purity<purity_s:
    #             rt_labels = labels
    #     labels = rt_labels
    # else:
    #     labels = clustering_graph_hierarchical(cloud, word, language, rt=rt, sct=sct)

    labels = clustering_graph_hierarchical(cloud, word, language, rt=rt, sct=sct)
    return labels


def eva_clustering_model(cloud1, cloud2, word, language, model_name='kmeans'):
    # lsize = 20
    lsize = len(cloud2)
    cloud = np.array(cloud1 + cloud2[0:lsize])

    # labels = clustering_HDBSCAN(cloud)
    # labels = clustering_DBSCAN(cloud)
    if model_name == 'GM':
        labels = clustering_GaussianMixture(cloud)
    elif model_name == 'ap':
        labels = clustering_affinity_propagation(cloud)
    elif model_name == 'gh':
        labels = eva_gh(cloud1, cloud2, lsize, cloud, word, language)
    elif model_name == 'kmeans':
        labels = clustering_kmeans(cloud)
    # plt_cloud(cloud1, cloud2, language, word, model_name, labels, lsize=lsize)
    pred_c_diff, purity_s = eva_cluster_result(labels, cloud1, cloud2, lsize)
    print("Estimated pred_c_diff:", pred_c_diff)
    print("Estimated purity_s:", purity_s)
    return labels, pred_c_diff, purity_s


def plt_cloud(cloud1, cloud2, language, word, model, labels, lsize=0):
    # word = 'kort' # 'mouse', book, bank # crane, light
    # # language='en'
    datapath = './data/sysdata/{}/words/{}/ls{}_'.format(language, word, lsize)
    model_datapath = './data/sysdata/{}/words/{}/{}_ls{}_'.format(language, word, model, lsize)
    # cloud1 = utils.load_from_disk(datapath+'cloud1.pkl')
    # cloud2 = utils.load_from_disk(datapath+'cloud2.pkl')

    cloud = cloud1 + cloud2
    X = np.array(cloud)
    pca = PCA(n_components=2)
    pca = pca.fit(X)
    X_dr = pca.transform(X)
    colors = sns.color_palette(n_colors=3)
    plt.figure(figsize=(10, 8))
    plt.scatter(X_dr[0:len(cloud1), 0], X_dr[0:len(cloud1), 1], c=colors[0], s=30)
    plt.scatter(np.mean(X_dr[0:len(cloud1), 0]), np.mean(X_dr[0:len(cloud1), 1]), marker='+', c=colors[0], s=120)
    plt.scatter(X_dr[len(cloud1):len(cloud1)+lsize, 0], X_dr[len(cloud1):len(cloud1)+lsize, 1], c=colors[1], s=30)
    plt.scatter(np.mean(X_dr[len(cloud1):len(cloud1)+lsize, 0]), np.mean(X_dr[len(cloud1):len(cloud1)+lsize, 1]), marker='+', c=colors[1], s=120)
    # plt.legend()
    # plt.title('title')
    plt.savefig(datapath+'cloud_truth.pdf')
    plt.savefig(datapath+'cloud_truth.png')
    # plt.show()

    plt.figure(figsize=(10, 8))
    labels = [la if la != -1 else 2 for la in labels]
    labels0 = []
    labels1 = []
    # set label for cloud1
    counts = np.bincount(labels[0:len(cloud1)])
    # label_cloud1 = np.argmax(counts)
    label_cloud1 = 0 if counts[0] > counts[1] else 1
    for i, la in enumerate(labels):
        # if la == 0:
        if la == label_cloud1:
            plt.scatter(X_dr[i, 0], X_dr[i, 1], c=colors[0], s=30)
            labels0.append([X_dr[i, 0], X_dr[i, 1]])
        elif la == 1-label_cloud1:
            plt.scatter(X_dr[i, 0], X_dr[i, 1], c=colors[1], s=30)
            labels1.append([X_dr[i, 0], X_dr[i, 1]])
        else:
            plt.scatter(X_dr[i, 0], X_dr[i, 1], c='gray', s=18)
    plt.scatter(np.mean(np.array(labels0)[:,0]), np.mean(np.array(labels0)[:,1]), c=colors[0], marker='+', s=120)
    plt.scatter(np.mean(np.array(labels1)[:,0]), np.mean(np.array(labels1)[:,1]), c=colors[1], marker='+', s=120)
    plt.savefig(model_datapath+'cloud_pred.pdf')
    plt.savefig(model_datapath+'cloud_pred.png')


def withening_clusters_pca(X_dr, hf_clusters, scale):
    for clu in hf_clusters:
        clu_dr = [X_dr[pid, :] for pid in clu.points]
        center = np.mean(clu_dr, axis=0)
        for pid in clu.points:
            point = X_dr[pid]
            newpoint = center + scale*(point-center)
            X_dr[pid] = newpoint 
    return X_dr

def plt_cloud_clusters_tmp():
    word = 'ratio' # 'mouse', book, bank # crane, light
    language='la'
    datapath = './data/sysdata/{}/words/{}/'.format(language, word)
    cloud1 = utils.load_from_disk(datapath+'cloud1.pkl')
    cloud2 = utils.load_from_disk(datapath+'cloud2.pkl')
    tmp_pth = './data/sysdata/la/words/{}/cluster_tmp/rt0.88/'. \
            format(word)
    hf_clusters = utils.load_from_disk(tmp_pth+'hf_clusters.pkl')
    lf_clusters = utils.load_from_disk(tmp_pth+'lf_clusters.pkl')

    cloud = cloud1 + cloud2
    X = np.array(cloud)
    pca = PCA(n_components=2)
    pca = pca.fit(X)
    X_dr = pca.transform(X)
    colors = sns.color_palette(n_colors=15)
    X_dr = withening_clusters_pca(X_dr, hf_clusters, scale = 0.31)
    # plt.figure(figsize=(10, 8))
    plt.figure()
    # plt.scatter(X_dr[:, 0], X_dr[:, 1], c='black', s=28)
    for i, clu in enumerate(hf_clusters):
        for pid in clu.points:
            # plt.scatter(X_dr[pid, 0], X_dr[pid, 1], c=colors[i], s=28, alpha=0.3)
            plt.scatter(X_dr[pid, 0], X_dr[pid, 1], c=colors[i], s=18)
    for i, clu in enumerate(lf_clusters):
        for pid in clu.points:
            plt.scatter(X_dr[pid, 0], X_dr[pid, 1], c='black', s=18)
    plt.title("$\mathcal{C}_w^{t}$ (w = ratio)", size=16)
    plt.xlabel("Our Clustering (n\_clusters = {}, 1st round)".format(len(hf_clusters)), size=16)
    plt.savefig(tmp_pth+'cloud_tmp_clusters.pdf')
    plt.savefig(tmp_pth+'cloud_tmp_clusters.png')

    hf_semantic = utils.load_from_disk(tmp_pth+'hf_semantic.pkl')
    lf_semantic = utils.load_from_disk(tmp_pth+'lf_semantic.pkl')
    plt.figure()
    for i, clu in enumerate(hf_semantic):
        for pid in clu.points:
            plt.scatter(X_dr[pid, 0], X_dr[pid, 1], c=colors[i], s=18)
    for i, clu in enumerate(lf_semantic):
        for pid in clu.points:
            plt.scatter(X_dr[pid, 0], X_dr[pid, 1], c='black', s=18)
    plt.title("$\mathcal{C}_w^{t}$ (w = ratio)", size=16)
    plt.xlabel("Our Clustering (n\_clusters = {}, 2nd round)".format(len(hf_semantic)), size=16)
    plt.savefig(tmp_pth+'cloud_semantic_clusters.pdf')
    plt.savefig(tmp_pth+'cloud_semantic_clusters.png')


def evaluation_word(language, word = 'mouse', model_name='kmeans'):
    cloud_nbs = load_cloud_nbs(language, word)

    datapath = './data/sysdata/{}/words/{}/'.format(language, word)
    cloud1 = utils.load_from_disk(datapath+'cloud1.pkl')
    cloud2 = utils.load_from_disk(datapath+'cloud2.pkl')
    # cloud2 = cloud2[0:20]
    labels, pred_c_diff, purity_s = eva_clustering_model(cloud1, cloud2, word, language, model_name=model_name)
 
    return {'purity_s':purity_s, 'pred_c_diff': pred_c_diff}

def evaluation(language='en', model_name='kmeans'):
    corpus = load_corpus(language)
    stat = {}

    datapath = './data/sysdata/{}/'.format(language)
    corpus = ['ratio']
    # corpus = ['bank']
    # corpus = ['bark']
    purity_s = []
    for word in corpus:
        wstat = evaluation_word(language, word = word, model_name=model_name)
        stat[word] = wstat
        purity_s.append(wstat['purity_s'])
    print(stat)
    print("purity_s mean {} std {}".format(np.mean(purity_s), np.std(purity_s)))
    return {'p_mean': np.mean(purity_s), 'p_std': np.std(purity_s)}


def boxplot(none_compound_ws, compound_ws, xlabel, ylabel, savepath):
    # datapath = './data/sysdata/compound_ws/'
    all_data = [none_compound_ws, compound_ws]
    labels = ['noncompound', 'compound']

    fig, ax1 = plt.subplots(nrows=1, ncols=1, figsize=(5, 5))

    # rectangular box plot
    bplot1 = ax1.boxplot(all_data,
                         vert=True,  # vertical box alignment
                         patch_artist=True,  # fill with color
                         labels=labels)  # will be used to label x-ticks
    # ax1.set_title('Rectangular box plot')

    # fill with colors
    colors = ['pink', 'lightblue', 'lightgreen']
    for patch, color in zip(bplot1['boxes'], colors):
        patch.set_facecolor(color)

    # adding horizontal grid lines
    ax1.yaxis.grid(True)
    ax1.set_xlabel(xlabel)
    ax1.set_ylabel(ylabel)

    # plt.show()
    # plt.savefig(datapath+'test_boxplot2.png')
    plt.savefig(savepath)


def compound_word_stats():
    languages=['en', 'de', 'la', 'sv']
    compound_ws = []
    none_compound_ws = []
    tokens_stat = {}
    for lan in languages:
        corpus = load_corpus(lan)

        for word in corpus:
            tokens = tokenize_word(word)
            tokens_stat[word]=tokens
            if len(tokens) > 1:
                compound_ws.append(word)
            else:
                none_compound_ws.append(word)
    print("len none_compound_ws", len(none_compound_ws))
    print("len compound_ws", len(compound_ws))
    print(tokens_stat)
    return none_compound_ws, compound_ws
    

def compound_word_analysis():
    none_compound_ws, compound_ws = compound_word_stats()
    datapath = './data/sysdata/compound_ws/'
    languages=['en', 'de', 'la', 'sv']
    model_name='kmeans' # 'gh', 'kmeans', 'GM', 'ap'
    stat = {}
    for lan in languages:
        corpus = load_corpus(lan)
        for word in corpus:
            wstat = evaluation_word(lan, word = word, model_name=model_name)
            stat[word] = wstat
    none_compound_ps = [stat[word]['purity_s'] for word in none_compound_ws]
    compound_ps = [stat[word]['purity_s'] for word in compound_ws]
    print("none_compound_ps purity_s mean {} std {}".format(np.mean(none_compound_ps), np.std(none_compound_ps)))
    print("compound_ps purity_s mean {} std {}".format(np.mean(compound_ps), np.std(compound_ps)))
    utils.save_to_disk(datapath+model_name+"_cpws.pkl", stat)


def load_compound_word_analysis_purity():
    none_compound_ws, compound_ws = compound_word_stats()
    datapath = './data/sysdata/compound_ws/'
    model_name='kmeans' # 'gh', 'kmeans', 'GM', 'ap'
    stat = utils.load_from_disk(datapath+model_name+"_cpws.pkl")
    none_compound_ps = [stat[word]['purity_s'] for word in none_compound_ws]
    compound_ps = [stat[word]['purity_s'] for word in compound_ws]
    print("none_compound_ps purity_s mean {} std {}".format(np.mean(none_compound_ps), np.std(none_compound_ps)))
    print("compound_ps purity_s mean {} std {}".format(np.mean(compound_ps), np.std(compound_ps)))
    boxplot(none_compound_ps, compound_ps, xlabel='', ylabel='purity score', savepath=datapath+model_name+"_cpws.png")


def load_compound_word_analysis_distance():
    none_compound_ws, compound_ws = compound_word_stats()
    
    languages=['en', 'de', 'la', 'sv']
    stat = {}
    for lan in languages:
        datapath = './data/sysdata/{}/'.format(lan)
        stat_lan = utils.load_from_disk(datapath+"cloud_densities_stat.pkl")
        for word in stat_lan:
            stat[word] = stat_lan[word]

    savepath = './data/sysdata/compound_ws/'
    none_compound_ced_dist = [stat[word]['ced_dist'] for word in none_compound_ws]
    compound_ced_dist = [stat[word]['ced_dist'] for word in compound_ws]
    boxplot(none_compound_ced_dist, compound_ced_dist, xlabel='',
            ylabel='Euclidean distance', savepath=savepath+"_ced_dist.png")

    none_compound_c_sim = [1-stat[word]['c_sim'] for word in none_compound_ws]
    compound_c_sim = [1-stat[word]['c_sim'] for word in compound_ws]
    boxplot(none_compound_c_sim, compound_c_sim, xlabel='',
            ylabel='Cosine distance', savepath=savepath+"_c_sim.png")
    
    none_compound_c_nbs_sim = [1-stat[word]['c_nbs_sim'] for word in none_compound_ws]
    compound_c_nbs_sim = [1-stat[word]['c_nbs_sim'] for word in compound_ws]
    boxplot(none_compound_c_nbs_sim, compound_c_nbs_sim, xlabel='',
            ylabel='Neighbor-based cosine distance', savepath=savepath+"_c_nbs_sim.png")

def load_compound_word_analysis():
    # load_compound_word_analysis_purity()
    load_compound_word_analysis_distance()

def main():
    language='la'
    # models = ['kmeans', 'GM', 'ap']
    models=['gh']
    # load_raw_corpus(language=language)
    # cloud_probability_density(language=language)
    # test_plt_density2()
    stat = {}
    for model_name in models:
        stat[model_name] = evaluation(language=language, model_name=model_name)
    
    print("stat {}".format(stat))

if __name__ == '__main__':
    main()
    plt_cloud_clusters_tmp()

    # compound_word_analysis()
    # load_compound_word_analysis()

