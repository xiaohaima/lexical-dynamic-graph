
import cloudprior as cl
from cloudprior import CloudCluster
import utils
import treesim as ts
import rundata as rd
import semantic_tree as st
import os


def cross_lingual_task():
    # cross_lingual_sc_en()
    cross_lingual_sc_de()
    # cross_lingual_sc_sv()
    # draw_semantic_match()


def draw_semantic_match():
    # de_words = ['Maus', 'Stiefel', 'Wolke', 'Erinnerung', 'Netz', 'Daten', 'füttern', 'Band']
    # en_words = ['mouse', 'boot', 'cloud', 'memory', 'web', 'data', 'feed', 'band']
    # sv_words = ['mus', 'känga', 'moln', 'minne', 'nät', 'data', 'utfodra', 'band']
    # en_words = ['mouse', 'cloud', 'memory', 'web', 'data', 'band']
    # sv_words = ['mus', 'moln', 'minne', 'nät', 'data', 'band']
    
    de_words = ["Dienstmädchen", "fröhlich", "Junggeselle", "Gift", "fast", "Kind", "Bad", "Mail", 'Dokument']
    en_words = ["maid", "gay", "bachelor", 'gift', 'fast', 'kind', 'bad', 'mail', 'document']
    sv_words = ["piga", "gay", "ungkarl", "gift", "fast", "kind", "bad", "mail", 'dokument']
    
    k = 14
    for id in range(len(en_words)):
        language1 = 'en'
        en_w = en_words[id]
        sct = 0.83
        rt = 0.65
        hf_path = './data/{}/words/{}/cloud_sim_matrix/sct{}/rt{}/k{}_cloud_hfsc_t{}.pkl'.\
            format(language1, en_w, sct, rt, k, '2')
        cache_path = './data/{}/words/{}/'.format(language1, en_w)
        hf_en = utils.load_from_disk(hf_path)
        hfnbs_en = [hf.c_neighbors for hf in hf_en]
        stat_emb_en = utils.load_from_disk(cache_path + 'stat_emb2')
        static_en = rd.init_static_embedding()
        static_en = st.update_static_embeds(static_en, stat_emb_en)

        # language2 = 'de'
        # de_w = de_words[id]
        # sct = 0.65
        # rt = 0.83
        # hf_path = './data/{}/words/{}/cloud_sim_matrix/sct{}/rt{}/k{}_cloud_hfsc_t{}.pkl'.\
        #     format(language2, de_w, sct, rt, k, '2')
        # cache_path = './data/{}/words/{}/'.format(language2, de_w)
        # hf_de = utils.load_from_disk(hf_path)
        # hfnbs_de = [hf.c_neighbors for hf in hf_de]
        # stat_emb_de = utils.load_from_disk(cache_path + 'stat_emb2')
        # static_de = rd.init_static_embedding()
        # static_de = st.update_static_embeds(static_de, stat_emb_de)

        language2 = 'sv'
        sv_w = sv_words[id]
        sct = 0.65
        rt = 0.85
        hf_path = './data/{}/words/{}/cloud_sim_matrix/sct{}/rt{}/k{}_cloud_hfsc_t{}.pkl'.\
            format(language2, sv_w, sct, rt, k, '2')
        cache_path = './data/{}/words/{}/'.format(language2, sv_w)
        hf_sv = utils.load_from_disk(hf_path)
        hfnbs_sv = [hf.c_neighbors for hf in hf_sv]
        stat_emb_sv = utils.load_from_disk(cache_path + 'stat_emb2')
        static_sv = rd.init_static_embedding()
        static_sv = st.update_static_embeds(static_sv, stat_emb_sv)

        static_embeds = utils.merge_static_embeds(static_en, static_sv)
        st_embs = [static_en, static_sv]
        match_save_path = './data/semanticmatch/{}-{}/{}-{}/'.format(
            language1, language2, en_w, sv_w)
        if not os.path.exists(match_save_path):
            os.makedirs(match_save_path)
        ts.semantic_match(hfnbs_en, hfnbs_sv, static_embeds, st_embs=st_embs,
                          save_path=match_save_path)



def cross_lingual_sc_en():
    language = 'en'
    k = 14
    rt = 0.70
    sct = 0.61
    # keywords = ['mouse', 'boot', 'cloud', 'memory', 'web', 'data', 'feed', 'band']
    keywords = ["maid", "gay", "bachelor", 'gift', 'fast', 'kind', 'bad', 'mail', 'document']
    # keywords = ['mail']
    print(keywords)
    pmodel = 'bert-base-multilingual-cased'  # 'bert-base-uncased'
    corpus1 = './corpus/{}/crosslingual-loss-eva/semeval/data.pkl'.format(language)
    corpus2 = './corpus/{}/crosslingual-loss-eva/wiki/data.pkl'.format(language)
    semantic_change_hf, semantic_change_hf_lf =\
        cl.run_cloud(keywords, pmodel, corpus1, corpus2, language,
                     lan_emb=None, k=k, separate=True, rt=rt, sct=sct)
    print("semantic_change_hf ", semantic_change_hf)
    

def cross_lingual_sc_de():
    language = 'de'
    k = 14
    rt = 0.83
    # rt = 0.75
    sct = 0.65
    # keywords = ["Dienstmädchen", "fröhlich", "Junggeselle", "Gift", "fast", "Kind", "Bad", "Mail", 'Dokument'] 
    keywords = ['Gift']
    print(keywords)
    pmodel = 'bert-base-multilingual-cased'  # 'bert-base-uncased'
    corpus1 = './corpus/{}/crosslingual-loss-eva/semeval/data.pkl'.format(language)
    corpus2 = './corpus/{}/crosslingual-loss-eva/wiki/data.pkl'.format(language)
    semantic_change_hf, semantic_change_hf_lf =\
        cl.run_cloud(keywords, pmodel, corpus1, corpus2, language,
                     lan_emb=None, k=k, separate=True, rt=rt, sct=sct)
    print("semantic_change_hf ", semantic_change_hf)


def cross_lingual_sc_sv():
    language = 'sv'
    k = 14
    # rt = 0.75
    # sct = 0.64
    rt = 0.85
    sct = 0.65
    # keywords = ['mus', 'känga', 'moln', 'minne', 'nät', 'data', 'utfodra', 'band']
    # keywords = ["piga", "gay", "ungkarl", "gift", "fast", "kind", "bad", "mail", 'dokument']
    keywords = ['mus']
    print(keywords)
    pmodel = 'bert-base-multilingual-cased'  # 'bert-base-uncased'
    corpus1 = './corpus/{}/crosslingual/semeval/data.pkl'.format(language)
    corpus2 = './corpus/{}/crosslingual/wiki/data.pkl'.format(language)
    # corpus1 = './corpus/{}/crosslingual-loss-eva/semeval/data.pkl'.format(language)
    # corpus2 = './corpus/{}/crosslingual-loss-eva/wiki/data.pkl'.format(language)
    semantic_change_hf, semantic_change_hf_lf =\
        cl.run_cloud(keywords, pmodel, corpus1, corpus2, language,
                     lan_emb=None, k=k, separate=True, rt=rt, sct=sct)
    print("semantic_change_hf ", semantic_change_hf)


if __name__ == '__main__':
    cross_lingual_task()
