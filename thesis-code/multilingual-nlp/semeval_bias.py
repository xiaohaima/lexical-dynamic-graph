
# import cloudprior as cl
# from cloudprior import CloudCluster
import utils
import rundata as rd
import treesim as ts
import semantic_tree as st


def semeval_bias_statistic(language):
    keywords_file = './corpus/{}/semeval_task/keywords.pkl'.format(language)
    keywords = utils.load_from_disk(keywords_file)
    static_embeds1 = rd.init_static_embedding()
    static_embeds2 = rd.init_static_embedding()
    for word in keywords:
        cache_path = './data/{}/words/{}/'.format(language, word)
        rte = rd.check_cache(cache_path)
        if rte is None:
            print("... {} missing cache cloud ...".format(word))
        else:
            print("... Use cache cloud ...")
            cloud1, stat_emb1, cloud2, stat_emb2 = rte
            # static_embeds1 = rd.init_static_embedding()
            # static_embeds2 = rd.init_static_embedding()
            s_embeds1 = st.update_static_embeds(static_embeds1, stat_emb1)
            s_embeds2 = st.update_static_embeds(static_embeds2, stat_emb2)
            # ts.cross_lingual_embedding_mean_sim(s_embeds1, s_embeds2)
            # return
    print("semeval_bias_statistic for language {}".format(language))
    ts.cross_lingual_embedding_mean_sim(s_embeds1, s_embeds2)


def semeval_bias():
    languages = ['en','de', 'la','sv']
    # sample_sct = [1]
    for language in languages:
        semeval_bias_statistic(language)
        

if __name__ == '__main__':
    semeval_bias()



