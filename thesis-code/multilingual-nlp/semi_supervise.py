import numpy as np
import cloudprior as cl
import utils
import codecs
from random import sample
import rundata as rd
import semantic_tree as st
import semeval_task2 as st2
from scipy.spatial import distance
from cloudprior import CloudCluster, get_cloud_data, neighbor_similarity
from sklearn.metrics import accuracy_score, precision_recall_fscore_support
from scipy.stats import spearmanr
# from main import evaluate_task1


def semeval_rank(hf1, hf2, rt_clusters1, rt_clusters2, lf1, lf2,
                 sct, static_embeds, usenf=True, uself=False):
    point_num1 = 0
    point_num2 = 0
    for hf in rt_clusters1:
        point_num1 += hf.point_num
    for hf in rt_clusters2:
        point_num2 += hf.point_num
    print("point_num1 {} point_num2 {}".format(point_num1, point_num2))

    hflf1_num = 0
    hflf2_num = 0
    scenes = []
    for hf in hf1:
        hflf1_num += hf.point_num
        st2.eva_hf_with_scenes(hf, 1, scenes, sct, static_embeds)

    for hf in hf2:
        hflf2_num += hf.point_num
        st2.eva_hf_with_scenes(hf, 2, scenes, sct, static_embeds)
    
    if uself:
        for lf in lf1:
            hflf1_num += lf.point_num
            st2.eva_hf_with_scenes(lf, 1, scenes, sct, static_embeds)

        for lf in lf2:
            hflf2_num += lf.point_num
            st2.eva_hf_with_scenes(lf, 2, scenes, sct, static_embeds)
    print("hf1_num {} hf2_num {}".format(hflf1_num, hflf2_num))
    print("scenes: ", scenes)
    if usenf:
        p_nums = [point_num1, point_num2]
        p = np.zeros((2, len(scenes) + 1))
        p[0, len(scenes)] = (point_num1 - hflf1_num) / point_num1
        p[1, len(scenes)] = (point_num2 - hflf2_num) / point_num2
    else:
        p_nums = [hflf1_num, hflf2_num]
        p = np.zeros((2, len(scenes)))
    for scene_id, scene in enumerate(scenes):
        clus = scene['clus']
        s = [0, 0]
        for clu_tuple in clus:
            time, clu = clu_tuple
            timeid = time-1
            s[timeid] += clu.point_num
        p[0, scene_id] = s[0] / p_nums[0]
        p[1, scene_id] = s[1] / p_nums[1]
    print("probability distribution ", p)
    jsd = distance.jensenshannon(p[0, :], p[1, :])
    print("jensenshannon divergence ", jsd)
    return jsd


def semeval_tasks(keywords, rt, sct, k,):
    # keywords = positive+negative
    scs = []
    ranks = []
    for keyword in keywords:
        cache_path = './data/{}/words/{}/'.format(language, keyword)
        stat_emb1 = utils.load_from_disk(cache_path + 'stat_emb1')
        stat_emb2 = utils.load_from_disk(cache_path + 'stat_emb2')
        static_embeds = rd.init_static_embedding()
        static_embeds = st.update_static_embeds(static_embeds, stat_emb1)
        static_embeds = st.update_static_embeds(static_embeds, stat_emb2)

        hf1, hf2, rt_clusters1, rt_clusters2, lf1, lf2 = st2.load_clusters(language, rt, sct, k, keyword)
        sc, _, _ = cl.semantic_change_cloud_prior(hf1, lf1, hf2, lf2,
                                                  static_embeds, rt=0.99, sct=sct)
        rank = semeval_rank(hf1, hf2, rt_clusters1, rt_clusters2, lf1, lf2,
                            sct, static_embeds, usenf=True, uself=False)
        scs.append(sc)
        ranks.append(rank)
    
    return scs, ranks

def get_hyperp_sample(language):
    # sample_k = [10, 12, 14]
    sample_k = [10, 12, 14]
    if language == 'en':
        sample_rt = [0.65, 0.66, 0.67, 0.68, 0.69, 0.70, 0.71, 0.72, 0.73, 0.74, 0.75]
        sample_sct = [0.55, 0.56, 0.57, 0.58, 0.59, 0.60, 0.61, 0.62, 0.63, 0.64, 0.65]
    elif language == 'de':
        sample_rt = [0.76, 0.77, 0.78, 0.79, 0.80, 0.81, 0.82, 0.83, 0.84, 0.85]
        sample_sct = [0.57, 0.58, 0.59, 0.60, 0.61, 0.62, 0.63, 0.64, 0.65, 0.66, 0.67, 0.68,]
    elif language == 'sv':
        sample_rt = [0.76, 0.77, 0.78, 0.79, 0.80, 0.81, 0.82, 0.83, 0.84, 0.85]
        sample_sct = [0.60, 0.61, 0.62, 0.63, 0.64, 0.65,
                      0.66, 0.67, 0.68, 0.69, 0.70, 0.71, 0.72, 0.73, 0.74, 0.75,]
    else:
        # rt, sct configuration for language ['la']
        sample_rt = [0.82, 0.83, 0.84, 0.85,
                     0.86, 0.87, 0.88, 0.89]
        sample_sct = [ 0.82, 0.83, 0.84, 0.85,
                      0.86, 0.87, 0.88, 0.89]
    
    # sample_k = [14]
    # sample_rt = [0.69]
    # sample_sct = [0.60]
    return sample_k, sample_rt, sample_sct


def get_semi_results(language):
    # keywords_file = './corpus/{}/semeval_task/keywords.pkl'.format(language)
    # keywords = utils.load_from_disk(keywords_file)
    gold_file = './corpus/{}/semeval_task/binary.txt'.format(language)
    with codecs.open(gold_file, 'r', 'utf-8') as truth_file:
        btruth = {line.strip().split('\t')[0]:int(line.strip().split('\t')[1]) for line in truth_file}
    print(btruth)
    gold_file = './corpus/{}/semeval_task/graded.txt'.format(language)
    with codecs.open(gold_file, 'r', 'utf-8') as truth_file:
        rtruth = {line.strip().split('\t')[0]:float(line.strip().split('\t')[1]) for line in truth_file}
    print(rtruth)
    positive = []
    negative = []
    for target in btruth:
        if btruth[target] == 1:
            positive.append(target)
        elif btruth[target] == 0:
            negative.append(target)
    p_sample = sample(positive, 3)
    n_sample = sample(negative, 3)
    print(p_sample)
    print(n_sample)
    keywords_sample = p_sample+n_sample
    gold_scs=[]
    gold_ranks=[]
    for w in keywords_sample:
        gold_scs.append(btruth[w])
        gold_ranks.append(rtruth[w])

    if language == 'en':
        keywords_sample = [w.split('_')[0] for w in keywords_sample]
    sample_k, sample_rt, sample_sct = get_hyperp_sample(language)
    semi_grid = []
    for k in sample_k:
        for rt in sample_rt:
            for sct in sample_sct:
                if sct > rt:
                    continue
                pred_scs, pred_ranks = semeval_tasks(keywords_sample, rt, sct, k)
                b_acc = accuracy_score(gold_scs, pred_scs)
                r_rho, _ = spearmanr(pred_ranks, gold_ranks, nan_policy='raise')
                semi_grid.append({'b_acc':b_acc, 'r_rho':r_rho,
                                  'k':k, 'rt':rt, 'sct':sct,})
                
    semi_results = {'keywords_sample':keywords_sample, 'positive':positive,
                    'negative':negative, 'semi_grid':semi_grid}
    return semi_results


def get_test_acc(language, b_accs, grid):
    valid_results = []
    max_b = np.max(b_accs)
    k_count = {14:0, 12:0, 10:0,}
    k_rt = {14:[], 12:[], 10:[],}
    k_sct = {14:[], 12:[], 10:[],}
    for result in grid:
        if abs(result['b_acc'] - max_b) < 0.0001:
            k = result['k']
            rt = result['rt']
            sct = result['sct']
            k_count[k] = k_count[k]+1
            k_rt[k].append(rt)
            k_sct[k].append(sct)
            acc_path = './data/{}/semeval_task/sc_b/k{}/acc_result.pkl'.format(language, k)
            test_accs = utils.load_from_disk(acc_path)
            for test_acc_ in test_accs:
                if abs(test_acc_['sct'] - sct) < 0.001 and abs(test_acc_['rt'] - rt) < 0.001:
                    print('one max binary classification acc ', test_acc_['acc'], ' config ', result)
                    valid_results.append({'acc':test_acc_['acc'], 'config':result})
    
    valids = [valid['acc'] for valid in valid_results]
    print(" >>>>>> max config : ", valid_results[np.argmax(valids)])

    k_counts = [k_count[14], k_count[12], k_count[10]]
    k_sample = [14, 12, 10]
    t_k = k_sample[np.argmax(k_counts)]
    t_rt = round(np.mean(k_rt[t_k]), 2)
    t_sct = round(np.mean(k_sct[t_k]), 2)
    acc_path = './data/{}/semeval_task/sc_b/k{}/acc_result.pkl'.format(language, t_k)
    test_accs = utils.load_from_disk(acc_path)
    for test_acc_ in test_accs:
        if abs(test_acc_['sct'] - t_sct) < 0.001 and abs(test_acc_['rt'] - t_rt) < 0.001:
            print('  :::<<<<<<<++++>>>>> select t classification acc ', test_acc_['acc'],
                  ' config k {} sct {} rt {}'.format(t_k, t_sct, t_rt))
    



def get_test_rho(language, r_rhos, grid, usenf=True, uself=False):
    valid_results = []
    max_r = np.max(r_rhos)
    k_count = {14:0, 12:0, 10:0,}
    k_rt = {14:[], 12:[], 10:[],}
    k_sct = {14:[], 12:[], 10:[],}
    for result in grid:
        if abs(result['r_rho'] - max_r) < 0.0001:
            k = result['k']
            rt = result['rt']
            sct = result['sct']
            k_count[k] = k_count[k]+1
            k_rt[k].append(rt)
            k_sct[k].append(sct)
            save_path = "./data/semevalrank/{}/k{}/usenf_{}-uself_{}/".format(language, k, usenf, uself)
            test_rhos = utils.load_from_disk(save_path+'sc_ranking_results.pkl')
            for test_rho_ in test_rhos:
                if abs(test_rho_['sct'] - sct) < 0.001 and abs(test_rho_['rt'] - rt) < 0.001:
                    print('one max coorelation ', test_rho_['correlation'], ' config ', result)
                    valid_results.append({'correlation':test_rho_['correlation'], 'config':result})
    
    valids = [valid['correlation'] for valid in valid_results]
    print(" >>>>>> max config : ", valid_results[np.argmax(valids)])

    k_counts = [k_count[14], k_count[12], k_count[10]]
    k_sample = [14, 12, 10]
    t_k = k_sample[np.argmax(k_counts)]
    t_rt = round(np.mean(k_rt[t_k]), 2)
    t_sct = round(np.mean(k_sct[t_k]), 2)
    save_path = "./data/semevalrank/{}/k{}/usenf_{}-uself_{}/".format(language, k, usenf, uself)
    test_rhos = utils.load_from_disk(save_path+'sc_ranking_results.pkl')
    for test_rho_ in test_rhos:
        if abs(test_rho_['sct'] - t_sct) < 0.001 and abs(test_rho_['rt'] - t_rt) < 0.001:
            print('  :::<<<<<<<++++>>>>> select t correlation ', test_rho_['correlation'],
                  ' config k {} sct {} rt {}'.format(t_k, t_sct, t_rt))


def semi_semeval(language, usenf=True, uself=False):
    semi_results_path = './data/semi/{}/semi_results/'.format(language)
    semi_results = utils.load_from_disk(semi_results_path+'semi_results.pkl')
    if semi_results == None:
        semi_results = get_semi_results(language)
        utils.save_to_disk(semi_results_path+'semi_results.pkl', semi_results)
        utils.save_as_txt(semi_results_path+'semi_results.txt', str(semi_results))
    grid = semi_results['semi_grid']
    b_accs = [result['b_acc'] for result in grid]
    r_rhos = [result['r_rho'] for result in grid]
    print('b_accs ', b_accs)
    print('r_rhos ', r_rhos)
    print('b_acc max', grid[np.argmax(b_accs)])
    print('r_rho max', grid[np.argmax(r_rhos)])

    get_test_acc(language, b_accs, grid)
    get_test_rho(language, r_rhos, grid, usenf=usenf, uself=uself)


if __name__ == '__main__':
    language = 'sv'
    semi_semeval(language)



