import utils
import simplemma
import bertcloud as bc
import cloudprior as cp
import numpy as np
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from simplemma import text_lemmatizer
from sklearn.metrics.cluster import normalized_mutual_info_score, adjusted_mutual_info_score, mutual_info_score
from tqdm import tqdm
import cloudprior_ft as cft
import semeval as sev
import codecs
import semeval_task2 as st2
import rundata as rd
import semantic_tree as st
from scipy.spatial import distance


def word_in_tokens(word, lemma_tokens):
    word_in = False
    new_lemma_tokens = []
    for i, token in enumerate(lemma_tokens):
        # if token == 'boka' or token == 'boken' or token == 'lärobok':
        # if 'bok' in token:
        #     lemma_tokens[i] = 'bok'
        # if word == lemma_tokens[i]:
        #     return True, lemma_tokens
        if word in token and word != token:
            toks = token.split(word)
            new_lemma_tokens += toks
            new_lemma_tokens.append(word)
            word_in = True
            continue

        if word == 'leda' and (token == 'liva' or token == 'leva') and ('leda' not in " ".join(lemma_tokens)):
            new_lemma_tokens.append('leda')
            word_in = True
            # print("Word {} Sen: {}".format(word, " ".join(new_lemma_tokens)))
            continue
        
        if word == token:
            word_in = True
        new_lemma_tokens.append(token)
    # if not word_in and word == 'leda':
    #     print("Word {} Sen: {}".format(word, " ".join(new_lemma_tokens)))
    return word_in, new_lemma_tokens


def load_raw_corpus(language, case=False):
    base = './data/sysdata/{}/raw'.format(language)
    files = utils.all_files(base)
    data = {}
    for senfile in files:
        filename = senfile.split('/')[-1].split('.')[0]
        word = filename.split('_')[0]
        label = filename.split('_')[1]
        if word not in data:
            data[word] = {}
        data[word][label] = []
        if word == 'leda' and label == '2':
            aa = 0
        with open(senfile, "r", encoding="utf-8") as corpus_data:
            corpus_raw = corpus_data.read().split('\n')
        for sen in corpus_raw:
            if len(sen) < 60:
                continue
            if sen[0] == '(' and sen[-1] == ')':
                continue
            if sen[0] == '"' and sen[-1] == '"':
                sen = sen[1:len(sen)-1]
            # lsen = sen.casefold() if case else sen
            # simplemma will handle lemma automatically
            lemma_tokens = text_lemmatizer(sen, lang=language)
            word_in, lemma_tokens = word_in_tokens(word, lemma_tokens)
            if (not word_in):
                continue
            sentence = " ".join(lemma_tokens)
            sen_sims = [utils.sen_similarity(sen, sentence) for sen in data[word][label]]
            # if len(sen_sims) > 0 and max(sen_sims) > 0.6:
            #     print("Repetion {} sen: {}".format(max(sen_sims), sen))
            #     continue
            if word in sentence:
                data[word][label].append(sentence)
        print('word {} label {} len {}'.format(word, label, len(data[word][label])))

    datapath = './data/sysdata/{}/corpus.pkl'.format(language)
    utils.save_to_disk(datapath, data)


def load_corpus(language):
    datapath = './data/sysdata/{}/corpus.pkl'.format(language)
    data = utils.load_from_disk(datapath)
    return data


def gen_cloud(language):
    prbert_model = 'bert-base-multilingual-cased'
    data = load_corpus(language)
    for word in data:
        # if word != 'light':
        #     continue
        s1 = data[word]['1']
        s2 = data[word]['2']
        cloud1, stat_emb1 = bc.bert_cloud(word, s1, prbert_model)
        cloud2, stat_emb2 = bc.bert_cloud(word, s2, prbert_model)
        static_embeds1 = utils.init_static_embedding()
        static_embeds2 = utils.init_static_embedding()
        utils.update_static_embeds(static_embeds1, stat_emb1)
        utils.update_static_embeds(static_embeds2, stat_emb2)
        static_embeds = utils.merge_static_embeds(static_embeds1, static_embeds2)
        datapath = './data/sysdata/{}/words/{}/'.format(language, word)
        utils.save_to_disk(datapath+'cloud1.pkl', cloud1)
        utils.save_to_disk(datapath+'cloud2.pkl', cloud2)
        utils.save_sendata_as_txt(datapath+'s1.txt', s1)
        utils.save_sendata_as_txt(datapath+'s2.txt', s2)
        utils.save_to_disk(datapath+'static_embeds.pkl', static_embeds)


def plt_cloud(language):
    word = 'kort' # 'mouse', book, bank # crane, light
    # language='en'
    datapath = './data/sysdata/{}/words/{}/'.format(language, word)
    cloud1 = utils.load_from_disk(datapath+'cloud1.pkl')
    cloud2 = utils.load_from_disk(datapath+'cloud2.pkl')

    cloud = cloud1 + cloud2
    X = np.array(cloud)
    pca = PCA(n_components=2)
    pca = pca.fit(X)
    X_dr = pca.transform(X)

    plt.figure(figsize=(10, 8))
    plt.scatter(X_dr[0:len(cloud1), 0], X_dr[0:len(cloud1), 1], c='g', s=8)
    plt.scatter(X_dr[len(cloud1):, 0], X_dr[len(cloud1):, 1], c='r', s=8)
    # plt.legend()
    # plt.title('title')
    plt.savefig(datapath+'cloud.pdf')
    plt.savefig(datapath+'cloud.png')
    plt.show()


def cloud_distance_euclidean(cloud, static_embeds, keyword, k=14):
    nbs_cloud = []
    pairs = []
    sim_matrix = np.ones((len(cloud), len(cloud)), dtype='float32')
    clouds = []
    for i, point in enumerate(cloud):
        clouds.append((i, point))
    for point in tqdm(clouds):
        i, emb = point
        nbs = cp.clear_sim_words(keyword, emb, k, static_embeds)
        for j, nbs_ in enumerate(nbs_cloud):
            sim = utils.cosine_similarity(cloud[i], cloud[j])
            sim_matrix[i, j] = sim
            sim_matrix[j, i] = sim
        nbs_cloud.append(nbs)

    return sim_matrix, nbs_cloud


def cloud_clusters_reduce(clusters, cluster_simdist, sim_matrix, cloud, static_embeds, keyword, rt=0.9, drop=False):
    drop_semantic = []
    print("cluster_simdist shape", cluster_simdist.shape)
    t_ub = 0
    t_lb = 0
    if len(clusters) == 2:
        return clusters, cluster_simdist, drop_semantic, t_ub, t_lb
    rt_clusters = clusters
    rt_cluster_simdist = cluster_simdist
    while np.max(cluster_simdist) > rt:

        clusters, rip_dist, drop_st =\
            cp.merge_cluster_once(clusters, cluster_simdist, cloud, static_embeds, keyword, drop=drop)

        cluster_simdist = cp.clusters_distances(clusters, sim_matrix, rip_dist, static_embeds)
        drop_semantic += drop_st
        if len(clusters) == 3:
            t_ub = np.max(cluster_simdist)
        if len(clusters) == 2:
            rt_clusters = clusters
            rt_cluster_simdist = cluster_simdist
            t_lb = np.max(cluster_simdist)
    if len(clusters) > 2:
        return clusters, cluster_simdist, drop_semantic, t_ub, t_lb
    return rt_clusters, rt_cluster_simdist, drop_semantic, t_ub, t_lb


def gen_cloud_distance(cloud, static_embeds, keyword, language, time,
                       k=12, save_nbs=True, usecache=True, syn=''):
    print(" cloud points ", len(cloud))
    matrix_path = './data/sysdata/{}/words/{}/cloud_sim_matrix/{}k{}_cloud_matrix_t{}.npy'.format(language, keyword, syn, k, time)
    nbs_path = './data/sysdata/{}/words/{}/cloud_sim_matrix/{}k{}_cloud_nbs_t{}.pkl'.format(language, keyword, syn, k, time)
    if usecache and utils.exists(matrix_path):
        print(" cloud points distance matrix exists ")
        return cp.load_cloud_prior(matrix_path, nbs_path, load_nbs=save_nbs)

    sim_matrix, nbs_cloud = cp.cloud_distance_mp(cloud, static_embeds, keyword, k=k)
    utils.create_filepath_dir(matrix_path)
    np.save(matrix_path, sim_matrix)
    # ts.plt_weights(sim_matrix)
    if save_nbs:
        utils.save_to_disk(nbs_path, nbs_cloud)
    return sim_matrix, nbs_cloud


def cloud_cluster_rt(sim_matrix, nbs_cloud, cloud, static_embeds, keyword, rt, k, time, language, usecache=True):
    # rt_clusters_pth = './data/sysdata/{}/words/{}/cloud_sim_matrix/rt{}/k{}_rt_clusters_t{}.pkl'. \
    #     format(language, keyword, rt, k, time)
    # rt_cluster_simdist_pth = './data/sysdata/{}/words/{}/cloud_sim_matrix/rt{}/k{}_rt_cluster_simdist_t{}.pkl'. \
    #     format(language, keyword, rt, k, time)
    # clusters = utils.load_from_disk(rt_clusters_pth)
    # cluster_simdist = utils.load_from_disk(rt_cluster_simdist_pth)
    # if usecache and clusters is not None and cluster_simdist is not None:
    #     print(" Load cached rt clusters for word ".format(keyword))
    #     return clusters, cluster_simdist

    clusters = []
    for i, point in enumerate(cloud):
        clusters.append(cp.CloudCluster(point, nbs_cloud[i], [i], k, [1]))

    cluster_simdist = sim_matrix.copy()
    row, col = np.diag_indices_from(cluster_simdist)
    cluster_simdist[row, col] = 0
    # ts.plt_weights(sim_matrix - cluster_simdist)

    clusters, cluster_simdist, _, t_ub, t_lb = \
        cloud_clusters_reduce(clusters, cluster_simdist, sim_matrix,
                              cloud, static_embeds, keyword, rt=rt)

    # utils.save_to_disk(rt_clusters_pth, clusters)
    # utils.save_to_disk(rt_cluster_simdist_pth, cluster_simdist)
    return clusters, cluster_simdist, t_ub, t_lb


def subsmantic_from_clusters(clusters, cluster_simdist, sim_matrix,
                             cloud, static_embeds, keyword, sct=0.6, drop=True, exc=5):
    hf_clusters = []
    lf_semantic = []
    for i, clu in enumerate(clusters):
        # generaly hf_t is relate to the number of cloud points and the rt threshold
        # the higher points number the higher hf_t,
        # the lower rt threshold the higher hf_t.
        # it's hard to make a single best choice for different corpus.
        # hf_t = 5 if len(cloud) > 300 else 3
        # hf_t = len(cloud)//100 if len(cloud)//100 > 5 else 5
        hf_t = exc
        if len(clu.points) >= hf_t:
            hf_clusters.append(clu)
            print(" Pending hf cid {} points {} ".format(i, clu.points))
            print(clu.c_neighbors)
            # print("sim ", clu.sim2c)
        elif len(clu.points) > 1:
            lf_semantic.append(clu)
    if len(hf_clusters) == 0:
        hf_clusters = []
        lf_semantic = []
        for i, clu in enumerate(clusters):
             if len(clu.points) >= 2:
                 hf_clusters.append(clu)

    print("length hf_clusters before", len(hf_clusters))
    print("length lf_semantic before", len(lf_semantic))
    tmp_pth = './data/sysdata/la/words/{}/cluster_tmp/rt0.88/'. \
            format(keyword)
    utils.save_to_disk(tmp_pth+'hf_clusters.pkl', hf_clusters)
    utils.save_to_disk(tmp_pth+'lf_clusters.pkl', lf_semantic)
    hf_cluster_simdist = cp.clusters_distances(hf_clusters, sim_matrix, None, static_embeds)

    hf_semantic, polysemy_simdist, drop_semantic, t_ub, t_lb = \
        cloud_clusters_reduce(hf_clusters, hf_cluster_simdist, sim_matrix,
                              cloud, static_embeds, keyword, rt=sct, drop=drop)
    # ts.plt_weights(polysemy_simdist)
    lf_semantic += drop_semantic

    print("length lf_semantic after", len(lf_semantic))
    print("hf_semantic len", len(hf_semantic))
    for i, clu in enumerate(hf_semantic):
        print(">>>>> high cid {} points {} ".format(i, clu.points))
        print(clu.c_neighbors)

    for i, clu in enumerate(lf_semantic):
        print(">>>>> low cid {} points {} ".format(i, clu.points))
        print(clu.c_neighbors)

    # ts.plt_weights(polysemy_simdist)
    print(">>>>>>>  <<<<<<<<<< ", polysemy_simdist)
    utils.save_to_disk(tmp_pth+'hf_semantic.pkl', hf_semantic)
    utils.save_to_disk(tmp_pth+'lf_semantic.pkl', lf_semantic)
    return hf_semantic, lf_semantic


def purity_score(hf_semantic, cloud1, cloud2):
    assert len(hf_semantic) == 2
    l1 = len(cloud1)
    l2 = len(cloud2)
    clu1 = hf_semantic[0]
    clu2 = hf_semantic[1]
    pc1l1 = sum([1 if id < l1 else 0 for id in clu1.points])
    pc1l2 = sum([1 if id >= l1 else 0 for id in clu1.points])
    pc2l1 = sum([1 if id < l1 else 0 for id in clu2.points])
    pc2l2 = sum([1 if id >= l1 else 0 for id in clu2.points])

    score1 = (pc1l1 + pc2l2) / (l1 + l2)
    score2 = (pc1l2 + pc2l1) / (l1 + l2)
    scores = [score1, score2]
    print('scores ', scores)
    print('purity_score ', max(scores))
    return max(scores)


def purity_score_in_clus(hf_semantic, cloud1, cloud2):
    assert len(hf_semantic) == 2
    l1 = len(cloud1)
    l2 = len(cloud2)
    clu1 = hf_semantic[0]
    clu2 = hf_semantic[1]
    cl1 = len(clu1.points)
    cl2 = len(clu2.points)
    pc1l1 = sum([1 if id < l1 else 0 for id in clu1.points])
    pc1l2 = sum([1 if id >= l1 else 0 for id in clu1.points])
    pc2l1 = sum([1 if id < l1 else 0 for id in clu2.points])
    pc2l2 = sum([1 if id >= l1 else 0 for id in clu2.points])

    clu1_p = [pc1l1/cl1, pc1l2/cl1]
    clu2_p = [pc2l1/cl2, pc2l2/cl2]

    clu1_pred = np.argmax(clu1_p)
    clu2_pred = np.argmax(clu2_p)

    if clu1_pred == clu2_pred:
        return 0

    return (max(clu1_p) + max(clu2_p))/2


def nmi_score(hf_semantic, cloud1, cloud2):
    l1 = len(cloud1)
    l2 = len(cloud2)
    label = [0 for id in range(l1)] + [1 for id in range(l1, l1+l2)]
    clu1 = hf_semantic[0]
    clu2 = hf_semantic[1]
    pl1c1 = [0 if id in clu1.points else 2 for id in range(l1)]
    pl1c2 = [0 if id in clu2.points else 2 for id in range(l1)]

    pl2c1 = [1 if id in clu1.points else 2 for id in range(l1, l1+l2)]
    pl2c2 = [1 if id in clu2.points else 2 for id in range(l1, l1+l2)]

    score1 = normalized_mutual_info_score(pl1c1+pl2c2, label)
    score2 = normalized_mutual_info_score(pl1c2+pl2c1, label)
    scores = [score1, score2]
    print('scores ', scores)
    print('nmi_score ', max(scores))
    return max(scores)


def filter_acc(hf_semantic, cloud1, cloud2, scloud):
    l1 = len(cloud1)
    l2 = len(cloud2)
    l3 = len(scloud)
    clu1 = hf_semantic[0]
    clu2 = hf_semantic[1]
    filtered = [0 if id in clu1.points or id in clu2.points else 1 for id in range(l1+l2, l1+l2+l3)]

    acc = sum(filtered)/l3
    return acc



def syn_cloud(cloud1, cloud2, static_embeds):
    cloud = []
    index_voc, voc_index, embeddings = static_embeds
    mixture_n = 5
    idx1 = np.random.randint(0, len(cloud1), mixture_n)
    idx2 = np.random.randint(0, len(cloud2), mixture_n)

    c = np.mean(np.array(cloud1+cloud2), axis=0)
    pendings = []
    for eid in embeddings:
        sim = utils.cosine_similarity(c, embeddings[eid])
        if sim < 0.5:
            pendings.append(embeddings[eid])
    noise_n = 10
    idx3 = np.random.randint(0, len(pendings), noise_n)

    noise_cloud = [(c+pendings[id])/2 for id in idx3]
    mixture_cloud = [(cloud1[idx1[i]] + cloud2[idx2[i]])/2 for i in range(len(idx1))]
    cloud = noise_cloud + mixture_cloud
    sims = [utils.cosine_similarity(c, e) for e in cloud]
    print(sims)
    return cloud


def fine_tuning_cloud_t(cloud, static_embeds, keyword, language, time, k,
                        usecache=True, subsample=True, rt=0.73, sct=0.6, exc=5):

    sim_matrix, nbs_cloud = \
        gen_cloud_distance(cloud, static_embeds, keyword, language,
                           time=time, k=k, usecache=False, syn='syn_')
    
    # sim_matrix, nbs_cloud = cloud_distance_euclidean(cloud, static_embeds, keyword, k=k)

    clusters, cluster_simdist, t_ub, t_lb =\
        cloud_cluster_rt(sim_matrix, nbs_cloud, cloud, static_embeds, keyword,
                         rt, k, time, language, usecache=False)
    print("Reduce clusters num: ", len(clusters))

    hf_semantic, lf_semantic = \
        subsmantic_from_clusters(clusters, cluster_simdist, sim_matrix,
                                 cloud, static_embeds, keyword, sct=sct, exc=exc)

    return hf_semantic, lf_semantic, t_ub, t_lb


def fine_tuning_cloud_t0(cloud, static_embeds, keyword, language, time, k,
                  usecache=True, subsample=True, rt=0.73, sct=0.6, syn='syn_', drop=True):

    sim_matrix, nbs_cloud = \
        gen_cloud_distance(cloud, static_embeds, keyword, language,
                           time=time, k=k, usecache=False, syn=syn)
    
    # sim_matrix, nbs_cloud = cloud_distance_euclidean(cloud, static_embeds, keyword, k=k)

    clusters, cluster_simdist, t_ub, t_lb =\
        cloud_cluster_rt(sim_matrix, nbs_cloud, cloud, static_embeds, keyword,
                         rt, k, time, language, usecache=False)
    print("Reduce clusters num: ", len(clusters))

    hf_semantic, lf_semantic = \
        subsmantic_from_clusters(clusters, cluster_simdist, sim_matrix,
                                 cloud, static_embeds, keyword, sct=sct, drop=drop)

    return hf_semantic, lf_semantic, t_ub, t_lb


def fine_tuning_cloud_t1(cloud, static_embeds, keyword, language, time, k,
                         usecache=True, subsample=True, rt=0.73, sct=0.6):
    usecache = True
    # if keyword == 'light':
    #     usecache = False
    sim_matrix, nbs_cloud = \
        gen_cloud_distance(cloud, static_embeds, keyword, language, time=time, k=k, usecache=usecache)
    
    # sim_matrix, nbs_cloud = cloud_distance_euclidean(cloud, static_embeds, keyword, k=k)

    clusters, cluster_simdist, t_ub, t_lb =\
        cloud_cluster_rt(sim_matrix, nbs_cloud, cloud, static_embeds,
                         keyword, rt, k, time, language, usecache=True)

    return clusters, [], t_ub, t_lb

def fine_tuning_cloud(language):
    data = load_corpus(language)
    stat = []
    k = 14
    rt = 0.30
    sct = 0.30
    t_ubs = []
    t_lbs = []
    csims = []
    for word in data:
        datapath = './data/sysdata/{}/words/{}/'.format(language, word)
        cloud1 = utils.load_from_disk(datapath+'cloud1.pkl')
        cloud2 = utils.load_from_disk(datapath+'cloud2.pkl')
        static_embeds = utils.load_from_disk(datapath+'static_embeds.pkl')
        cloud = cloud1 + cloud2
        hf_semantic, lf_semantic, t_ub, t_lb = \
            fine_tuning_cloud_t1(cloud, static_embeds, word, language,
                                 time='1', k=k, rt=rt, sct=sct)
        print('hf_semantic1', hf_semantic)
        print('lf_semantic1', lf_semantic)
        csim = utils.cosine_similarity(hf_semantic[0].center, hf_semantic[1].center)
        # print('t_ub', t_ub)
        # print('t_lb', t_lb)
        p = purity_score(hf_semantic, cloud1, cloud2)
        nmi = nmi_score(hf_semantic, cloud1, cloud2)
        clu_ps = purity_score_in_clus(hf_semantic, cloud1, cloud2)
        stat.append((word, t_ub, t_lb, p, nmi, clu_ps, csim))
    print("stat ", stat)
    t_ubs = [item[1] for item in stat]
    t_lbs = [item[2] for item in stat]
    csims = [item[6] for item in stat]
    print("t_lbs max {} min {} mean {} std {}".format(max(t_lbs), min(t_lbs), np.mean(t_lbs), np.std(t_lbs)))
    print("t_ubs max {} min {} mean {} std {}".format(max(t_ubs), min(t_ubs), np.mean(t_ubs), np.std(t_ubs)))
    print("csims max {} min {} mean {} std {}".format(max(csims), min(csims), np.mean(csims), np.std(csims)))
    
    # rt = 0.75
    # sct = 0.60
    # for word in data:
    #     datapath = './data/sysdata/{}/words/{}/'.format(language, word)
    #     cloud1 = utils.load_from_disk(datapath+'cloud1.pkl')
    #     cloud2 = utils.load_from_disk(datapath+'cloud2.pkl')
    #     static_embeds = utils.load_from_disk(datapath+'static_embeds.pkl')
    #     s_cloud = syn_cloud(cloud1, cloud2, static_embeds)
    #     cloud = cloud1 + cloud2 + s_cloud

    #     hf_semantic, lf_semantic, t_ub, t_lb = \
    #         fine_tuning_cloud_t0(cloud, static_embeds, word, language,
    #                              time='1', k=k, rt=rt, sct=sct)
    #     print('hf_semantic1', hf_semantic)
    #     print('lf_semantic1', lf_semantic)
    #     facc = filter_acc(hf_semantic, cloud1, cloud2, s_cloud)
    #     print('facc', facc)

    # crane have over 0.85 clu accuracy.
    # rt = 0.85
    # sct = 0.65
    # exc = 3
    sample_sct = [0.57, 0.58, 0.59, 0.60, 0.61, 0.62, 0.63, 0.64, 0.65, 0.66, 0.67, 0.68, 0.69, 0.70]
    sample_rt = [0.62, 0.63, 0.64, 0.65, 0.66, 0.67, 0.68, 0.69, 0.70, 0.71, 0.72, 0.73, 0.74, 0.75, 0.76, 0.77, 0.78, 0.79,
                 0.80, 0.81, 0.82, 0.83, 0.84, 0.85]
    sample_exc = [2, 3, 4, 5]
    params = []
    for sct in sample_sct:
        for rt in sample_rt:
            if rt < sct:
                continue
            for exc in sample_exc:
                params.append((sct, rt, exc))

    # result = {}
    # for param in params:
    #     param = (0.60, 0.69, 5)
    #     sct, rt, exc = param
    #     stat = []
    #     for word in data:
    #         datapath = './data/sysdata/{}/words/{}/'.format(language, word)
    #         cloud1 = utils.load_from_disk(datapath+'cloud1.pkl')
    #         cloud2 = utils.load_from_disk(datapath+'cloud2.pkl')
    #         static_embeds = utils.load_from_disk(datapath+'static_embeds.pkl')
    #         cloud = cloud1 + cloud2

    #         hf_semantic, lf_semantic, t_ub, t_lb = \
    #             fine_tuning_cloud_t(cloud, static_embeds, word, language,
    #                                  time='1', k=k, rt=rt, sct=sct, exc=exc)
    #         print('hf_semantic1', hf_semantic)
    #         print('lf_semantic1', lf_semantic)
    #         if (len(hf_semantic) !=2):
    #             stat.append((word, 0, 0, 0))
    #             continue
    #         ps = purity_score(hf_semantic, cloud1, cloud2)
    #         nmi = nmi_score(hf_semantic, cloud1, cloud2)
    #         clu_ps = purity_score_in_clus(hf_semantic, cloud1, cloud2)
    #         stat.append((word, ps, nmi, clu_ps))

    #     print(stat)
    #     result[param] = stat
    #     break
    # resultpath = './data/sysdata/{}/search_rte.pkl'.format(language)
    # utils.save_to_disk(resultpath, result)


def load_resultpath(language='en'):
    resultpath = './data/sysdata/{}/search_rte.pkl'.format(language)
    result = utils.load_from_disk(resultpath)

    for param in result:
        stat = result[param]
        pss = []
        nmis = []
        clu_pss = []
        for item in stat:
            pss.append(stat[1])
            nmis.append(stat[2])
            clu_pss.append(stat[3])

    print(result)


def semeval_task1(language, k, sct, rt, separate=None):
    print("Run semeval cloud distance. Language {}".format(language))
    keywords_file = './corpus/{}/semeval_task/keywords.pkl'.format(language)
    keywords = utils.load_from_disk(keywords_file)
    # keywords = ["stroke"]
    print(keywords)
    pmodel = 'bert-base-multilingual-cased'  # 'bert-base-uncased'
    corpus1 = './corpus/{}/semeval_task/data1.pkl'.format(language)
    corpus2 = './corpus/{}/semeval_task/data2.pkl'.format(language)
    semantic_change_hf, semantic_change_hf_lf =\
        cft.run_cloud(keywords, pmodel, corpus1, corpus2, language,
                     lan_emb=None, k=k, separate=separate, rt=rt, sct=sct)
    acc_hf = sev.evaluate_semeval_task1(semantic_change_hf, language)
    acc_hf_lf = sev.evaluate_semeval_task1(semantic_change_hf_lf, language)
    print("semeval_task1 accuracy acc_hf {} acc_hf_lf {}".format(acc_hf, acc_hf_lf))
    return acc_hf, acc_hf_lf


def eva_scenes(hf, time, scenes, sct, static_embeds):
    # scene_clus = []
    nbs = hf.c_neighbors
    has_scene = False
    sim_max = sct
    simid = -1
    for sid, scene in enumerate(scenes):
        # clus = scene['clus']
        rep_clu = scene['rep_clu']
        nbs_ = rep_clu.c_neighbors
        sim = cp.neighbor_similarity(nbs, nbs_, static_embeds)
        if sim > sim_max:
            sim_max = sim
            simid = sid
            # clus.append((time, hf))
            # scene['clus'] = clus
            # has_scene = True
            # break
    if simid != -1:
        clus = scenes[simid]['clus']
        clus.append((time, hf))
        scenes[simid]['clus'] = clus
        if hf.point_num > scenes[simid]['rep_clu'].point_num:
           scenes[simid]['rep_clu'] = hf
        return
    
    new_scene = {'clus': [(time, hf)], 'rep_clu': hf}
    scenes.append(new_scene)


def semeval_attension_rank(rt_clusters1, rt_clusters2):
    point_num1 = 0
    point_num2 = 0
    for hf in rt_clusters1:
        point_num1 += hf.point_num
    for hf in rt_clusters2:
        point_num2 += hf.point_num
    
    att1 = [(hf.point_num/point_num1) for hf in rt_clusters1]
    att2 = [(hf.point_num/point_num2) for hf in rt_clusters2]

    att1 = [(hf.point_num/point_num1)*hf.center for hf in rt_clusters1]
    att2 = [(hf.point_num/point_num2)*hf.center for hf in rt_clusters2]
    rep1 = np.sum(np.array(att1), axis=0)
    rep2 = np.sum(np.array(att2), axis=0)
    sim = utils.cosine_similarity(rep1, rep2)
    return 1-sim


def semeval_rank(hf1, hf2, rt_clusters1, rt_clusters2, lf1, lf2,
                 sct, static_embeds, usenf=True, uself=False):
    # return semeval_attension_rank(rt_clusters1, rt_clusters2)
    p_num1 = 0
    p_num2 = 0
    rm_t = 3
    for hf in rt_clusters1:
        p_num1 += hf.point_num
    for hf in rt_clusters2:
        p_num2 += hf.point_num
    # rm_t1 = p_num1/10
    # rm_t2 = p_num2/10
    # rm_t1 = p_num1/30
    # rm_t2 = p_num2/30
    # rm_t1 = 0
    # rm_t2 = 0
    point_num1 = 0
    point_num2 = 0
    rm_t1 = rm_t
    rm_t2 = rm_t
    for hf in rt_clusters1:
        if hf.point_num < rm_t1:
            continue
        point_num1 += hf.point_num
    for hf in rt_clusters2:
        if hf.point_num < rm_t2:
            continue
        point_num2 += hf.point_num
    print("point_num1 {} point_num2 {}".format(point_num1, point_num2))
    scenes = []
    for hf in rt_clusters1:
        if hf.point_num < rm_t1:
            continue
        # st2.eva_hf_with_scenes(hf, 1, scenes, sct, static_embeds)
        eva_scenes(hf, 1, scenes, sct, static_embeds)

    for hf in rt_clusters2:
        if hf.point_num < rm_t2:
            continue
        # st2.eva_hf_with_scenes(hf, 2, scenes, sct, static_embeds)
        eva_scenes(hf, 2, scenes, sct, static_embeds)
    
    print("scenes: ", scenes)
    p_nums = [point_num1, point_num2]
    p = np.zeros((2, len(scenes)))
    for scene_id, scene in enumerate(scenes):
        clus = scene['clus']
        s = [0, 0]
        for clu_tuple in clus:
            time, clu = clu_tuple
            timeid = time-1
            s[timeid] += clu.point_num
        p[0, scene_id] = s[0] / p_nums[0]
        p[1, scene_id] = s[1] / p_nums[1]
    print("probability distribution ", p)
    jsd = distance.jensenshannon(p[0, :], p[1, :])
    print("jensenshannon divergence ", jsd)
    return jsd


def tuning_rank_semeval_task2(language, rt, sct, k):
    gold_file = './corpus/{}/semeval_task/graded.txt'.format(language)
    with codecs.open(gold_file, 'r', 'utf-8') as truth_file:
        rtruth = {line.strip().split('\t')[0]:float(line.strip().split('\t')[1]) for line in truth_file}
    keywords = [target for target in rtruth]
    if language == 'en':
        keywords = [w.split('_')[0] for w in keywords]
    print(keywords)
    rank_results = {}
    # keywords = ['plane', 'prop', 'tip', 'attack', 'chairman', 'tree', 'word']
    # keywords = ['prop', 'tip', 'tree', 'word']
    # keywords = ['tip', 'tree', 'word']
    for keyword in keywords:
        cache_path = './data/{}/words/{}/'.format(language, keyword)
        stat_emb1 = utils.load_from_disk(cache_path + 'stat_emb1')
        stat_emb2 = utils.load_from_disk(cache_path + 'stat_emb2')
        static_embeds = rd.init_static_embedding()
        static_embeds = st.update_static_embeds(static_embeds, stat_emb1)
        static_embeds = st.update_static_embeds(static_embeds, stat_emb2)
        hf1, hf2, rt_clusters1, rt_clusters2, lf1, lf2 = st2.load_clusters(language, rt, sct, k, keyword)
        print("keyword", keyword)
        jsd = semeval_rank(hf1, hf2, rt_clusters1, rt_clusters2, lf1, lf2,
                     sct, static_embeds, usenf=True, uself=False)
        rank_results[keyword] = jsd
    print("rank_results: ", rank_results)
    rho = st2.evaluate_semeval_task2(rank_results, language)



def test_normalized_mutual_info_score():
    # a1 = [0, 0, 1, 1, 2]
    # a2 = [1, 1, 0, 0, 0]
    a1 = [1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0]
    a2 = [1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0]
    print("mi", mutual_info_score(a1, a2))
    print("nmi", normalized_mutual_info_score(a1, a2))
    print("ami", adjusted_mutual_info_score(a1, a2))
    
if __name__ == '__main__':
    language='en'
    # load_raw_corpus(language=language)
    gen_cloud(language=language)
    plt_cloud(language=language)
    fine_tuning_cloud(language=language)
    # load_resultpath(language=language)
    # test_normalized_mutual_info_score()
    k = 14
    # la
    # # sct = 0.83
    # # rt = 0.83
    sct = 0.77 # 0.84
    rt = 0.85 # 0.84

    # de
    # sct = 0.62
    # rt = 0.78
    # sct = 0.63
    # rt = 0.75

    # sv
    # sct = 0.68
    # rt = 0.72
    # sct = 0.70
    # rt = 0.73

    # en
    # sct = 0.60
    # rt = 0.66
    # semeval_task1(language, k, sct, rt, separate=None)
    tuning_rank_semeval_task2(language, rt, sct, k)





