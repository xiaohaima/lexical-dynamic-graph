import numpy as np
import utils
import rundata as rd
import semeval_task2 as st2
import cloudprior as cp
from scipy.spatial import distance
from cloudprior import CloudCluster, get_cloud_data, neighbor_similarity
from semeval import evaluate_semeval_task1


def eva_clus_with_scenes(hf, time, scenes, sct, static_embeds):
    # scene_clus = []
    # nbs = hf.c_neighbors
    center = hf.center
    has_scene = False
    for scene in scenes:
        clus = scene['clus']
        for clu_tuple in clus:
            _, clu = clu_tuple
            # nbs_ = clu.c_neighbors
            center_ = clu.center
            # sim = neighbor_similarity(nbs, nbs_, static_embeds)
            sim = utils.cosine_similarity(center, center_)
            if sim > sct:
                clus.append((time, hf))
                scene['clus'] = clus
                has_scene = True
                break
        if has_scene:
            break
    if not has_scene:
        new_scene = {'clus': [(time, hf)]}
        scenes.append(new_scene)


def rank_from_euclidean_distance(hf1, hf2, rt_clusters1, rt_clusters2, e_sct, static_embeds, usenf=True):
    point_num1 = 0
    point_num2 = 0
    for hf in rt_clusters1:
        point_num1 += hf.point_num
    for hf in rt_clusters2:
        point_num2 += hf.point_num
    print("point_num1 {} point_num2 {}".format(point_num1, point_num2))
    
    hflf1_num = 0
    hflf2_num = 0
    scenes = []
    for hf in hf1:
        hflf1_num += hf.point_num
        eva_clus_with_scenes(hf, 1, scenes, e_sct, static_embeds)

    for hf in hf2:
        hflf2_num += hf.point_num
        eva_clus_with_scenes(hf, 2, scenes, e_sct, static_embeds)
    print("hf1_num {} hf2_num {}".format(hflf1_num, hflf2_num))
    print("scenes: ", scenes)
    if usenf:
        p_nums = [point_num1, point_num2]
        p = np.zeros((2, len(scenes) + 1))
        p[0, len(scenes)] = (point_num1 - hflf1_num) / point_num1
        p[1, len(scenes)] = (point_num2 - hflf2_num) / point_num2
    else:
        p_nums = [hflf1_num, hflf2_num]
        p = np.zeros((2, len(scenes)))
    for scene_id, scene in enumerate(scenes):
        clus = scene['clus']
        s = [0, 0]
        for clu_tuple in clus:
            time, clu = clu_tuple
            timeid = time-1
            s[timeid] += clu.point_num
        p[0, scene_id] = s[0] / p_nums[0]
        p[1, scene_id] = s[1] / p_nums[1]
    print("probability distribution ", p)
    jsd = distance.jensenshannon(p[0, :], p[1, :])
    print("jensenshannon divergence ", jsd)
#     rank_results[word] = jsd
    return jsd


def sc_from_euclidean_distance(hf1, hf2, cloud1, cloud2, e_sct):
    sim_m = np.zeros((len(hf1), len(hf2)))
    for i, clu1 in enumerate(hf1):
        for j, clu2 in enumerate(hf2):
            c1 = clu1.center
            c2 = clu2.center
            sim_m[i,j] = utils.cosine_similarity(c1, c2)
    print("sim matrix: ", sim_m)
    sc = cp.sc_from_similarity_matrix(sim_m, None, None, rt=0.99, sct=e_sct, use_lf=False)
    return sc


def euclidean_distance_test(language, rt, sct, k):
    keywords_file = './corpus/{}/semeval_task/keywords.pkl'.format(language)
    keywords = utils.load_from_disk(keywords_file)
    # keywords = ['bit']
    separate = None if language == 'en' or language == 'sv' else True
    corpus1 = './corpus/{}/semeval_task/data1.pkl'.format(language)
    corpus2 = './corpus/{}/semeval_task/data2.pkl'.format(language)
    f = rd.gen_run_data(keywords, "", corpus1, corpus2, language, lan_emb=None, separate=separate)
    rank_results = {}
    sc_results = {}
    e_sct_sample = [0.65, 0.66, 0.67, 0.68, 0.69, 0.70, 0.71, 0.72, 0.73, 0.74,
                    0.75, 0.76, 0.77, 0.78, 0.79, 0.80, 0.81, 0.82, 0.83, 0.84,
                    0.85, 0.86, 0.87, 0.88, 0.89, 0.90, 0.91, 0.92, 0.93, 0.94]
    # e_sct_sample = [0.80]
    for e_sct in e_sct_sample:
        sc_results[e_sct] = {}
        rank_results[e_sct] = {}

    for run_data in f:
        word, cloud1, cloud2, static_embeds1, static_embeds2, static_embeds = \
            get_cloud_data(run_data)
        print("exec word ", word)
        hf1, hf2, rt_clusters1, rt_clusters2, lf1, lf2 = st2.load_clusters(language, rt, sct, k, word)
        for e_sct in e_sct_sample:
            sc_results[e_sct][word] = sc_from_euclidean_distance(hf1, hf2, cloud1, cloud2, e_sct)
            rank_results[e_sct][word] = rank_from_euclidean_distance(
                hf1, hf2, rt_clusters1, rt_clusters2, e_sct, static_embeds, usenf=True)
    print("sc_results", sc_results)
    print("rank_results", rank_results)

    sc_accs = {}
    rank_cors = {}
    for e_sct in e_sct_sample:
        rank_cors[e_sct] = st2.evaluate_semeval_task2(rank_results[e_sct], language)
        scs = {}
        for w in sc_results[e_sct]:
           scs[w] = sc_results[e_sct][w][0]
        sc_accs[e_sct] = evaluate_semeval_task1(scs, language)
    print("sc_accs", sc_accs)
    print("rank_cors", rank_cors)
    sc_accs_arr = []
    rank_cors_arr = []
    for e_sct in e_sct_sample:
        sc_accs_arr.append(sc_accs[e_sct])
        rank_cors_arr.append(rank_cors[e_sct])
    print("sc_accs max ", np.max(sc_accs_arr))
    print("rank_cors", np.max(rank_cors_arr))
    




def euclidean_distance(language='en'):
    sample_k = [14]
    if language in ['en', 'de', 'sv']:
        # rt, sct configuration for language ['en', 'de', 'sv']
        # sample_rt = [0.65, 0.66, 0.67, 0.68, 0.69, 0.70, 0.71, 0.72, 0.73, 0.74, 0.75,
        #              0.75, 0.76, 0.77, 0.78, 0.79, 0.80, 0.81, 0.82, 0.83, 0.84, 0.85]
        # sample_sct = [0.55, 0.56, 0.57, 0.58, 0.59, 0.60, 0.61, 0.62, 0.63, 0.64, 0.65,
        #               0.65, 0.66, 0.67, 0.68, 0.69, 0.70, 0.71, 0.72, 0.73, 0.74, 0.75,]
        sample_k = [14]
        sample_rt = [0.66]
        sample_sct = [0.60]
    else:
        # rt, sct configuration for language ['la']
        sample_rt = [0.75, 0.76, 0.77, 0.78, 0.79, 0.80, 0.81, 0.82, 0.83, 0.84, 0.85,
                     0.86, 0.87, 0.88, 0.89, 0.90,]
        sample_sct = [0.75, 0.76, 0.77, 0.78, 0.79, 0.80, 0.81, 0.82, 0.83, 0.84, 0.85,
                      0.86, 0.87, 0.88, 0.89, 0.90,]
    for k in sample_k:
        for rt in sample_rt:
            for sct in sample_sct:
                euclidean_distance_test(language, rt, sct, k)
                    


euclidean_distance()

