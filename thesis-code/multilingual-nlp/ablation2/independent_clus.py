import numpy as np
import utils
import rundata as rd
import semeval_task2 as st2
import cloudprior as cp
from scipy.spatial import distance
from cloudprior import CloudCluster, get_cloud_data, neighbor_similarity
from semeval import evaluate_semeval_task1


def subsmantic_from_clusters(clusters, cluster_simdist, sim_matrix,
                             cloud, static_embeds, keyword, sct=0.6, drop=False):
    hf_clusters = []
    lf_semantic = []
    for i, clu in enumerate(clusters):
        if len(clu.points) >= 2:
            hf_clusters.append(clu)
        else:
            lf_semantic.append(clu)

    print("length hf_clusters before", len(hf_clusters))
    print("length lf_semantic before", len(lf_semantic))
    hf_cluster_simdist = cp.clusters_distances(hf_clusters, sim_matrix, None, static_embeds)

    hf_semantic, polysemy_simdist, drop_semantic = \
        cp.cloud_clusters_reduce(hf_clusters, hf_cluster_simdist, sim_matrix,
                              cloud, static_embeds, keyword, rt=sct, drop=drop)
    # ts.plt_weights(polysemy_simdist)
    lf_semantic += drop_semantic

    print("length lf_semantic after", len(lf_semantic))
    print("hf_semantic len", len(hf_semantic))
    for i, clu in enumerate(hf_semantic):
        print(">>>>> high cid {} points {} ".format(i, clu.points))
        print(clu.c_neighbors)

    for i, clu in enumerate(lf_semantic):
        print(">>>>> low cid {} points {} ".format(i, clu.points))
        print(clu.c_neighbors)

    # ts.plt_weights(polysemy_simdist)
    return hf_semantic, lf_semantic


def semantic_change_from_counting(hf_semantic, pid_t):
    for hf in hf_semantic:
        time1_cou = 0
        time2_cou = 0
        for pid in hf.points:
            if pid < pid_t:
                time1_cou += 1
            else:
                time2_cou += 1
        if (time1_cou > 5 and time2_cou <= 5) or (time2_cou > 5 and time1_cou <= 5):
            return 1
    return 0
        

def independent_clus_test(language, rt, sct, k):
    keywords_file = './corpus/{}/semeval_task/keywords.pkl'.format(language)
    keywords = utils.load_from_disk(keywords_file)
    # keywords = ['bit']
    separate = None if language == 'en' or language == 'sv' else True
    corpus1 = './corpus/{}/semeval_task/data1.pkl'.format(language)
    corpus2 = './corpus/{}/semeval_task/data2.pkl'.format(language)
    f = rd.gen_run_data(keywords, "", corpus1, corpus2, language, lan_emb=None, separate=separate)
    # rank_results = {}
    sc_results = {}

    for run_data in f:
        word, cloud1, cloud2, static_embeds1, static_embeds2, static_embeds = \
            get_cloud_data(run_data)
        print("exec word ", word)
        hf1, hf2, rt_clusters1, rt_clusters2, lf1, lf2 = st2.load_clusters(language, rt, sct, k, word)
        cloud = np.concatenate((cloud1, cloud2), axis=0)
        for clu in rt_clusters2:
            clu.points = [pid + cloud1.shape[0] for pid in clu.points]
        rt_clusters = rt_clusters1+rt_clusters2

        # cluster_simdist = cp.clusters_distances(
        #     rt_clusters, sim_matrix=None, rip_dist=None, static_embeds=static_embeds)
        # print("cluster_simdist", cluster_simdist.shape)
        # hf_semantic, lf_semantic = cp.subsmantic_from_clusters(
        #     rt_clusters, cluster_simdist, None, cloud, static_embeds, word, sct=sct, drop=False)
        # define subsmantic_from_clusters for this test
        hf_semantic, lf_semantic = subsmantic_from_clusters(
            rt_clusters, None, None, cloud, static_embeds, word, sct=sct, drop=False)
        print("hf_semantic", len(hf_semantic))
        print("lf_semantic", len(lf_semantic))
        sc = semantic_change_from_counting(hf_semantic, cloud1.shape[0])
        sc_results[word] = sc
    acc = evaluate_semeval_task1(sc_results, language)
    return acc


def independent_clus(language='en'):
    sample_k = [14]
    # sample_rt = [0.65, 0.66, 0.67, 0.68, 0.69, 0.70, 0.71, 0.72,]
    # sample_sct = [ 0.57, 0.58, 0.59, 0.60, 0.61, 0.62, 0.63]
    sample_rt = [0.66]
    sample_sct = [0.60]
    results = []
    for k in sample_k:
        for rt in sample_rt:
            for sct in sample_sct:
                acc = independent_clus_test(language, rt, sct, k)
                results.append((acc, sct, rt))
    utils.save_as_txt('./data/ablation/indepedent_clus/{}/result.txt'.format(language), str(results))
    print("results", results)


if __name__ == '__main__':
    independent_clus()
