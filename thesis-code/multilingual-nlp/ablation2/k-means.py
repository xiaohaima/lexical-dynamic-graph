import numpy as np
import utils
import rundata as rd
import semeval_task2 as st2
import cloudprior as cp
from scipy.spatial import distance
from cloudprior import CloudCluster, get_cloud_data, neighbor_similarity
from semeval import evaluate_semeval_task1
from sklearn.cluster import KMeans


def get_means_clusters(cloud, fixk):
    X = cloud
    X = np.array(X)
    nm = np.sqrt((X ** 2).sum(axis=1))[:, None]
    X = X / nm
    kmeans = KMeans(n_clusters=fixk).fit(X)
    return kmeans


def from_kmeans_to_clusterprior(kmeans, fixk, keyword, static_embeds, language='en'):
    clusterprior = []
    # def __init__(self, cpoint, nbs, ids, k=0, sim2c=None):
    k = 14
    for cid in range(fixk):
        cpoint = kmeans.cluster_centers_[cid]
        cidx = kmeans.labels_ == cid
        ids = []
        for pid in range(cidx.shape[0]):
            if cidx[pid]:
                ids.append(pid)
        nbs = cp.clear_sim_words(keyword, cpoint, k, static_embeds, language=language)
        clusterprior.append(CloudCluster(cpoint, nbs, ids, k, None))
    
    return clusterprior


def get_sc_rank(hf1, hf2, static_embeds, sct=0.6):
    # hf1, hf2, rt_clusters1, rt_clusters2, lf1, lf2 = load_clusters(language, rt, sct, k, word)

    point_num1 = 0
    point_num2 = 0
    scenes = []
    for hf in hf1:
        point_num1 += hf.point_num
        st2.eva_hf_with_scenes(hf, 1, scenes, sct, static_embeds)

    for hf in hf2:
        point_num2 += hf.point_num
        st2.eva_hf_with_scenes(hf, 2, scenes, sct, static_embeds)
    print("point_num1 {} point_num2 {}".format(point_num1, point_num2))
    print("scenes: ", scenes)
    p_nums = [point_num1, point_num2]
    p = np.zeros((2, len(scenes)))
    for scene_id, scene in enumerate(scenes):
        clus = scene['clus']
        s = [0, 0]
        for clu_tuple in clus:
            time, clu = clu_tuple
            timeid = time-1
            s[timeid] += clu.point_num
        p[0, scene_id] = s[0] / p_nums[0]
        p[1, scene_id] = s[1] / p_nums[1]
    print("probability distribution ", p)
    jsd = distance.jensenshannon(p[0, :], p[1, :])
    print("jensenshannon divergence ", jsd)

    return jsd


def kmeans_test(language, sct=0.86):
    keywords_file = './corpus/{}/semeval_task/keywords.pkl'.format(language)
    keywords = utils.load_from_disk(keywords_file)
    # keywords = ['bit']
    separate = None if language == 'en' or language == 'sv' else True
    corpus1 = './corpus/{}/semeval_task/data1.pkl'.format(language)
    corpus2 = './corpus/{}/semeval_task/data2.pkl'.format(language)
    f = rd.gen_run_data(keywords, "", corpus1, corpus2, language, lan_emb=None, separate=separate)
    fixk = 8
    rank_results = {}
    sc_results = {}
    for run_data in f:
        word, cloud1, cloud2, static_embeds1, static_embeds2, static_embeds = \
            get_cloud_data(run_data)
        
        cache_means_path = './data/{}/kmeans/{}/'.format(language, word)
        kmeans1 = utils.load_from_disk(cache_means_path+'kmeans1.pkl')
        kmeans2 = utils.load_from_disk(cache_means_path+'kmeans2.pkl')
        if kmeans1 is None:
            kmeans1 = get_means_clusters(cloud1, fixk)
            utils.save_to_disk(cache_means_path+'kmeans1.pkl', kmeans1)
        if kmeans2 is None:
            kmeans2 = get_means_clusters(cloud2, fixk)
            utils.save_to_disk(cache_means_path+'kmeans2.pkl', kmeans2)
        
        clu1 = utils.load_from_disk(cache_means_path+'clu1.pkl')
        clu2 = utils.load_from_disk(cache_means_path+'clu2.pkl')
        if clu1 is None:
            clu1 = from_kmeans_to_clusterprior(kmeans1, fixk, word, static_embeds, language)
            utils.save_to_disk(cache_means_path+'clu1.pkl', clu1)
        if clu2 is None:
            clu2 = from_kmeans_to_clusterprior(kmeans2, fixk, word, static_embeds, language)
            utils.save_to_disk(cache_means_path+'clu2.pkl', clu2)

        hf_sms1 = [hf.c_neighbors for hf in clu1]
        hf_sms2 = [hf.c_neighbors for hf in clu2]
        hf_sims = cp.sub_semantic_similarity_matrix(hf_sms1, hf_sms2, static_embeds)
        print("hf_sims is ", hf_sims)
        sc, sc_type = cp.sc_from_similarity_matrix(hf_sims, None, None,
                                                   rt=0.99, sct=sct, use_lf=False)
        sc_results[word] = sc
        rank = get_sc_rank(clu1, clu2, sct=sct, static_embeds=static_embeds)
        rank_results[word] = rank
    
    print("rank_results: ", rank_results)
    print("sc_results: ", sc_results)
    acc = evaluate_semeval_task1(sc_results, language)
    cor, _ = st2.evaluate_semeval_task2(rank_results, language)
    return acc, cor


if __name__ == '__main__':
    # sample_sct = [0.75, 0.76, 0.77, 0.78, 0.79, 0.80, 0.81, 0.82, 0.83, 0.84, 0.85,
    #               0.86, 0.87, 0.88, 0.89, 0.90,]
    sample_sct = [0.68, ]
    accs = []
    cors = []
    for sct in sample_sct:
        acc, cor = kmeans_test(language='en', sct=sct)
        # print('acc', acc)
        # print('cor', cor)
        accs.append(acc)
        cors.append(cor)
    print('accs', accs)
    print('cors', cors)

    print('accs max', max(accs))
    print('cors max', max(cors))


