import utils
import numpy as np
import semantic_tree as st
import bertcloud as bc
import treesim as ts


def check_cache(cache_path):
    files = utils.find_files(cache_path)
    if len(files) != 4:
        return None
    cloud1 = utils.load_from_disk(cache_path+'cloud1')
    cloud2 = utils.load_from_disk(cache_path + 'cloud2')
    stat_emb1 = utils.load_from_disk(cache_path + 'stat_emb1')
    stat_emb2 = utils.load_from_disk(cache_path + 'stat_emb2')
    return cloud1, stat_emb1, cloud2, stat_emb2


def save_cache(cloud1, stat_emb1, cloud2, stat_emb2, cache_path):
    utils.save_to_disk(cache_path + 'cloud1', cloud1)
    utils.save_to_disk(cache_path + 'stat_emb1', stat_emb1)
    utils.save_to_disk(cache_path + 'cloud2', cloud2)
    utils.save_to_disk(cache_path + 'stat_emb2', stat_emb2)


def init_static_embedding():
    index_voc = {}
    voc_index = {}
    embeddings = {}
    static_embeds = (index_voc, voc_index, embeddings)
    return static_embeds


def gen_run_data(keywords, pmodel, corpus1, corpus2, language, static_embedding=None, separate=False):
    data1 = utils.load_from_disk(corpus1)
    data2 = utils.load_from_disk(corpus2)
    for keyword in keywords:
        sendata1 = data1[keyword]
        sendata2 = data2[keyword]
        if len(sendata1) < 40 or len(sendata2) < 40:
            continue
        print("Exec semantic tree for word ", keyword)
        cache_path = './data/{}/words/{}/'.format(language, keyword)
        rte = check_cache(cache_path)
        if rte is None:
            cloud1, stat_emb1 = bc.bert_cloud(keyword=keyword, sendata=sendata1, prbert_model=pmodel)
            cloud2, stat_emb2 = bc.bert_cloud(keyword=keyword, sendata=sendata2, prbert_model=pmodel)
            save_cache(cloud1, stat_emb1, cloud2, stat_emb2, cache_path)
        else:
            cloud1, stat_emb1, cloud2, stat_emb2 = rte

        X = np.array(cloud1)
        nm = np.sqrt((X ** 2).sum(axis=1))[:, None]
        cloud1 = X / nm
        X = np.array(cloud2)
        nm = np.sqrt((X ** 2).sum(axis=1))[:, None]
        cloud2 = X / nm

        if not separate:
            static_embeds = init_static_embedding()
            if static_embedding is not None:
                static_embeds = utils.load_from_disk(static_embedding)
            static_embeds = st.update_static_embeds(static_embeds, stat_emb1)
            static_embeds = st.update_static_embeds(static_embeds, stat_emb2)
            yield keyword, cloud1, cloud2, static_embeds
        else:
            static_embeds1 = init_static_embedding()
            static_embeds2 = init_static_embedding()
            static_embeds = init_static_embedding()
            s_embeds1 = st.update_static_embeds(static_embeds1, stat_emb1)
            s_embeds2 = st.update_static_embeds(static_embeds2, stat_emb2)
            static_embeds = st.update_static_embeds(static_embeds, stat_emb1)
            static_embeds = st.update_static_embeds(static_embeds, stat_emb2)
            yield keyword, cloud1, cloud2, s_embeds1, s_embeds2, static_embeds


def get_evolution_set(run_data, simsize=12, separate=True):
    if separate:
        keyword, cloud1, cloud2, static_embeds1, static_embeds2, static_embeds = run_data
        # poly_embeds1 = st.get_polysemy(keyword, cloud1, static_embeds1, check_simsize=simsize)
        # poly_embeds2 = st.get_polysemy(keyword, cloud2, static_embeds2, check_simsize=simsize)
        poly_embeds1 = ts.get_polysemy_embedding(keyword, cloud1, static_embeds1, check_simsize=simsize)
        poly_embeds2 = ts.get_polysemy_embedding(keyword, cloud2, static_embeds2, check_simsize=simsize)
        evolve1 = ts.get_most_sim_words(keyword, poly_embeds1, simsize, static_embeds1)
        evolve2 = ts.get_most_sim_words(keyword, poly_embeds2, simsize, static_embeds2)
        return keyword, static_embeds, evolve1, evolve2
    else:
        keyword, cloud1, cloud2, static_embeds = run_data
        poly_embeds1 = st.get_polysemy(keyword, cloud1, static_embeds, check_simsize=simsize)
        poly_embeds2 = st.get_polysemy(keyword, cloud2, static_embeds, check_simsize=simsize)
        evolve1 = ts.get_most_sim_words(keyword, poly_embeds1, simsize, static_embeds)
        evolve2 = ts.get_most_sim_words(keyword, poly_embeds2, simsize, static_embeds)
        return keyword, static_embeds, evolve1, evolve2


def get_evolution(keywords, pmodel, corpus1, corpus2, language, static_embedding=None, simsize=12, separate=False):
    f = gen_run_data(keywords, pmodel, corpus1, corpus2, language, static_embedding=static_embedding, separate=separate)
    evolves = {}
    for run_data in f:
        keyword, static_embeds, evolve1, evolve2 = \
            get_evolution_set(run_data, simsize=simsize, separate=separate)
        evolves[keyword] = {"static_emb": static_embeds, "psim_words": (evolve1, evolve2)}
        # evolves[keyword] = {"psim_words": (evolve1, evolve2)}
    return evolves


def plt_evolve_trees(keywords, pmodel, corpus1, corpus2, language, static_embedding=None, simsize=8):
    f = gen_run_data(keywords, pmodel, corpus1, corpus2, language, static_embedding=static_embedding)
    for run_data in f:
        keyword, cloud1, cloud2, static_embeds = run_data
        title = 'Semantic evolve tree for English word ' + keyword
        st.plt_evolve_tree(keyword, cloud1, cloud2, static_embeds, simsize=simsize, title=title)


def evolve_trees_en(will_plt=False):
    # keywords = ['mouse', 'cloud', 'stream', 'virus', 'desktop', 'browser', 'hacker', 'spam', 'troll', 'phone',
    #             'tablet', 'upload', 'download', 'friend', 'post', 'profile', 'filter', 'cyber', 'data', 'domain',
    #             'online', 'digital', 'smart', 'warable', 'robot', 'automobile', 'computer', 'laptop', 'speaker',
    #             'camera', 'remote']
    # keywords = ['mouse', 'cloud', 'stream', 'virus', 'troll', 'tablet', 'friend', 'post', 'profile',
    #             'filter', 'data', 'domain', 'smart', 'speaker', 'camera', 'remote']
    keywords = ['mouse', 'cloud', 'stream']
    pmodel = 'bert-base-multilingual-cased'  # 'bert-base-uncased'
    corpus1 = './data/en/semeval/data_ccoha1'
    corpus2 = './data/en/wiki/data_p1p41242'
    language = 'en'
    static_embedding = './data/en/static_embed.pkl'  # './data/multi/static_embed.pkl'
    evolves = get_evolution(keywords, pmodel, corpus1, corpus2,
                            language, static_embedding=None, simsize=12)
    for kw in evolves:
        print(evolves[kw]["psim_words"])
    utils.save_to_disk('./en_evolve_set', evolves)
    if will_plt:
        plt_evolve_trees(keywords, pmodel, corpus1, corpus2, language, static_embedding=None, simsize=8)


def evolve_trees_de(will_plt=False):
    keywords = ['Maus', 'Wolke', 'Stream']
    pmodel = 'bert-base-multilingual-cased'  # 'bert-base-german-cased'
    corpus1 = './data/de/semeval/data_corpus1'
    corpus2 = './data/de/wiki/data_p1p297012'
    language = 'de'
    static_embedding = './data/de/static_embed.pkl'  # './data/multi/static_embed.pkl'
    evolves = get_evolution(keywords, pmodel, corpus1, corpus2,
                            language, static_embedding=None, simsize=12)
    print(evolves)
    if will_plt:
        plt_evolve_trees(keywords, pmodel, corpus1, corpus2, language, static_embedding=None, simsize=8)


def sc_sim_en():
    # evolves = utils.load_from_disk('./en_evolve_set')
    evolves = utils.load_from_disk('./en_semeval_evolve_set_sep')
    ts.evaluate_tree_similarity(evolves)


def semeval_evolution(will_plt=False):
    keywords = utils.load_from_disk('./corpus/en/semevalSC_r/keywords')
    print(keywords)
    # keywords = ["attack", "plane"]
    # keywords = ["attack", "plane", "edge", "stab", "tip", "thump"]
    pmodel = 'bert-base-multilingual-cased'  # 'bert-base-uncased'
    corpus1 = './corpus/en/semevalSC_r/data_ccoha1'
    corpus2 = './corpus/en/semevalSC_r/data_ccoha2'
    language = 'en'
    static_embedding = './data/en/static_embed.pkl'  # './data/multi/static_embed.pkl'
    evolves = get_evolution(keywords, pmodel, corpus1, corpus2,
                            language, static_embedding=None, simsize=12, separate=True)
    # print(evolves)
    utils.save_to_disk('./en_semeval_evolve_set_sep', evolves)
    if will_plt:
        plt_evolve_trees(keywords, pmodel, corpus1, corpus2, language, static_embedding=None, simsize=8)


def evaluate_task1():
    targets = 'corpus/semeval2020_ulscd_eng/targets.txt'
    with open(targets, "r", encoding="utf-8") as targets_data:
        targets_words = targets_data.read().split('\n')
    import semeval_official as seo
    truth_path = "./corpus/semeval2020_ulscd_eng/truth/binary.txt"
    pred_path = "./corpus/en/semevalSC_r/semantic_change_results.txt"
    target_results = utils.load_from_disk('./corpus/en/semevalSC_r/semantic_change_results')
    results_str = ""
    for t_word in target_results:
        save_word=""
        for targets_word in targets_words:
            if t_word in targets_word:
                save_word = targets_word
                break
        results_str = results_str + save_word + "\t" + str(target_results[t_word]) + "\n"
    res_file = open(pred_path, 'w', encoding='utf-8')
    res_file.write(results_str)
    res_file.close()
    acc = seo.accuracy_official(truth_path, pred_path)
    print(" Sem eval task 1 accuracy = ", acc)
    return


if __name__ == '__main__':
    # semeval_evolution(will_plt=False)
    # sc_sim_en()
    evaluate_task1()




