import matplotlib.pyplot as plt
from sklearn.datasets import load_iris
from sklearn.decomposition import PCA
import utils
from sklearn.cluster import KMeans
from scipy.spatial.distance import cdist
import numpy as np
from sklearn.metrics import silhouette_score


def find_k_elbow_rule(X, K_max=10, eva=3):
    distortions = []
    K = range(1, K_max)
    curves = {}
    for k in K:
        # KMeans 是基于euclidean dis计算，而不是cos dis计算
        kmeanModel = KMeans(n_clusters=k).fit(X)
        # distortions.append(sum(np.min(cdist(X, kmeanModel.cluster_centers_, 'euclidean'), axis=1)) / X.shape[0])
        distortions.append(kmeanModel.inertia_)
        # distortions.append(silhouette_score(X, kmeanModel.labels_, metric='euclidean'))

    for k in range(2, K_max-1):
        curve = (distortions[k - 2] - distortions[k - 1]) - (distortions[k - 1] - distortions[k])
        curves[k] = curve

    curves_sorted = sorted(curves.items(), key=lambda kv: kv[1], reverse=True)
    eva_k = curves_sorted[0][0]
    optimal_k = 1
    if eva_k < (K_max/2):
        judge_k_v = ((distortions[0] - distortions[eva_k - 1]) / (eva_k - 1)) / (
                (distortions[eva_k - 1] - distortions[-1]) / (K[-1] - eva_k))
        judge_k = judge_k_v > eva
        optimal_k = eva_k if judge_k else 1
        print(eva_k)
        print(judge_k_v)
        print(optimal_k)

    # plt.plot(K, distortions, 'bx-')
    # plt.xlabel('k')
    # plt.ylabel('Distortion')
    # plt.title('The Elbow Method showing the optimal k')
    # plt.show()

    return optimal_k


def find_embeding_centers(target_embeds):
    X = target_embeds
    X = np.array(X)
    nm = np.sqrt((X ** 2).sum(axis=1))[:, None]
    X = X / nm

    k = find_k_elbow_rule(X, K_max=10, eva=3)
    kmeans = KMeans(n_clusters=k).fit(X)
    # print(kmeans.cluster_centers_.shape)
    # index_voc, voc_index, embeddings = utils.load_from_disk("en_static_embed.pkl")
    # for embed in kmeans.cluster_centers_:
    #     print('+++++++++++++++++++++++')
    #     utils.most_sim_words(embed, 8, voc_index, embeddings)

    return kmeans.cluster_centers_


def evolve_branch(embeds1, embeds2):
    # e1 = np.mean(embeds1, axis=0)
    # e2 = np.mean(embeds2, axis=0)
    kmeans = KMeans(n_clusters=1).fit(embeds1)
    e1 = kmeans.cluster_centers_[0]

    kmeans = KMeans(n_clusters=1).fit(embeds2)
    e2 = kmeans.cluster_centers_[0]

    return (e1, e2)


def polysemy_branch(embeds):
    poly_embeds = find_embeding_centers(embeds)
    return poly_embeds


def semantic_branch(poly_embeds, static_embeds):
    index_voc, voc_index, embeddings = static_embeds
    sim_edges_all = []
    for embed in poly_embeds:
        print('+++++++++++++++++++++++')
        sim_edges = []
        words = utils.most_sim_words(embed, 5, voc_index, embeddings)
        for sim_word in words:
            sim_w = sim_word[0]
            sim_e = sim_word[2]
            sim_edges.append(embed, sim_e)

        sim_edges_all.append(sim_edges)

    return sim_edges_all


def similarity_edges(poly_ids, X, static_embeds):
    index_voc, voc_index, embeddings = static_embeds
    sim_edges_all = []
    for poly_id in poly_ids:
        embed = X[poly_id]
        words = utils.most_sim_words(embed, 3, voc_index, embeddings)
        sim_edges = []
        for sim_word in words:
            sim_w = sim_word[0]
            sim_e = sim_word[2]
            sim_edges.append((poly_id, len(X)))
            X.append(sim_e)

        sim_edges_all.append(sim_edges)

    return sim_edges_all


def factor_score(factor, words, keyword, static_embeds):
    # should change factor to a list of words related to factors.
    index_voc, voc_index, embeddings = static_embeds
    sim_scores = []
    for sim_word in words:
        sim_w = sim_word[0]
        if keyword == sim_w:
            continue
        sim = utils.words_sim(factor, sim_w, voc_index, embeddings)
        sim_scores.append(sim)

    print(factor, "sim_scores ", sim_scores)
    print(factor, "avg sim_scores ", np.mean(sim_scores))


def similarity_edges_approx(key_word, poly_edges, poly_ids, X, static_embeds, X_dr, simsize=5):
    # print("similarity_edges_approx ========= ")
    index_voc, voc_index, embeddings = static_embeds
    sim_edges_all = []
    for j, poly_id in enumerate(poly_ids):
        print("similarity_edges_approx poly_ids +++++ ", poly_id)
        # poly_edge = poly_edges[j] if len(poly_edges) > 1 else (0, 1)
        poly_edge = poly_edges[j]
        embed = X[poly_id]
        words = utils.most_sim_words(embed, simsize, voc_index, embeddings, willprint=True)

        # sim = utils.cosine_similarity(embed, embeddings[voc_index['keyboard']])
        # print(" =========================== keyboard ", sim)

        # factor_score('technology', words, key_word, static_embeds)
        # factor_score('culture', words, key_word, static_embeds)
        # factor_score('migration', words, key_word, static_embeds)
        special_tokens = [key_word, "[PAD]", "[CLS]", "[SEP]", "[UNK]", "[MASK]"]
        sim_edges = []
        for sim_word in words:
            sim_w = sim_word[0]
            if sim_w in special_tokens:
                continue
            # print(sim_w)
            id = find_most_sim(sim_w, static_embeds, X, X_dr, poly_edge, poly_id, sim_edges)
            sim_edges.append(((poly_id, id), sim_w))

        sim_edges_all.append(sim_edges)

    return sim_edges_all


def distance(a, b):
    return np.linalg.norm(a-b)


def find_most_sim(word, static_embeds, X, X_dr=None, poly_edge=None, poly_id=None, sim_edges=None):
    index_voc, voc_index, embeddings = static_embeds
    e = utils.word_voc(word, voc_index, embeddings)
    emb_sim = {}
    for i, emb in enumerate(X):
        sim = utils.cosine_similarity(e, emb)
        exists_ids = [edge[0][1] for edge in sim_edges]
        if poly_edge is not None and poly_id != i and i not in exists_ids:
            l = distance(X_dr[poly_edge[0]], X_dr[poly_edge[1]])
            le = distance(X_dr[0], X_dr[1])
            # temp lengthen l if l shorter than le
            l = le if le > l else l
            a = X_dr[poly_id]
            b = X_dr[i]
            r = distance(a, b)
            dir_p = X_dr[poly_edge[0]] - a
            dir_s = b - a
            dot = np.dot(dir_p, dir_s)
            if 1.5*r < l and dot < 0:
                emb_sim[i] = sim
            # if 0.3*r < l:
            #     emb_sim[i] = sim
        # else:
        #     emb_sim[i] = sim

    emb_sim_sorted = sorted(emb_sim.items(), key=lambda kv: kv[1], reverse=True)

    most_sim_id = emb_sim_sorted[0][0]

    return most_sim_id


def plt_sim_nodes(sim_edges_list, X_dr):
    labeled = False
    for sim_edges_ in sim_edges_list:
        for sim_edges in sim_edges_:
            for sim_edge, sim_w in sim_edges:
                sim_id = sim_edge[1]
                if labeled:
                    plt.scatter(X_dr[sim_id, 0], X_dr[sim_id, 1], alpha=0.9, c='#1E90FF', s=16, zorder=2)
                else:
                    plt.scatter(X_dr[sim_id, 0], X_dr[sim_id, 1], alpha=0.9, c='#1E90FF', s=16, zorder=2, label='Similar embedding')
                    labeled = True
                plt.text(X_dr[sim_id, 0], X_dr[sim_id, 1], sim_w, fontsize=10, color="blue", style="italic", weight="light",
                         verticalalignment='center', horizontalalignment='right', rotation=45, alpha=0.5)


def plt_sim_lines(sim_edges_list, X_dr):
    labeled = False
    for sim_edges_ in sim_edges_list:
        for sim_edges in sim_edges_:
            for sim_edge, sim_w in sim_edges:
                if labeled:
                    plt.plot(X_dr[sim_edge, 0], X_dr[sim_edge, 1], c='#7FFFD4', zorder=1)
                else:
                    plt.plot(X_dr[sim_edge, 0], X_dr[sim_edge, 1], c='#7FFFD4', zorder=1, label='Similarity cluster')
                    labeled = True


def plt_poly_nodes(poly_ids_list, X_dr):
    labeled = False
    for poly_ids in poly_ids_list:
        for poly_id in poly_ids:
            if labeled:
                plt.scatter(X_dr[poly_id, 0], X_dr[poly_id, 1], alpha=0.9, c='#FF4500', s=40, zorder=3)
            else:
                plt.scatter(X_dr[poly_id, 0], X_dr[poly_id, 1], alpha=0.9, c='#FF4500', s=40, zorder=3, label='Polysemy embedding')
                labeled = True


def plt_poly_lines(poly_edges_list, X_dr):
    labeled = False
    for poly_edges in poly_edges_list:
        if len(poly_edges) == 1:
            continue
        for poly_edge in poly_edges:
            if labeled:
                plt.plot(X_dr[poly_edge, 0], X_dr[poly_edge, 1], c='#FF7F50', zorder=1)
            else:
                plt.plot(X_dr[poly_edge, 0], X_dr[poly_edge, 1], c='#FF7F50', zorder=1, label='Polysemy')
                labeled = True


def plt_evolve_nodes(X_dr):
    plt.scatter(X_dr[0, 0], X_dr[0, 1], alpha=0.99, c='#000080', s=60, zorder=4, label='Center embedding before evolve')
    plt.scatter(X_dr[1, 0], X_dr[1, 1], alpha=0.99, c='#006400', s=60, zorder=4, label='Center embedding after evolve')


def plt_evolve_lines(evolve_edge, X_dr):
    plt.plot(X_dr[evolve_edge, 0], X_dr[evolve_edge, 1], c='red', zorder=1, label='Evolve')


def plt_embedding_distribution(xsize, size1, size2, X_dr):
    plt.scatter(X_dr[xsize:xsize+size1, 0], X_dr[xsize:xsize+size1, 1],
                alpha=0.3, c='#FF1493', s=5, label='Embeddings before evolve')
    plt.scatter(X_dr[xsize+size1:xsize+size1+size2, 0], X_dr[xsize+size1:xsize+size1+size2, 1],
                alpha=0.3, c='#778899', s=5, label='Embeddings after evolve')


# Our purpose is to separate polysemy as much as possible, not to merge them.
# it's better to set a dynamic sim size. Only set max sim size which would be max degree of the tree.
def merge_poly(keyword, poly_embeds, static_embeds, check_simsize=12, merge_t=0.6):
    index_voc, voc_index, embeddings = static_embeds
    merged = False
    polysim_words = []
    for embed in poly_embeds:
        words = utils.most_sim_words(embed, check_simsize+1, voc_index, embeddings, willprint=False)
        words = [w[0] for w in words]
        checkwords = []
        for w in words:
            if w != keyword and len(checkwords) < check_simsize:
                checkwords.append(w)
        polysim_words.append(checkwords)

    # print("polysim_words ", polysim_words)
    for rid in range(len(polysim_words)):
        rwords = polysim_words[rid]
        for cid in range(len(polysim_words)):
            if cid == rid:
                continue
            cwords = polysim_words[cid]
            rcount = 0
            for rw in rwords:
                rcount = rcount+1 if rw in cwords else rcount
            sim_percent = rcount/len(rwords)
            print("sim_percent ", sim_percent, " rid ", rid, " cid", cid)
            if sim_percent > merge_t:
                new_poly_embeds = []
                merge_emb = (poly_embeds[rid]+poly_embeds[cid])/2
                new_poly_embeds.append(merge_emb)
                for id in range(len(polysim_words)):
                    if id != rid and id != cid:
                        new_poly_embeds.append(poly_embeds[id])
                print("Merge poly_embeds:")
                print("origin poly_embeds len ", len(poly_embeds))
                print("new poly_embeds len ", len(new_poly_embeds))
                # poly_embeds = new_poly_embeds
                merged = True
                return merged, new_poly_embeds

    return merged, poly_embeds


def polysemy_reduce(keyword, poly_embeds, static_embeds, check_simsize=10):
    if len(poly_embeds) == 1:
        return poly_embeds
    merge = True
    while merge:
        merge, poly_embeds = merge_poly(keyword, poly_embeds, static_embeds, check_simsize=6, merge_t=0.5)
    return poly_embeds


def get_polysemy(keyword, cloud, static_embedding, check_simsize=12, joint_cloud=None, eid=0):
    poly_embeds = polysemy_branch(cloud)
    poly_embeds = polysemy_reduce(keyword, poly_embeds, static_embedding, check_simsize=check_simsize)

    if joint_cloud is not None:
        poly_ids = []
        poly_edges = []
        if len(poly_embeds) > 1:
            for poly in poly_embeds:
                poly_ids.append(len(joint_cloud))
                poly_edges.append((eid, len(joint_cloud)))
                joint_cloud.append(poly)
        else:
            poly_ids = [eid]
            poly_edges.append((1-eid, eid))
        return poly_embeds, poly_ids, poly_edges, joint_cloud
    else:
        return poly_embeds


def plt_evolve_tree(key_word, embeds1, embeds2, static_embeds, simsize=5, title='Evolve Tree'):
    e1, e2 = evolve_branch(embeds1, embeds2)
    X = [e1, e2]
    evolve_edge = (0, 1)

    poly_embeds1, poly_ids1, poly_edges1, X =\
        get_polysemy(key_word, embeds1, static_embeds, check_simsize=12, joint_cloud=X, eid=0)

    poly_embeds2, poly_ids2, poly_edges2, X =\
        get_polysemy(key_word, embeds2, static_embeds, check_simsize=12, joint_cloud=X, eid=1)

    xsize = len(X)
    X = np.array(X)
    print("X shape ", X.shape)
    X = np.concatenate((X, embeds1, embeds2), axis=0)
    print("X shape concatenate ", X.shape)
    pca = PCA(n_components=2)
    pca = pca.fit(X)
    X_dr = pca.transform(X)
    # print(X_dr)

    sim_edges1 = similarity_edges_approx(key_word, poly_edges1, poly_ids1, X, static_embeds, X_dr, simsize)
    sim_edges2 = similarity_edges_approx(key_word, poly_edges2, poly_ids2, X, static_embeds, X_dr, simsize)

    plt.figure(figsize=(10, 8))
    # plt.scatter(X_dr[:, 0], X_dr[:, 1], alpha=0.2, c='gray', s=5)
    plt_embedding_distribution(xsize, embeds1.shape[0], embeds2.shape[0], X_dr)

    plt_sim_lines([sim_edges1, sim_edges2], X_dr)
    plt_poly_lines([poly_edges1, poly_edges2], X_dr)
    plt_evolve_lines(evolve_edge, X_dr)

    plt_sim_nodes([sim_edges1, sim_edges2], X_dr)
    plt_poly_nodes([poly_ids1, poly_ids2], X_dr)
    plt_evolve_nodes(X_dr)

    plt.legend()
    plt.title(title)
    plt.show()
    print("plt done")


def gen_evolve_tree_en():
    key_word = 'mouse'
    target_embeds1 = utils.load_from_disk("mouse_target_embeds_c01.pkl")
    X = np.array(target_embeds1)
    nm = np.sqrt((X ** 2).sum(axis=1))[:, None]
    embeds1 = X / nm

    target_embeds2 = utils.load_from_disk("mouse_target_embeds.pkl")
    X = np.array(target_embeds2)
    nm = np.sqrt((X ** 2).sum(axis=1))[:, None]
    embeds2 = X / nm

    static_embeds = utils.load_from_disk("en_static_embed.pkl")
    title = 'Semantic evolve tree for English word "mouse" '
    plt_evolve_tree(key_word, embeds1, embeds2, static_embeds, simsize=8, title=title)


def update_static_embeds(static_embeds, stat_emb):
    print("len(stat_emb)", len(stat_emb))
    index_voc, voc_index, embeddings = static_embeds
    idxs = [key for key in index_voc]
    voc_len = np.max(idxs) if len(idxs)>0 else 0
    voc_len += 1
    print("voc_len start ", voc_len)
    for word in stat_emb:
        if word not in voc_index:
            voc_index[word] = voc_len
            index_voc[voc_len] = word
            embeddings[voc_len] = stat_emb[word]
            voc_len += 1
    print("voc_len end ", voc_len)
    static_embeds = (index_voc, voc_index, embeddings)

    # sim = utils.cosine_similarity(e, stat_emb['Tastatur'])
    # print("Tastatur ", sim)

    return static_embeds


def gen_evolve_tree_de():
    key_word = 'Maus'
    target_embeds1 = utils.load_from_disk("./corpus/de/Maus/semeval/cloud")
    X = np.array(target_embeds1)
    nm = np.sqrt((X ** 2).sum(axis=1))[:, None]
    embeds1 = X / nm

    target_embeds2 = utils.load_from_disk("./corpus/de/Maus/wiki/cloud")
    X = np.array(target_embeds2)
    nm = np.sqrt((X ** 2).sum(axis=1))[:, None]
    embeds2 = X / nm

    static_embeds = utils.load_from_disk("./corpus/de/static_embed.pkl")
    stat_emb = utils.load_from_disk("./corpus/de/Maus/wiki/stat_emb")
    static_embeds = update_static_embeds(static_embeds, stat_emb)
    title = 'Semantic evolve tree for German word "Maus" '
    plt_evolve_tree(key_word, embeds1, embeds2, static_embeds, simsize=6, title=title)


def gen_evolve_tree_swi():
    key_word = 'mus'
    target_embeds1 = utils.load_from_disk("mus_swi_ccohaembeds.pkl01")
    X = np.array(target_embeds1)
    nm = np.sqrt((X ** 2).sum(axis=1))[:, None]
    embeds1 = X / nm

    target_embeds2 = utils.load_from_disk("mus_swi_wikiembeds.pkl")
    X = np.array(target_embeds2)
    nm = np.sqrt((X ** 2).sum(axis=1))[:, None]
    embeds2 = X / nm

    static_embeds = utils.load_from_disk("swi_static_embed.pkl")

    stat_emb = utils.load_from_disk("stat_embmus_swi_wikiembeds.pkl")
    static_embeds = update_static_embeds(static_embeds, stat_emb)

    title = 'Semantic evolve tree for Swedish word "mus" '
    plt_evolve_tree(key_word, embeds1, embeds2, static_embeds, simsize=6, title=title)


if __name__ == '__main__':
    gen_evolve_tree_en()
    # gen_evolve_tree_de()
    # gen_evolve_tree_swi()

