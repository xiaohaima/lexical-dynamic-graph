import utils
import numpy as np
import semantic_tree as st
from tqdm import tqdm
import math
import multiprocessing as mp


def check_cache(cache_path):
    cloud1 = utils.load_from_disk(cache_path+'cloud1')
    cloud2 = utils.load_from_disk(cache_path + 'cloud2')
    stat_emb1 = utils.load_from_disk(cache_path + 'stat_emb1')
    stat_emb2 = utils.load_from_disk(cache_path + 'stat_emb2')
    if cloud1 and stat_emb1 and cloud2 and stat_emb2:
        return cloud1, stat_emb1, cloud2, stat_emb2
    else:
        return None


def save_cache(cloud1, stat_emb1, cloud2, stat_emb2, cache_path):
    utils.save_to_disk(cache_path + 'cloud1', cloud1)
    utils.save_to_disk(cache_path + 'stat_emb1', stat_emb1)
    utils.save_to_disk(cache_path + 'cloud2', cloud2)
    utils.save_to_disk(cache_path + 'stat_emb2', stat_emb2)


def init_static_embedding():
    index_voc = {}
    voc_index = {}
    embeddings = {}
    static_embeds = (index_voc, voc_index, embeddings)
    return static_embeds


def normalize_sendata(sendata1, sendata2, separate, normalizedata=True):
    if separate is None:
        separate = True if len(sendata1) > 200 and len(sendata2) > 200 else False
    if not normalizedata:
        return sendata1, sendata2, separate

    if len(sendata1) > 1500:
        sendata1 = sendata1[0:1500]
    if len(sendata2) > 1500:
        sendata2 = sendata2[0:1500]

    return sendata1, sendata2, separate


def statistic_neighbor_distance_mp(vocabulary, k, static_embeds, pid, return_dict, l_min=3, language="en"):
    index_voc, voc_index, embeddings = static_embeds
    distance_scores = []
    for w in tqdm(vocabulary):
        if utils.eva_uncommon_word(w, '', l_min=l_min, language=language):
            continue
        idx = voc_index[w]
        embed = embeddings[idx]
        sims = utils.clear_sim_words(w, embed, k, static_embeds)
        scores = []
        for ws in sims:
            ws_embed = embeddings[voc_index[ws]]
            ws_sims = utils.clear_sim_words(ws, ws_embed, k, static_embeds)
            match_score = utils.eva_polysim_linear_assignment(sims, ws_sims, static_embeds)
            scores.append(match_score)
        # print("w {} poly_sims {} scores {}".format(w, sims, scores))
        distance_scores.append(np.mean(scores))
    print(distance_scores)
    print(np.mean(distance_scores))
    return_dict[pid] = distance_scores


def statistic_neighbor_distance(static_embeds, k=12, p_n=8):
    index_voc, voc_index, embeddings = static_embeds
    print("len static_embeds vocabulary ", len(voc_index))
    vocabulary = [v for v in voc_index]
    vocabulary = vocabulary[0:512]
    step = int(math.ceil(len(vocabulary) / p_n))
    manager = mp.Manager()
    return_dict = manager.dict()
    procs = []
    for pid in range(p_n):
        endf = (pid + 1) * step
        endf = endf if endf < len(vocabulary) else len(vocabulary)
        vocabulary_p = vocabulary[pid * step:endf]
        p = mp.Process(target=statistic_neighbor_distance_mp, args=(
            vocabulary_p, k, static_embeds, pid, return_dict, 3, 'en'))
        p.start()
        procs.append(p)
        print("start process id ", pid)
    for p in procs:
        p.join()

    print(return_dict)
    scores = []
    for pid in return_dict:
        scores += return_dict[pid]
    print("scores", scores)
    print("scores mean {} std {} ", np.mean(scores), np.std(scores))
    exit(0)


def gen_run_data(keywords, pmodel, corpus1, corpus2, language, lan_emb=None, separate=False):
    data1 = utils.load_from_disk(corpus1)
    data2 = utils.load_from_disk(corpus2)
    for keyword in keywords:
        sendata1 = data1[keyword]
        sendata2 = data2[keyword]
        if len(sendata1) < 40 or len(sendata2) < 40:
            continue
        # sendata1, sendata2, separate = normalize_sendata(sendata1, sendata2, separate)
        sendata_path = './data/{}/words/{}/sendata/'.format(language, keyword)
        utils.save_sendata_as_txt(sendata_path + 'sendata1.txt', sendata1)
        utils.save_sendata_as_txt(sendata_path + 'sendata2.txt', sendata2)

        print("Exec bert cloud data for word ", keyword)
        print("Sendata length time1: {} time2: {}, separate {}".format(len(sendata1), len(sendata2), separate))
        cache_path = './data/{}/words/{}/'.format(language, keyword)
        rte = check_cache(cache_path)
        if rte is None:
            print("... Run bert cloud ...")
            import bertcloud as bc
            cloud1, stat_emb1 = bc.bert_cloud(keyword=keyword, sendata=sendata1, prbert_model=pmodel)
            cloud2, stat_emb2 = bc.bert_cloud(keyword=keyword, sendata=sendata2, prbert_model=pmodel)
            save_cache(cloud1, stat_emb1, cloud2, stat_emb2, cache_path)
        else:
            print("... Use cache cloud ...")
            cloud1, stat_emb1, cloud2, stat_emb2 = rte

        X = np.array(cloud1)
        nm = np.sqrt((X ** 2).sum(axis=1))[:, None]
        cloud1 = X / nm
        X = np.array(cloud2)
        nm = np.sqrt((X ** 2).sum(axis=1))[:, None]
        cloud2 = X / nm

        if not separate:
            static_embeds = init_static_embedding()
            if lan_emb is not None:
                static_embeds = utils.load_from_disk(lan_emb)
            static_embeds = st.update_static_embeds(static_embeds, stat_emb1)
            static_embeds = st.update_static_embeds(static_embeds, stat_emb2)
            statistic_neighbor_distance(static_embeds)
            yield keyword, cloud1, cloud2, static_embeds, static_embeds, static_embeds, separate
        else:
            static_embeds1 = init_static_embedding()
            static_embeds2 = init_static_embedding()
            static_embeds = init_static_embedding()
            s_embeds1 = st.update_static_embeds(static_embeds1, stat_emb1)
            s_embeds2 = st.update_static_embeds(static_embeds2, stat_emb2)
            static_embeds = st.update_static_embeds(static_embeds, stat_emb1)
            static_embeds = st.update_static_embeds(static_embeds, stat_emb2)
            statistic_neighbor_distance(static_embeds)
            yield keyword, cloud1, cloud2, s_embeds1, s_embeds2, static_embeds, separate


