
import cloudprior as cl
from cloudprior import CloudCluster
import utils
# from main import evaluate_task1


def evaluate_semeval_task1(target_results: dict, language: str):
    targets = './corpus/{}/semeval_task/targets.txt'.format(language)
    with open(targets, "r", encoding="utf-8") as targets_data:
        targets_words = targets_data.read().split('\n')
    import semeval_official as seo
    # truth_path = "./corpus/semeval2020_ulscd_eng/truth/binary.txt"
    # pred_path = "./corpus/en/semevalSC_r/semantic_change_results.txt"
    # target_results = utils.load_from_disk('./corpus/en/semevalSC_r/semantic_change_results')
    truth_path = './corpus/{}/semeval_task/binary.txt'.format(language)
    pred_path = './corpus/{}/semeval_task/semantic_change_results.txt'.format(language)
    results_str = ""
    for t_word in target_results:
        save_word = ""
        for targets_word in targets_words:
            if t_word in targets_word:
                save_word = targets_word
                break
        results_str = results_str + save_word + "\t" + str(target_results[t_word]) + "\n"
    res_file = open(pred_path, 'w', encoding='utf-8')
    res_file.write(results_str)
    res_file.close()
    acc = seo.accuracy_official(truth_path, pred_path)
    print(" Sem eval task 1 accuracy = ", acc)
    return acc


def semeval_task1(language, k, sct, rt, separate=None):
    print("Run semeval cloud distance. Language {}".format(language))
    keywords_file = './corpus/{}/semeval_task/keywords.pkl'.format(language)
    keywords = utils.load_from_disk(keywords_file)
    # keywords = ["stroke"]
    print(keywords)
    pmodel = 'bert-base-multilingual-cased'  # 'bert-base-uncased'
    corpus1 = './corpus/{}/semeval_task/data1.pkl'.format(language)
    corpus2 = './corpus/{}/semeval_task/data2.pkl'.format(language)
    semantic_change_hf, semantic_change_hf_lf =\
        cl.run_cloud(keywords, pmodel, corpus1, corpus2, language,
                     lan_emb=None, k=k, separate=separate, rt=rt, sct=sct)
    acc_hf = evaluate_semeval_task1(semantic_change_hf, language)
    acc_hf_lf = evaluate_semeval_task1(semantic_change_hf_lf, language)
    print("semeval_task1 accuracy acc_hf {} acc_hf_lf {}".format(acc_hf, acc_hf_lf))
    return acc_hf, acc_hf_lf


def exec_semeval_task1():
    # languages = ['en']
    # sample_sct = [0.6+i*0.01 for i in range(23)]
    # sample_rt = [0.7+i*0.01 for i in range(15)]
    # sample_sct = [0.6]
    # sample_rt = [0.73]
    # sample_sct = [0.60, 0.61, 0.62, 0.63, 0.64, 0.65, 0.66, 0.67, 0.68, 0.69,
    #               0.70, 0.71, 0.72, 0.73, 0.73, 0.75, 0.76, 0.77, 0.78, 0.79,
    #               0.80, 0.81, 0.82, 0.83]
    # sample_sct = [0.57, 0.58, 0.59, 0.60, 0.61, 0.62, 0.63, 0.64, 0.65, 0.66, 0.67, 0.68, 0.69,
    #               0.70, 0.71, 0.72, 0.73, 0.74, 0.75]
    # sample_sct = [0.60, 0.61, 0.62, 0.65, 0.67, 0.69, 0.70, 0.71, 0.72, 0.73, 0.74, 0.75,
    #               0.76,]
    # sample_rt = [0.67, 0.68, 0.69, 0.70, 0.71, 0.72, 0.73, 0.74, 0.75, 0.76, 0.77, 0.78, 0.79,
    #              0.80, 0.81, 0.82, 0.83, 0.84, 0.85]
    # sample_rt = [0.75, 0.76, 0.77, 0.78, 0.79, 0.80, 0.81, 0.82, 0.83, 0.84, 0.85, 0.86, 0.87]
    # sample_rt = [0.82, 0.83, 0.84, 0.85, 0.86, 0.87, 0.88, 0.89, 0.90] # la
    # sample_rt = [0.67, 0.68, 0.69, 0.70, 0.71, 0.72, 0.73, 0.74, 0.75, 0.76, 0.77, 0.78]
    # sample_k = [8, 10, 12, 14]
    # sample_k = [12, 14]

    # languages = ['de']
    # sample_k = [14]
    # sample_rt = [0.82]
    # sample_sct = [0.63]

    # languages = ['la']
    # sample_k = [10]
    # sample_rt = [0.85]
    # sample_sct = [0.85]

    # languages = ['sv']
    # sample_k = [10]
    # sample_rt = [0.85]
    # sample_sct = [0.71]
    # acc_hf = []
    # acc_hf_lf = []

    # languages = ['sv']
    # sample_k = [10, 12, 14]
    # sample_rt = [0.65, 0.66, 0.67, 0.68, 0.69, 0.70, 0.71, 0.72, 0.73, 0.74, 0.75,
    #              0.75, 0.76, 0.77, 0.78, 0.79, 0.80, 0.81, 0.82, 0.83, 0.84, 0.85]
    # sample_sct = [0.55, 0.56, 0.57, 0.58, 0.59, 0.60, 0.61, 0.62, 0.63, 0.64, 0.65,
    #               0.65, 0.66, 0.67, 0.68, 0.69, 0.70, 0.71, 0.72, 0.73, 0.74, 0.75,]
    languages = ['la']
    sample_k = [10, 12, 14]
    # sample_rt = [0.80, 0.81, 0.82, 0.83, 0.84, 0.85,
    #              0.86, 0.87, 0.88, 0.89, 0.90, 0.91, 0.92, 0.93, 0.94, 0.95]
    # sample_sct = [0.80, 0.81, 0.82, 0.83, 0.84, 0.85,
    #               0.86, 0.87, 0.88, 0.89, 0.90, 0.91, 0.92, 0.93, 0.94, 0.95,]
    sample_rt = [0.75, 0.76, 0.77, 0.78, 0.79, 0.80, 0.81, 0.82, 0.83, 0.84, 0.85,
                 0.86, 0.87, 0.88, 0.89, 0.90,]
    sample_sct = [0.75, 0.76, 0.77, 0.78, 0.79, 0.80, 0.81, 0.82, 0.83, 0.84, 0.85,
                  0.86, 0.87, 0.88, 0.89, 0.90,]
    for language in languages:
        for k in sample_k:
            sc_task1_results = []
            for rt in sample_rt:
                for sct in sample_sct:
                    if sct > rt:
                        continue
                    separate = None if language == 'en' or language == 'sv' else True
                    acc_hf, acc_hf_lf = semeval_task1(language, k, sct, rt, separate=separate)
                    sc_task1_result = {'acc': acc_hf, 'acc_hf': acc_hf, 'acc_hf_lf': acc_hf_lf,
                                       'k': k, 'sct': sct, 'rt': rt}
                    sc_task1_results.append(sc_task1_result)
                    utils.save_as_txt(
                        './data/{}/semeval_task/sc_b/k{}/acc{}_k{}_sct{}_rt{}_result.txt'.format(
                        language, k, max(acc_hf, acc_hf_lf), k, sct, rt),
                        str(sc_task1_result))
            sc_task1_result_path = \
                "./data/{}/semeval_task/sc_b/k{}/".format(language, k)
            utils.save_to_disk(sc_task1_result_path+'acc_result.pkl', sc_task1_results)
            utils.save_as_txt(sc_task1_result_path+'acc_result.txt', str(sc_task1_results))
            # print("sc_task1_results", str(sc_task1_results))


if __name__ == '__main__':
    exec_semeval_task1()



