import utils as ut
import numpy as np
import os
import matplotlib.pyplot as plt


def load_semeval_results(rpath):
    results = ut.load_from_disk(rpath)
    return results

def construct_semeval_results_matrix(results, sample_rt, sample_sct):
    accs = {}
    for acc_r in results:
        if acc_r['rt'] not in accs:
            accs[acc_r['rt']] = {}
        accs[acc_r['rt']][acc_r['sct']] = acc_r['acc']
    # print(accs)
    for rt in sample_rt:
        for sct in sample_sct:
            if sct not in accs[rt]:
                accs[rt][sct] = accs[rt][rt]

    # print(accs[0.65])
    print(accs)
    results_matrix = np.zeros((len(sample_rt), len(sample_sct)))
    for i, rt in enumerate(sample_rt):
        for j, sct in enumerate(sample_sct):
            # results_matrix[len(sample_rt)-i-1,j] = accs[rt][sct]
            results_matrix[i,j] = accs[rt][sct]

    return results_matrix


def plt_acc_matrix(acc_matrix, sample_rt, sample_sct, language, k):
    sample_rt = [round(1-rt,2) for rt in sample_rt]
    sample_sct = [round(1-sct,2) for sct in sample_sct]
    plt.matshow(acc_matrix)
    plt.colorbar()
    # plt.title(title +" similarity matrix")
    save_path = './data/semevalplt/{}/'.format(language)
    if not os.path.exists(save_path):
        os.makedirs(save_path)
    file='k{}-acc.png'.format(k)
    # plt.xticks(range(len(sample_rt)), sample_rt.reverse())
    # plt.yticks(range(len(sample_sct)), sample_sct)
    scts = [str(sct)[1:] for sct in sample_sct]
    rts = [str(rt)[1:] for rt in sample_rt]
    # scts = scts[::2]
    # rts = rts[::2]
    # plt.xticks(range(len(scts)), scts, fontsize=7, rotation=45)
    # plt.yticks(range(len(rts)), rts, fontsize=7, rotation=45)
    # plt.xticks(range(0, len(sample_sct), 2), sample_sct[::2], fontsize=7, rotation=45)
    # plt.yticks(range(0, len(sample_rt), 2), sample_rt[::2], fontsize=7)
    plt.xticks(range(0, len(sample_sct), 1), sample_sct, fontsize=7, rotation=45)
    plt.yticks(range(0, len(sample_rt), 1), sample_rt, fontsize=7)

    plt.xlabel("threshold - second stage")
    plt.ylabel("threshold - first stage")
    plt.title("k = {}".format(k), y=-0.18, fontsize=10)
    plt.savefig(save_path+file)
    plt.show()

    # # 创建一个 5x5 的随机矩阵
    # data = np.random.rand(5, 5)
    
    # # 绘制矩阵图，并设置坐标轴标签
    # plt.matshow(data, cmap=plt.cm.Blues)
    # plt.xticks(range(5), ['A', 'B', 'C', 'D', 'E'])
    # plt.yticks(range(5), ['1', '2', '3', '4', '5'])
    # plt.savefig(save_path+file)
    # # 显示图像
    # plt.show()


# import numpy as np
# import matplotlib.pyplot as plt



def draw_acc_matrixs(language, k):
    # language = 'en'
    # k=14
    sample_rt = [0.65, 0.66, 0.67, 0.68, 0.69, 0.70, 0.71, 0.72, 0.73, 0.74, 0.75,
                 0.75, 0.76, 0.77, 0.78, 0.79, 0.80, 0.81, 0.82, 0.83, 0.84, 0.85]
    sample_sct = [0.55, 0.56, 0.57, 0.58, 0.59, 0.60, 0.61, 0.62, 0.63, 0.64, 0.65,
                  0.65, 0.66, 0.67, 0.68, 0.69, 0.70, 0.71, 0.72, 0.73, 0.74, 0.75,]
    # sample_rt = [0.75, 0.76, 0.77, 0.78, 0.79, 0.80, 0.81, 0.82, 0.83, 0.84, 0.85,
    #              0.86, 0.87, 0.88, 0.89, 0.90,]
    # sample_sct = [0.75, 0.76, 0.77, 0.78, 0.79, 0.80, 0.81, 0.82, 0.83, 0.84, 0.85,
    #               0.86, 0.87, 0.88, 0.89, 0.90,]
    rpath = './data/{}/semeval_task/sc_b/k{}/acc_result.pkl'.format(language, k)
    results = load_semeval_results(rpath)
    acc_matrix = construct_semeval_results_matrix(results, sample_rt, sample_sct)
    plt_acc_matrix(acc_matrix, sample_rt, sample_sct, language, k)
    # print(results)


languages = ['en', 'de', 'sv']
# languages = ['la']
sample_k = [10, 12, 14]

for language in languages:
    for k in sample_k:
        draw_acc_matrixs(language, k)

