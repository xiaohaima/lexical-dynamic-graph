import matplotlib.pyplot as plt
import numpy as np
from sklearn.decomposition import PCA
import utils
import rundata as rd
import semantic_tree as st
from sklearn.cluster import KMeans
# from cloudprior import CloudCluster


class SemanticTree:
    def __init__(self, cloud: list, static_embeddings: tuple, hf_clu: list):
        subsemantics = [hfsm.center for hfsm in hf_clu]
        subse_nbs = [hfsm.c_neighbors for hfsm in hf_clu]
        self.cloud = cloud
        self.center_semantic = np.mean(np.array(cloud), axis=0)
        self.static_embeddings = static_embeddings
        self.subsemantics = subsemantics
        self.subse_nbs = subse_nbs
        self.hf_clu = hf_clu

        self.init_plot_data()

    def init_plot_data(self):
        pca_data = [self.center_semantic]
        pca_data += self.subsemantics
        pca_data += self.cloud
        pca_data = np.array(pca_data)
        print("pca_data shape ", pca_data.shape)
        pca = PCA(n_components=2)
        pca = pca.fit(pca_data)
        X_dr = pca.transform(pca_data)

        self.X_dr = X_dr

    def get_center_semantic_point(self):
        return self.X_dr[0, :]

    def get_sub_semantics_points(self):
        len_subses = len(self.subsemantics)
        return self.X_dr[1:len_subses+1, :]

    def get_cloud_points(self):
        len_subses = len(self.subsemantics)
        return self.X_dr[len_subses+1:, :]

    def get_subsemantic_clouds(self, size=10):
        # len_subses = len(self.hf_clu)

        X = []
        sizes = []
        for clu in self.hf_clu:
            clu_cloud = []
            nbs_size = len(clu.points) if len(clu.points) < size else size
            for i in range(nbs_size):
                clu_cloud.append(self.cloud[clu.points[i]])
            # subsemantic_clouds.append(clu_cloud)
            X += clu_cloud
            sizes.append(nbs_size)
        pca = PCA(n_components=2)
        pca = pca.fit(X)
        X_dr = pca.transform(X)
        subsemantic_clouds = []
        startid = 0
        for sz in sizes:
            subsemantic_clouds.append(X_dr[startid:startid+sz])
            startid = startid+sz
        return subsemantic_clouds, X_dr

    def get_nbs_points(self, k=5):
        len_subses = len(self.subsemantics)
        subse_nbs_points = []
        for i in range(len_subses):
            subse_nbs_ = []
            subse_nbs = self.subse_nbs[i]
            k = len(subse_nbs) if k >= len(subse_nbs) else k
            for nb_w in subse_nbs[0:k]:
                nb_p = []
                subse_nbs_.append(nb_w, nb_p)
            subse_nbs_points.append(subse_nbs_)


def load_semantic_tree():
    cloud = utils.load_from_disk('./data/en/words/bit/cloud2')
    stat_emb = utils.load_from_disk('./data/en/words/bit/stat_emb2')
    static_embeds = rd.init_static_embedding()
    static_embeds = st.update_static_embeds(static_embeds, stat_emb)
    hf_semantics = utils.load_from_disk('./data/en/words/bit/cloud_sim_matrix/sct0.6/rt0.73/k12_cloud_hfsc_t2.pkl')
    # subsemantics = [hfsm.center for hfsm in hf_semantics]
    # subse_nbs = [hfsm.c_neighbors for hfsm in hf_semantics]
    stree = SemanticTree(cloud, static_embeds, hf_semantics)
    # print(stree.__dict__)
    return stree


def plot_3dsurface():
    # 绘制面
    fig3 = plt.figure(3)
    ax3 = plt.subplot(projection='3d')
    ax3.set_xlim(0, 50)
    ax3.set_ylim(0, 50)
    ax3.set_zlim(0, 50)
    x = np.arange(1, 50, 1)
    y = np.arange(1, 50, 1)
    X, Y = np.meshgrid(x, y)  # 将坐标向量(x,y)变为坐标矩阵(X,Y)

    # def Z(X, Y):
    #     return X * 0.2 + Y * 0.3 + 20
    def Z(X, Y):
        return X * 0 + Y * 0 + 20

    Zt = Z(X, Y)
    ax3.plot_surface(X=Z(X, Y), Y=Y, Z=X, rstride=10, cstride=10, antialiased=True, color=(0.1, 0.2, 0.5, 0.3))
    ax3.set_xlabel('x轴')
    ax3.set_ylabel('y轴')
    ax3.set_zlabel('z轴')

    x = [30, 40, 50]
    ax3.scatter(x[0], x[1], x[2], s=20, color='b', marker='*')

    x = [20, 30, 30]
    y = [20, 30, 40]
    z = [20, 30, 50]
    ax3.plot3D(xs=x, ys=y, zs=z, color='blue')

    ax3.text(x=20, y=20, z=20, s="xxx", )

    ax3.grid(False)#不显示3d背景网格
    ax3.set_xticks([])#不显示x坐标轴
    ax3.set_yticks([])#不显示y坐标轴
    ax3.set_zticks([])#不显示z坐标轴
    plt.axis('off')#关闭所有坐标轴

    plt.title('三维曲面')

    plt.show()


# plot_3dsurface()


def plot_3d_semantic_tree():
    stree = load_semantic_tree()
    c_ = stree.get_center_semantic_point()
    sub_ = stree.get_sub_semantics_points()
    # cloud = stree.get_cloud_points()
    clu_clouds, cloud = stree.get_subsemantic_clouds(size=5)
    # cloud = np.array(clu_clouds)
    ax_max = np.max(cloud)
    ax_min = np.min(cloud)
    lim_max = ax_max + (ax_max - ax_min) / 8
    lim_min = ax_min - (ax_max - ax_min) / 8
    fig3 = plt.figure(3, figsize=(12, 12))
    ax3 = plt.subplot(projection='3d')
    ax3.set_xlim(lim_min, lim_max)
    ax3.set_ylim(lim_min, lim_max)
    ax3.set_zlim(lim_min, lim_max)
    x = np.arange(lim_min, lim_max, (ax_max - ax_min) / 50)
    y = np.arange(lim_min, lim_max, (ax_max - ax_min) / 50)
    X, Y = np.meshgrid(x, y)  # 将坐标向量(x,y)变为坐标矩阵(X,Y)

    t = lim_min + (lim_max - lim_min) / 4
    def Z(X, Y):
        return X * 0 + Y * 0 + t

    Zt = Z(X, Y)
    ax3.plot_surface(X=Z(X, Y), Y=Y, Z=X, rstride=10, cstride=10, antialiased=True, color=(0.1, 0.2, 0.5, 0.1))

    c_ = np.mean(np.array(cloud), axis=0)
    ax3.scatter(t, c_[0], c_[1], s=50, color='r', marker='o')
    for i in range(sub_.shape[0]):
        point = sub_[i, :]
        # ax3.scatter(t, point[0], point[1], s=20, color='g', marker='^')

    markers = ['o', '*', ]
    colors = ["#00CED1", "#C71585", "#DAA520", "#FF6347"]
    for c, clu_cloud in enumerate(clu_clouds):
        clu_center = np.mean(clu_cloud, axis=0)
        ax3.scatter(t, clu_center[0], clu_center[1], alpha=0.8, c=colors[c], s=22, marker='o')
        kmeans = KMeans(n_clusters=2).fit(clu_cloud)
        sub_nbs_c_ = kmeans.cluster_centers_
        for i in range(sub_nbs_c_.shape[0]):
            point = sub_nbs_c_[i, :]
            ax3.scatter(t, point[0], point[1], alpha=0.3, c=colors[c], s=18, marker='*')
        for i in range(clu_cloud.shape[0]):
            point = clu_cloud[i, :]
            ax3.scatter(t, point[0], point[1], alpha=0.3, c=colors[c], s=5, marker='o')



    # ax3.grid(False)#不显示3d背景网格
    # ax3.set_xticks([])#不显示x坐标轴
    # ax3.set_yticks([])#不显示y坐标轴
    # ax3.set_zticks([])#不显示z坐标轴
    # plt.axis('off')#关闭所有坐标轴

    plt.title('三维曲面')
    plt.show()


plot_3d_semantic_tree()




