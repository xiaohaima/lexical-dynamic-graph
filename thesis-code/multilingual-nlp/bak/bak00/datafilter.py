import json
import os
import pickle
from sentence_splitter import SentenceSplitter, split_text_into_sentences


def save_to_disk(pickle_f, obj):
    with open(pickle_f, "wb") as f:
        pickle.dump(obj, f)


def load_from_disk(pickle_f):
    with open(pickle_f, "rb") as f:
        obj = pickle.load(f)
    return obj


def all_files(base):
    for root, ds, fs in os.walk(base):
        for f in fs:
            fullname = os.path.join(root, f)
            yield fullname


def key_word_filter(key_word, tile):
    # tile = 'enp1p41242'
    base = tile+'-json/'
    files = []
    for file in all_files(base):
        # convert to linux file path
        file = '/'.join(file.split('\\'))
        # print(file)
        files.append(file)

    data_list = []
    # key_word = 'mouse'
    for filename in files:
        with open(filename, "r") as load_f:
            for js_str in load_f.readlines():
                try:
                    # "json.load" load file_ptr
                    # "json.loads" load string
                    wiki = json.loads(js_str)
                except ValueError as err:
                    print("SetJsonOperator load json error ", err)
                    return None
                if key_word in wiki['text']:
                    # print(len(wiki['text']), " ", wiki)
                    data_list.append(wiki)
        # break

    print(len(data_list))
    save_to_disk("./filter/"+key_word+'_'+tile, data_list)


def text_truncate(key_word, tile, language, max_length=30):
    target_texts = []
    splitter = SentenceSplitter(language=language)
    data = load_from_disk("./filter/" + key_word + '_' + tile)
    print("data len", len(data))
    for wiki in data:
        text = wiki['text']
        result = splitter.split(text)
        # print(len(result))
        # print(result)
        for sentence in result:
            if key_word in sentence:
                target_texts.append(sentence)

    print("target_texts len", len(target_texts))
    # for sen in target_texts:
    #     print(len(sen), sen)
    save_to_disk("./filter/" + key_word + '_st_' + tile, target_texts)


def main():
    tile = 'svp153416p666977'
    key_word = 'mus'
    # key_word_filter(key_word, tile)
    text_truncate(key_word, tile, language='sv')


if __name__ == '__main__':
    main()







