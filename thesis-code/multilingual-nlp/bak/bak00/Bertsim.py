import torch
import utils
from datasets import load_dataset
from transformers import BertTokenizer
from transformers import BertModel


#加载字典和分词工具

BERT_MODEL = 'KB/bert-base-swedish-cased'
language = 'swi'

# token = BertTokenizer.from_pretrained('bert-base-chinese')
token = BertTokenizer.from_pretrained(BERT_MODEL)

print(token)

vocab = token.get_vocab()

# print(vocab)


class Dataset(torch.utils.data.Dataset):
    def __init__(self, voc_data):
        data = []
        for key in voc_data:
            data.append(key)

        self.dataset = data

    def __len__(self):
        return len(self.dataset)

    def __getitem__(self, i):
        text = self.dataset[i]
        return text


dataset = Dataset(vocab)


def collate_fn(data_raw):
    #编码
    data = token.batch_encode_plus(batch_text_or_text_pairs=data_raw,
                                   truncation=True,
                                   padding='max_length',
                                   max_length=3,
                                   return_tensors='pt',
                                   return_length=True)

    #input_ids:编码之后的数字
    #attention_mask:是补零的位置是0,其他位置是1
    input_ids = data['input_ids']
    attention_mask = data['attention_mask']
    token_type_ids = data['token_type_ids']

    return input_ids, attention_mask, token_type_ids


#数据加载器
loader = torch.utils.data.DataLoader(dataset=dataset,
                                     batch_size=16,
                                     collate_fn=collate_fn,
                                     shuffle=False,
                                     drop_last=True)

for i, (input_ids, attention_mask, token_type_ids) in enumerate(loader):
    break

print(len(loader))
print(token.decode(input_ids[0]))
# # print(token.decode(labels[0]))
# print(input_ids.shape, attention_mask.shape, token_type_ids.shape)


# for i, (input_ids, attention_mask, token_type_ids) in enumerate(loader):
#     # if i < 45 or i > 50:
#     #     continue
#     for j in range(len(input_ids)):
#         print(token.decode(input_ids[j]))


#加载预训练模型
# pretrained = BertModel.from_pretrained('bert-base-chinese')
pretrained = BertModel.from_pretrained(BERT_MODEL)


#不训练,不需要计算梯度
for param in pretrained.parameters():
    param.requires_grad_(False)

#模型试算
out = pretrained(input_ids=input_ids,
           attention_mask=attention_mask,
           token_type_ids=token_type_ids)

print(out.last_hidden_state.shape)



#定义下游任务模型
class Model(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.decoder = torch.nn.Linear(768, token.vocab_size, bias=False)
        self.bias = torch.nn.Parameter(torch.zeros(token.vocab_size))
        self.decoder.bias = self.bias

    def forward(self, input_ids, attention_mask, token_type_ids):
        with torch.no_grad():
            out = pretrained(input_ids=input_ids,
                             attention_mask=attention_mask,
                             token_type_ids=token_type_ids)

        # out = self.decoder(out.last_hidden_state[:, 15])
        out = out.last_hidden_state.numpy()
        return out


model = Model()


index_voc = {}
voc_index = {}
embed = {}
for i, (input_ids, attention_mask, token_type_ids) in enumerate(loader):
    if i % 50 == 0:
        print("loop i ", i)
    out = model(input_ids=input_ids,
                attention_mask=attention_mask,
                token_type_ids=token_type_ids)
    for j, ids in enumerate(input_ids):
        # word = token.decode(ids[1])
        word = token.decode([ids[1]])
        idsnp = ids.numpy()
        # word = words[1]
        id = idsnp[1]
        embed[id] = out[j][1]
        index_voc[id] = word
        voc_index[word] = id


print(voc_index)
# print(embed.shape)
print("xiaohema")


static_embed = (index_voc, voc_index, embed)
utils.save_to_disk(language+"_static_embed.pkl", static_embed)

print("xiaohema finished")


