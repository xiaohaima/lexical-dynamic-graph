import matplotlib.pyplot as plt
from sklearn.datasets import load_iris
from sklearn.decomposition import PCA
import utils
from sklearn.cluster import KMeans
from scipy.spatial.distance import cdist
import numpy as np
from sklearn.metrics import silhouette_score


def find_k_elbow_rule(X, K_max=10, eva=3):
    distortions = []
    K = range(1, K_max)
    curves = {}
    for k in K:
        # KMeans 是基于euclidean dis计算，而不是cos dis计算
        kmeanModel = KMeans(n_clusters=k).fit(X)
        # distortions.append(sum(np.min(cdist(X, kmeanModel.cluster_centers_, 'euclidean'), axis=1)) / X.shape[0])
        distortions.append(kmeanModel.inertia_)
        # distortions.append(silhouette_score(X, kmeanModel.labels_, metric='euclidean'))

    for k in range(2, K_max-1):
        curve = (distortions[k - 2] - distortions[k - 1]) - (distortions[k - 1] - distortions[k])
        curves[k] = curve

    curves_sorted = sorted(curves.items(), key=lambda kv: kv[1], reverse=True)
    eva_k = curves_sorted[0][0]
    optimal_k = 1
    if eva_k < (K_max/2):
        judge_k_v = ((distortions[0] - distortions[eva_k - 1]) / (eva_k - 1)) / (
                (distortions[eva_k - 1] - distortions[-1]) / (K[-1] - eva_k))
        judge_k = judge_k_v > eva
        optimal_k = eva_k if judge_k else 1
        print(eva_k)
        print(judge_k_v)
        print(optimal_k)

    # plt.plot(K, distortions, 'bx-')
    # plt.xlabel('k')
    # plt.ylabel('Distortion')
    # plt.title('The Elbow Method showing the optimal k')
    # plt.show()

    return optimal_k


def find_embeding_centers(target_embeds):
    # target_embeds = utils.load_from_disk("mouse_target_embeds.pkl")

    X = target_embeds
    X = np.array(X)
    nm = np.sqrt((X ** 2).sum(axis=1))[:, None]
    X = X / nm

    k = find_k_elbow_rule(X, K_max=10, eva=3)
    kmeans = KMeans(n_clusters=k).fit(X)
    print(kmeans.cluster_centers_.shape)
    # index_voc, voc_index, embeddings = utils.load_from_disk("en_static_embed.pkl")
    # for embed in kmeans.cluster_centers_:
    #     print('+++++++++++++++++++++++')
    #     utils.most_sim_words(embed, 8, voc_index, embeddings)

    return kmeans.cluster_centers_


def evolve_branch(embeds1, embeds2):
    # e1 = np.mean(embeds1, axis=0)
    # e2 = np.mean(embeds2, axis=0)
    kmeans = KMeans(n_clusters=1).fit(embeds1)
    e1 = kmeans.cluster_centers_[0]

    kmeans = KMeans(n_clusters=1).fit(embeds2)
    e2 = kmeans.cluster_centers_[0]

    return (e1, e2)


def polysemy_branch(embeds):
    poly_embeds = find_embeding_centers(embeds)
    # poly_edges = []
    # for poly in poly_embeds:
    #     poly_edges.append((ec, poly))
    return poly_embeds


def semantic_branch(poly_embeds, static_embeds):
    index_voc, voc_index, embeddings = static_embeds
    sim_edges_all = []
    for embed in poly_embeds:
        print('+++++++++++++++++++++++')
        sim_edges = []
        words = utils.most_sim_words(embed, 5, voc_index, embeddings)
        for sim_word in words:
            sim_w = sim_word[0]
            sim_e = sim_word[2]
            sim_edges.append(embed, sim_e)

        sim_edges_all.append(sim_edges)

    return sim_edges_all


def similarity_edges(poly_ids, X, static_embeds):
    index_voc, voc_index, embeddings = static_embeds
    sim_edges_all = []
    for poly_id in poly_ids:
        embed = X[poly_id]
        words = utils.most_sim_words(embed, 3, voc_index, embeddings)
        sim_edges = []
        for sim_word in words:
            sim_w = sim_word[0]
            sim_e = sim_word[2]
            sim_edges.append((poly_id, len(X)))
            X.append(sim_e)

        sim_edges_all.append(sim_edges)

    return sim_edges_all


def similarity_edges_approx(key_word, poly_edges, poly_ids, X, static_embeds, X_dr, simsize=5):
    # print("similarity_edges_approx ========= ")
    index_voc, voc_index, embeddings = static_embeds
    sim_edges_all = []
    for j, poly_id in enumerate(poly_ids):
        # print("similarity_edges_approx poly_ids +++++ ", poly_id)
        poly_edge = poly_edges[j] if len(poly_edges) > 1 else (0,1)
        embed = X[poly_id]
        words = utils.most_sim_words(embed, simsize, voc_index, embeddings, willprint=True)
        sim_edges = []
        for sim_word in words:
            sim_w = sim_word[0]
            if key_word == sim_w:
                continue
            # print(sim_w)
            id = find_most_sim(sim_w, static_embeds, X, X_dr, poly_edge, poly_id, sim_edges)
            sim_edges.append(((poly_id, id), sim_w))

        sim_edges_all.append(sim_edges)

    return sim_edges_all


def distance(a, b):
    return np.linalg.norm(a-b)


def find_most_sim(word, static_embeds, X, X_dr=None, poly_edge=None, poly_id=None, sim_edges=None):
    index_voc, voc_index, embeddings = static_embeds
    e = utils.word_voc(word, voc_index, embeddings)
    emb_sim = {}
    for i, emb in enumerate(X):
        sim = utils.cosine_similarity(e, emb)
        exists_ids = [edge[0][1] for edge in sim_edges]
        if poly_edge is not None and poly_id != i and i not in exists_ids:
            l = distance(X_dr[poly_edge[0]], X_dr[poly_edge[1]])
            a = X_dr[poly_id]
            b = X_dr[i]
            r = distance(a, b)
            if 1.5*r < l:
                emb_sim[i] = sim
        # else:
        #     emb_sim[i] = sim

    emb_sim_sorted = sorted(emb_sim.items(), key=lambda kv: kv[1], reverse=True)

    most_sim_id = emb_sim_sorted[0][0]

    return most_sim_id


def plt_sim_nodes(sim_edges_list, X_dr):
    labeled = False
    for sim_edges_ in sim_edges_list:
        for sim_edges in sim_edges_:
            for sim_edge, sim_w in sim_edges:
                sim_id = sim_edge[1]
                if labeled:
                    plt.scatter(X_dr[sim_id, 0], X_dr[sim_id, 1], alpha=0.9, c='#1E90FF', s=16, zorder=2)
                else:
                    plt.scatter(X_dr[sim_id, 0], X_dr[sim_id, 1], alpha=0.9, c='#1E90FF', s=16, zorder=2, label='Similar embedding')
                    labeled = True
                plt.text(X_dr[sim_id, 0], X_dr[sim_id, 1], sim_w, fontsize=10, color="blue", style="italic", weight="light",
                         verticalalignment='center', horizontalalignment='right', rotation=45, alpha=0.5)


def plt_sim_lines(sim_edges_list, X_dr):
    labeled = False
    for sim_edges_ in sim_edges_list:
        for sim_edges in sim_edges_:
            for sim_edge, sim_w in sim_edges:
                if labeled:
                    plt.plot(X_dr[sim_edge, 0], X_dr[sim_edge, 1], c='#7FFFD4', zorder=1)
                else:
                    plt.plot(X_dr[sim_edge, 0], X_dr[sim_edge, 1], c='#7FFFD4', zorder=1, label='Similarity cluster')
                    labeled = True


def plt_poly_nodes(poly_ids_list, X_dr):
    labeled = False
    for poly_ids in poly_ids_list:
        for poly_id in poly_ids:
            if labeled:
                plt.scatter(X_dr[poly_id, 0], X_dr[poly_id, 1], alpha=0.9, c='#FF4500', s=40, zorder=3)
            else:
                plt.scatter(X_dr[poly_id, 0], X_dr[poly_id, 1], alpha=0.9, c='#FF4500', s=40, zorder=3, label='Polysemy embedding')
                labeled = True


def plt_poly_lines(poly_edges_list, X_dr):
    labeled = False
    for poly_edges in poly_edges_list:
        for poly_edge in poly_edges:
            if labeled:
                plt.plot(X_dr[poly_edge, 0], X_dr[poly_edge, 1], c='#FF7F50', zorder=1)
            else:
                plt.plot(X_dr[poly_edge, 0], X_dr[poly_edge, 1], c='#FF7F50', zorder=1, label='Polysemy')
                labeled = True


def plt_evolve_nodes(X_dr):
    plt.scatter(X_dr[0, 0], X_dr[0, 1], alpha=0.99, c='#000080', s=60, zorder=4, label='Center embedding before evolve')
    plt.scatter(X_dr[1, 0], X_dr[1, 1], alpha=0.99, c='#006400', s=60, zorder=4, label='Center embedding after evolve')


def plt_evolve_lines(evolve_edge, X_dr):
    plt.plot(X_dr[evolve_edge, 0], X_dr[evolve_edge, 1], c='red', zorder=1, label='Evolve')


def plt_embedding_distribution(xsize, size1, size2, X_dr):
    plt.scatter(X_dr[xsize:xsize+size1, 0], X_dr[xsize:xsize+size1, 1],
                alpha=0.3, c='#FF1493', s=5, label='Embeddings before evolve')
    plt.scatter(X_dr[xsize+size1:xsize+size1+size2, 0], X_dr[xsize+size1:xsize+size1+size2, 1],
                alpha=0.3, c='#778899', s=5, label='Embeddings after evolve')


def plt_evolve_tree(key_word, embeds1, embeds2, static_embeds, simsize=5, title='Evolve Tree'):
    e1, e2 = evolve_branch(embeds1, embeds2)
    X = [e1, e2]
    evolve_edge = (0, 1)

    poly_edges1 = []
    poly_embeds1 = polysemy_branch(embeds1)
    poly_ids1 = []
    if len(poly_embeds1) > 1:
        for poly in poly_embeds1:
            poly_ids1.append(len(X))
            poly_edges1.append((0, len(X)))
            X.append(poly)
    else:
        poly_ids1 = [0]

    poly_embeds2 = polysemy_branch(embeds2)
    poly_edges2 = []
    poly_ids2 = []
    if len(poly_embeds2) > 1:
        for poly in poly_embeds2:
            poly_ids2.append(len(X))
            poly_edges2.append((1, len(X)))
            X.append(poly)
    else:
        poly_ids2 = [1]

    xsize = len(X)
    X = np.array(X)
    print("X shape ", X.shape)
    X = np.concatenate((X, embeds1, embeds2), axis=0)
    print("X shape concatenate ", X.shape)
    pca = PCA(n_components=2)
    pca = pca.fit(X)
    X_dr = pca.transform(X)
    # print(X_dr)

    sim_edges1 = similarity_edges_approx(key_word, poly_edges1, poly_ids1, X, static_embeds, X_dr, simsize)
    sim_edges2 = similarity_edges_approx(key_word, poly_edges2, poly_ids2, X, static_embeds, X_dr, simsize)

    plt.figure(figsize=(10, 8))
    # plt.scatter(X_dr[:, 0], X_dr[:, 1], alpha=0.2, c='gray', s=5)
    plt_embedding_distribution(xsize, embeds1.shape[0], embeds2.shape[0], X_dr)

    plt_sim_lines([sim_edges1, sim_edges2], X_dr)
    plt_poly_lines([poly_edges1, poly_edges2], X_dr)
    plt_evolve_lines(evolve_edge, X_dr)

    plt_sim_nodes([sim_edges1, sim_edges2], X_dr)
    plt_poly_nodes([poly_ids1, poly_ids2], X_dr)
    plt_evolve_nodes(X_dr)

    plt.legend()
    plt.title(title)
    plt.show()
    print("plt done")


def gen_evolve_tree_en():
    key_word = 'mouse'
    target_embeds1 = utils.load_from_disk("mouse_target_embeds_c01.pkl")
    X = np.array(target_embeds1)
    nm = np.sqrt((X ** 2).sum(axis=1))[:, None]
    embeds1 = X / nm

    target_embeds2 = utils.load_from_disk("mouse_target_embeds.pkl")
    X = np.array(target_embeds2)
    nm = np.sqrt((X ** 2).sum(axis=1))[:, None]
    embeds2 = X / nm

    static_embeds = utils.load_from_disk("en_static_embed.pkl")
    title = 'Semantic evolve tree for English word "mouse" '
    plt_evolve_tree(key_word, embeds1, embeds2, static_embeds, simsize=8, title=title)


def gen_evolve_tree_de():
    key_word = 'Maus'
    target_embeds1 = utils.load_from_disk("Maus_de_ccohaembeds.pkl01")
    X = np.array(target_embeds1)
    nm = np.sqrt((X ** 2).sum(axis=1))[:, None]
    embeds1 = X / nm

    target_embeds2 = utils.load_from_disk("Maus_de_wikiembeds.pkl")
    X = np.array(target_embeds2)
    nm = np.sqrt((X ** 2).sum(axis=1))[:, None]
    embeds2 = X / nm

    static_embeds = utils.load_from_disk("en_static_embed.pkl")
    title = 'Semantic evolve tree for German word "Maus" '
    plt_evolve_tree(key_word, embeds1, embeds2, static_embeds, simsize=8, title=title)


def gen_evolve_tree_swi():
    key_word = 'mus'
    target_embeds1 = utils.load_from_disk("mus_swi_ccohaembeds.pkl01")
    X = np.array(target_embeds1)
    nm = np.sqrt((X ** 2).sum(axis=1))[:, None]
    embeds1 = X / nm

    target_embeds2 = utils.load_from_disk("mus_swi_wikiembeds.pkl")
    X = np.array(target_embeds2)
    nm = np.sqrt((X ** 2).sum(axis=1))[:, None]
    embeds2 = X / nm

    static_embeds = utils.load_from_disk("swi_static_embed.pkl")
    title = 'Semantic evolve tree for Swedish word "mus" '
    plt_evolve_tree(key_word, embeds1, embeds2, static_embeds, simsize=8, title=title)


if __name__ == '__main__':
    # gen_evolve_tree_en()
    # gen_evolve_tree_de()
    gen_evolve_tree_swi()

