import pickle
import numpy as np


def stop_words(language=None):
    support_languages = {'en': "./data/stop_words_english.txt"}
    if language not in support_languages:
        print("Unsupported stop word for language ", language)
        return []
    with open(support_languages['en'], "r", encoding="utf-8") as f:
        swl = f.read().split('\n')
    return swl


def rm_stop_words(sentence, stop_words_list):
    new_sentence = [word for word in sentence if word not in stop_words_list]
    return new_sentence


def remove_stop_words(corpus, language):
    stop_words_list = stop_words(language)
    new_corpus = []
    for sentence in corpus:
        new_corpus.append(rm_stop_words(sentence, stop_words_list))
    return new_corpus


def prepare_coupus_from_txt(data_file, language):
    with open(data_file, "r", encoding="utf-8") as corpus_data:
        corpus_raw = corpus_data.read().split('\n')
    corpus_cut = [sentence.split() for sentence in corpus_raw]
    corpus = remove_stop_words(corpus_cut, language)
    return corpus
    # print(corpus_cut)


def read_coupus_from_txt(key_word, data_file):
    with open(data_file, "r", encoding="utf-8") as corpus_data:
        corpus_raw = corpus_data.read().split('\n')
    data_list = []
    for sentence in corpus_raw:
        if key_word in sentence:
            data_list.append(sentence)
    print("data_list len ", len(data_list))
    return data_list

# stop_words = stop_words('en')
# print(stop_words)


def save_to_disk(pickle_f, obj):
    with open(pickle_f, "wb") as f:
        pickle.dump(obj, f)


def load_from_disk(pickle_f):
    with open(pickle_f, "rb") as f:
        obj = pickle.load(f)
    return obj


def word_voc(word, voc_index, embeddings):
    id = voc_index[word]
    return embeddings[id]


def cosine_similarity(v_w1, v_w2):
    theta_sum = np.dot(v_w1, v_w2)
    theta_den = np.linalg.norm(v_w1) * np.linalg.norm(v_w2)
    theta = theta_sum / theta_den
    return theta


def most_sim_words(v_w1, top_n, voc_index, embeddings, willprint=True):
    word_sim = {}
    for word_c in voc_index:
        v_w2 = word_voc(word_c, voc_index, embeddings)
        theta = cosine_similarity(v_w1, v_w2)
        word_sim[word_c] = theta
    words_sorted = sorted(word_sim.items(), key=lambda kv: kv[1], reverse=True)
    words = []
    for word, sim in words_sorted[:top_n]:
        if willprint:
            print(word, sim)
        words.append((word, sim, word_voc(word, voc_index, embeddings)))

    return words


def word_sim(word, top_n, voc_index, embeddings):
    print("+++++++++++ eva word_sim ", word)
    v_w1 = word_voc(word, voc_index, embeddings)
    sim_words = most_sim_words(v_w1, top_n, voc_index, embeddings)
    return sim_words


def test_embeds_sim(key_word):
    index_voc, voc_index, embeddings = load_from_disk("de_static_embed.pkl")
    word_sim(key_word, 20, voc_index, embeddings)
    target_embeds = load_from_disk("Maus_de_wikiembeds.pkl")
    for i, embed in enumerate(target_embeds):
        print("++++++++ ++++++ embed i ", i)
        most_sim_words(embed, 3, voc_index, embeddings)


def words_sim(w1,w2,voc_index, embeddings):
    wv1 = word_voc(w1, voc_index, embeddings)
    wv2 = word_voc(w2, voc_index, embeddings)
    theta_sum = np.dot(wv1, wv2)
    theta_den = np.linalg.norm(wv1) * np.linalg.norm(wv2)
    theta = theta_sum / theta_den
    return theta

# test_embeds_sim('mouse')
# test_embeds_sim('Maus')

# index_voc, voc_index, embeddings = load_from_disk("de_static_embed.pkl")
# print('words_sim', words_sim('Maus','Tastatur',voc_index, embeddings))

