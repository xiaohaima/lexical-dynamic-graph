
import cloudprior as cl
from cloudprior import CloudCluster
import utils
# from main import evaluate_task1


def evaluate_semeval_task1(target_results: dict, language: str):
    targets = 'corpus/semeval2020_ulscd_eng/targets.txt'
    with open(targets, "r", encoding="utf-8") as targets_data:
        targets_words = targets_data.read().split('\n')
    import semeval_official as seo
    # truth_path = "./corpus/semeval2020_ulscd_eng/truth/binary.txt"
    # pred_path = "./corpus/en/semevalSC_r/semantic_change_results.txt"
    # target_results = utils.load_from_disk('./corpus/en/semevalSC_r/semantic_change_results')
    truth_path = './corpus/{}/semeval_task/binary.txt'.format(language)
    pred_path = './corpus/{}/semeval_task/semantic_change_results.txt'.format(language)
    results_str = ""
    for t_word in target_results:
        save_word = ""
        for targets_word in targets_words:
            if t_word in targets_word:
                save_word = targets_word
                break
        results_str = results_str + save_word + "\t" + str(target_results[t_word]) + "\n"
    res_file = open(pred_path, 'w', encoding='utf-8')
    res_file.write(results_str)
    res_file.close()
    acc = seo.accuracy_official(truth_path, pred_path)
    print(" Sem eval task 1 accuracy = ", acc)
    return acc


def semeval_task1(language, k, sct, rt):
    print("Run semeval cloud distance. Language {}".format(language))
    keywords = utils.load_from_disk('./corpus/{}/semeval_task/keywords.pkl'.format(language))
    # keywords = ["stroke"]

    pmodel = 'bert-base-multilingual-cased'  # 'bert-base-uncased'
    corpus1 = './corpus/{}/semeval_task/data1.pkl'.format(language)
    corpus2 = './corpus/{}/semeval_task/data2.pkl'.format(language)
    semantic_change_hf, semantic_change_hf_lf =\
        cl.run_cloud(keywords, pmodel, corpus1, corpus2, language,
                     lan_emb=None, k=k, separate=None, rt=rt, sct=sct)
    acc_hf = evaluate_semeval_task1(semantic_change_hf, language)
    acc_hf_lf = evaluate_semeval_task1(semantic_change_hf_lf, language)
    print("semeval_task1 accuracy acc_hf {} acc_hf_lf {}".format(acc_hf, acc_hf_lf))
    return acc_hf, acc_hf_lf


def exec_semeval_task1():
    languages = ['en']
    sample_sct = [0.60]
    sample_rt = [0.73]
    sample_k = [12]
    for language in languages:
        sc_task1_results = []
        for k in sample_k:
            for sct in sample_sct:
                for rt in sample_rt:
                    acc_hf, acc_hf_lf = semeval_task1(language, k, sct, rt)
                    sc_task1_result = {'acc': acc_hf, 'acc_hf': acc_hf, 'acc_hf_lf': acc_hf_lf,
                                       'k': k, 'sct': sct, 'rt': rt}
                    sc_task1_results.append(sc_task1_result)

        sc_task1_result_path = \
            "./data/{}/semeval_task/sc_b/sct{}/rt{}/".format(language, sct, rt)
        utils.save_to_disk(sc_task1_result_path+'acc_result.pkl', sc_task1_results)
        utils.save_as_txt(sc_task1_result_path+'acc_result.txt', str(sc_task1_results))
        print("sc_task1_results", str(sc_task1_results))


if __name__ == '__main__':
    exec_semeval_task1()
