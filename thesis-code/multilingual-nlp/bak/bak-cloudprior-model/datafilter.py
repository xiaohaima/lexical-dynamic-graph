import json
import os
import pickle
import utils
import math
import multiprocessing as mp
from tqdm import tqdm
from sentence_splitter import SentenceSplitter, split_text_into_sentences


def check_key_words_in(sentence, keyword):
    enable_start = [' ', '.', '"', "(", ")", "'", ',', ":", "!", "?", ';', '[', '{']
    enable_end = [' ', '.', '"', "(", ")", "'", ',', ":", "!", "?", ';', ']', '}']
    sen = sentence
    beg = 0
    while beg < len(sen):
        idx = sen.find(keyword, beg)
        if idx == -1:
            return False

        i = idx + len(keyword)
        check_end = False
        if (i < len(sen) and sen[i] in enable_end) or i == len(sen):
            check_end = True
        check_start = False
        if (idx > 0 and sen[idx-1] in enable_start) or idx == 0:
            check_start = True

        if check_start and check_end:
            return True
        else:
            beg = i

    return False


def filter_keyword_sentences(text, keyword, splitter, case=False):
    sentences = []
    sens = splitter.split(text)
    # sens = [text]
    # print(len(sens))
    for sentence in sens:
        if keyword in sentence:
            # check key word is an individual word in sentence
            check_w = check_key_words_in(sentence, keyword)
            if check_w:
                sentence = sentence.lower() if case else sentence
                sentences.append(sentence)
    return sentences


def read_coupus_from_wiki(data_dir, keyword, language):
    print("exe filter_wikidata_from_keyword keyword ", keyword)
    splitter = SentenceSplitter(language=language)
    files = utils.find_files(data_dir)
    sen_data = []
    for filename in tqdm(files):
        # print("filename {} target data len {}".format(filename, len(sen_data)))
        with open(filename, "r") as load_f:
            for js_str in load_f.readlines():
                try:
                    # "json.load" load file_ptr
                    # "json.loads" load string
                    wiki = json.loads(js_str)
                except ValueError as err:
                    print("SetJsonOperator load json error ", err)
                    return None
                sentences = filter_keyword_sentences(wiki['text'], keyword, splitter)
                sen_data.extend(sentences)

    print("wiki sentences data number for word {}:".format(keyword), len(sen_data))
    return sen_data


def read_coupus_from_txt(datafile, keyword, language):
    with open(datafile, "r", encoding="utf-8") as corpus_data:
        corpus_raw = corpus_data.read().split('\n')
    splitter = SentenceSplitter(language=language)
    sen_data = []
    for sentence in tqdm(corpus_raw):
        sentences = filter_keyword_sentences(sentence, keyword, splitter)
        sen_data.extend(sentences)
    print("text sentences data number for word {}:".format(keyword), len(sen_data))
    return sen_data


def data_from_wiki_mp(data_dir, keywords, language, case=False, p_number=4):
    manager = mp.Manager()
    return_dict = manager.dict()
    files = utils.find_files(data_dir)
    step = int(math.ceil(len(files)/p_number))
    procs = []
    for pid in range(p_number):
        # if pid*step >= len(files):
        #     break
        endf = (pid+1)*step
        endf = endf if endf < len(files) else len(files)
        files_p = files[pid*step:endf]
        p = mp.Process(target=data_from_wiki_files, args=(
            files_p, keywords, language, pid, return_dict, case))
        p.start()
        procs.append(p)
        print("start process id ", pid)

    for p in procs:
        p.join()

    print(return_dict)
    sen_data = {}
    for keyword in keywords:
        sen_data[keyword] = []
        for pid in range(p_number):
            sen_data[keyword].extend(return_dict[pid][keyword])

    for keyword in keywords:
        print("text sentences data number for word {}:".format(keyword), len(sen_data[keyword]))
    return sen_data


def data_from_wiki_files(files, keywords, language, pidx, return_dict, case=False):
    splitter = SentenceSplitter(language=language)
    sen_data = {}
    for keyword in keywords:
        sen_data[keyword] = []
    for filename in tqdm(files):
        with open(filename, "r") as load_f:
            for js_str in load_f.readlines():
                try:
                    wiki = json.loads(js_str)
                except ValueError as err:
                    print("SetJsonOperator load json error ", err)
                    return None
                text = wiki['text']
                # 会影响到splitter
                # text = text.lower() if case else text
                for keyword in keywords:
                    sentences = filter_keyword_sentences(text, keyword, splitter, case=case)
                    sen_data[keyword].extend(sentences)

    return_dict[pidx] = sen_data
    # for keyword in keywords:
    #     print("text sentences data number for word {}:".format(keyword), len(sen_data[keyword]))


def data_from_wiki(data_dir, keywords, language, case=False):
    print("exe data_from_wiki keywords ", keywords)
    splitter = SentenceSplitter(language=language)
    files = utils.find_files(data_dir)
    sen_data = {}
    for keyword in keywords:
        sen_data[keyword] = []
    for filename in tqdm(files):
        with open(filename, "r") as load_f:
            for js_str in load_f.readlines():
                try:
                    # "json.load" load file_ptr
                    # "json.loads" load string
                    wiki = json.loads(js_str)
                except ValueError as err:
                    print("SetJsonOperator load json error ", err)
                    return None
                text = wiki['text']
                # 会影响到splitter
                # text = text.lower() if case else text
                for keyword in keywords:
                    sentences = filter_keyword_sentences(text, keyword, splitter, case=case)
                    sen_data[keyword].extend(sentences)

    for keyword in keywords:
        print("text sentences data number for word {}:".format(keyword), len(sen_data[keyword]))
    return sen_data


def data_from_txt_process(corpus, keywords, splitter, case, pidx, return_dict):
    # splitter = SentenceSplitter(language=language)
    sen_data = {}
    for keyword in keywords:
        sen_data[keyword] = []
    for text in tqdm(corpus):
        text = text.lower() if case else text
        for keyword in keywords:
            sentences = filter_keyword_sentences(text, keyword, splitter)
            sen_data[keyword].extend(sentences)
    return_dict[pidx] = sen_data


def data_from_txt(datafile, keywords, language, case=False, p_number=6):
    print("exe data_from_txt keywords ", keywords)
    with open(datafile, "r", encoding="utf-8") as corpus_data:
        corpus_raw = corpus_data.read().split('\n')
    splitter = SentenceSplitter(language=language)
    sen_data = {}
    for keyword in keywords:
        sen_data[keyword] = []
    # corpus_raw = corpus_raw[0:1600] # for test running
    if p_number < 2:
        for text in tqdm(corpus_raw):
            text = text.lower() if case else text
            for keyword in keywords:
                sentences = filter_keyword_sentences(text, keyword, splitter)
                sen_data[keyword].extend(sentences)
    else:
        manager = mp.Manager()
        return_dict = manager.dict()
        step = int(math.ceil(len(corpus_raw) / p_number))
        procs = []
        for pid in range(p_number):
            endf = (pid + 1) * step
            endf = endf if endf < len(corpus_raw) else len(corpus_raw)
            corpus_raw_p = corpus_raw[pid * step:endf]
            p = mp.Process(target=data_from_txt_process, args=(
                corpus_raw_p, keywords, splitter, case, pid, return_dict))
            p.start()
            procs.append(p)
            print("start process id ", pid)
        for p in procs:
            p.join()

        for keyword in keywords:
            for pid in range(p_number):
                sen_data[keyword].extend(return_dict[pid][keyword])

    for keyword in keywords:
        print("text sentences data number for word {}:".format(keyword), len(sen_data[keyword]))
    return sen_data


def filter_en():
    # examples read data for keywords list
    # keywords = ['mouse', 'cloud', 'stream', 'virus', 'desktop', 'browser', 'hacker', 'spam', 'troll', 'phone',
    #             'tablet', 'upload', 'download', 'friend', 'post', 'profile', 'filter', 'cyber', 'data', 'domain',
    #             'online', 'digital', 'smart', 'warable', 'robot', 'automobile', 'computer', 'laptop', 'speaker',
    #             'camera', 'remote']
    keywords = ['mouse', 'cloud', 'stream', 'virus', 'troll', 'tablet', 'friend', 'post', 'profile',
                'filter', 'data', 'domain', 'smart', 'speaker', 'camera', 'remote']
    # keywords = ['mouse']
    language = 'en'
    period = 'wiki'
    tile = 'p1p41242'
    data_dir = '../wikiextractor-master/enp1p41242-json'
    # sen_data = data_from_wiki(data_dir, keywords, language, case=True)
    sen_data = data_from_wiki_mp(data_dir, keywords, language, case=False, p_number=16)
    save_path = "./corpus/{}/{}/{}".format(language, period, 'data_' + tile)
    utils.save_to_disk(save_path, sen_data)

    # period = 'semeval'
    # tile = 'ccoha1'
    # datafile = '../wikiextractor-master/data/ccoha1.txt'
    # sen_data = data_from_txt(datafile, keywords, language, case=True)
    # save_path = "./corpus/{}/{}/{}".format(language, period, 'data_' + tile)
    # utils.save_to_disk(save_path, sen_data)


def filter_de():
    # example read sentences from wiki data
    keywords = ['Maus', 'Wolke', 'Stream']
    language = 'de'
    # period = 'wiki'
    # tile = 'p1p297012'
    # data_dir = '../wikiextractor-master/dep1p297012-json'
    # sen_data = data_from_wiki_mp(data_dir, keywords, language, case=False, p_number=12)
    # save_path = "./corpus/{}/{}/{}".format(language, period, 'data_' + tile)
    # utils.save_to_disk(save_path, sen_data)

    # example read sentences from cocha data
    period = 'semeval'
    tile = 'corpus1'
    corpus_path = '../wikiextractor-master/data/semeval2020_ulscd_ger/corpus1/lemma/dta.txt'
    sen_data = data_from_txt(corpus_path, keywords, language, case=False)
    save_path = "./corpus/{}/{}/{}".format(language, period, 'data_' + tile)
    utils.save_to_disk(save_path, sen_data)


def filter_sv():
    # example read sentences from wiki data
    keywords = ['mus',]
    # :param language: ISO 639-1 language code
    language = 'sv'
    period = 'wiki'
    tile = 'svp153416p666977'
    data_dir = '../wikiextractor-master/svp153416p666977-json'
    sen_data = data_from_wiki_mp(data_dir, keywords, language, case=False, p_number=12)
    save_path = "./corpus/{}/{}/{}".format(language, period, 'data_' + tile)
    utils.save_to_disk(save_path, sen_data)

    # example read sentences from cocha data
    # period = 'semeval'
    # tile = 'corpus1'
    # corpus_path = '../wikiextractor-master/data/swi/kubhist2a.txt'
    # sen_data = data_from_txt(corpus_path, keywords, language, case=False)
    # save_path = "./corpus/{}/{}/{}".format(language, period, 'data_' + tile)
    # utils.save_to_disk(save_path, sen_data)


def filter_en_semeval():
    targets = 'corpus/semeval2020_ulscd_eng/targets.txt'
    with open(targets, "r", encoding="utf-8") as targets_data:
        keywords = targets_data.read().split('\n')
    for kw in keywords:
        if len(kw) == 0:
            keywords.remove(kw)
    print(keywords)

    language = 'en'
    period = 'semevalSC'
    tile = 'ccoha1'
    datafile = 'corpus/semeval2020_ulscd_eng/corpus1/lemma/ccoha1.txt'
    sen_data = data_from_txt(datafile, keywords, language, case=True)
    save_path = "./corpus/{}/{}/{}".format(language, period, 'data_' + tile)
    utils.save_to_disk(save_path, sen_data)

    period = 'semevalSC'
    tile = 'ccoha2'
    datafile = 'corpus/semeval2020_ulscd_eng/corpus2/lemma/ccoha2.txt'
    sen_data = data_from_txt(datafile, keywords, language, case=True)
    save_path = "./corpus/{}/{}/{}".format(language, period, 'data_' + tile)
    utils.save_to_disk(save_path, sen_data)


def filter_de_semeval():
    targets = 'corpus/de/semeval_task/semeval2020_ulscd_ger/targets.txt'
    with open(targets, "r", encoding="utf-8") as targets_data:
        keywords = targets_data.read().split('\n')
    for kw in keywords:
        if len(kw) == 0:
            keywords.remove(kw)
    print(keywords)

    language = 'de'
    task_type = 'semeval_task'
    save_path = "./corpus/{}/{}/{}".format(language, task_type, 'keywords.pkl')
    utils.save_to_disk(save_path, keywords)

    datafile = 'corpus/de/semeval_task/semeval2020_ulscd_ger/corpus1/lemma/dta.txt/dta.txt'
    sen_data = data_from_txt(datafile, keywords, language, case=False)
    save_path = "./corpus/{}/{}/{}".format(language, task_type, 'data1.pkl')
    utils.save_to_disk(save_path, sen_data)

    datafile = 'corpus/de/semeval_task/semeval2020_ulscd_ger/corpus2/lemma/bznd.txt/bznd.txt'
    sen_data = data_from_txt(datafile, keywords, language, case=False)
    save_path = "./corpus/{}/{}/{}".format(language, task_type, 'data2.pkl')
    utils.save_to_disk(save_path, sen_data)


# remove semeval en data keywords appendix '_nn'
def process_semeval_data():
    targets = 'corpus/semeval2020_ulscd_eng/targets.txt'
    with open(targets, "r", encoding="utf-8") as targets_data:
        keywords = targets_data.read().split('\n')
    for kw in keywords:
        if len(kw) == 0:
            keywords.remove(kw)
    print(keywords)
    data1 = utils.load_from_disk('./corpus/en/semevalSC/data_ccoha1')
    data2 = utils.load_from_disk('./corpus/en/semevalSC/data_ccoha2')
    new_data1 = {}
    new_data2 = {}
    new_keywords = []
    for keyword in keywords:
        new_sendata1 = []
        new_sendata2 = []
        sendata1 = data1[keyword]
        sendata2 = data2[keyword]
        words = keyword.split('_')
        new_keyword = words[0]
        print(new_keyword)
        for sen in sendata1:
            new_sen = sen.replace(keyword, new_keyword)
            new_sendata1.append(new_sen)
        for sen in sendata2:
            new_sen = sen.replace(keyword, new_keyword)
            new_sendata2.append(new_sen)

        new_data1[new_keyword] = new_sendata1
        new_data2[new_keyword] = new_sendata2
        new_keywords.append(new_keyword)

    print(new_keywords)
    utils.save_to_disk('./corpus/en/semevalSC_r/keywords', new_keywords)
    utils.save_to_disk('./corpus/en/semevalSC_r/data_ccoha1', new_data1)
    utils.save_to_disk('./corpus/en/semevalSC_r/data_ccoha2', new_data2)


if __name__ == '__main__':
    # filter_en_semeval()
    # process_semeval_data()
    # filter_de()
    # filter_sv()

    filter_de_semeval()





