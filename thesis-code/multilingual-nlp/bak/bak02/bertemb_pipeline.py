import torch
import utils
import numpy as np
from datasets import load_dataset
from transformers import BertTokenizer, AutoModel, AutoTokenizer
from transformers import BertModel

#加载字典和分词工具

BERT_MODEL = 'KB/bert-base-swedish-cased'
key_word = 'mus'
# BERT_MODEL = 'bert-base-german-cased'
# key_word = 'Maus'

token = BertTokenizer.from_pretrained(BERT_MODEL)
# token = BertTokenizer.from_pretrained('bert-base-uncased')

print(token)

vocab = token.get_vocab()
print("vocab ", key_word, " id = ", vocab[key_word])


class Dataset(torch.utils.data.Dataset):
    def __init__(self, sen_data):
        print("origin sen len ", len(sen_data))
        enable_end = [' ', '.', '"', ")", "'", ',', ":", "!", "?", '*', ';', ']', '}']
        sen_data_filter = []
        for sen in sen_data:
            idx = sen.find(key_word)
            i = idx + len(key_word)
            if i < len(sen) and sen[i] in enable_end:
                sen_data_filter.append(sen)

        sen_data = sen_data_filter
        print("filter sen len ", len(sen_data))

        self.dataset = sen_data

    def __len__(self):
        return len(self.dataset)

    def __getitem__(self, i):
        text = self.dataset[i]
        return text


def collate_fn(data_raw):
    #编码
    data = token.batch_encode_plus(batch_text_or_text_pairs=data_raw,
                                   truncation=True,
                                   padding='max_length',
                                   max_length=80,
                                   return_tensors='pt',
                                   return_length=True)

    #input_ids:编码之后的数字
    #attention_mask:是补零的位置是0,其他位置是1
    input_ids = data['input_ids']
    attention_mask = data['attention_mask']
    token_type_ids = data['token_type_ids']

    return input_ids, attention_mask, token_type_ids


# 加载预训练模型

pretrained = BertModel.from_pretrained(BERT_MODEL)
# pretrained = BertModel.from_pretrained('bert-base-uncased')

# 不训练,不需要计算梯度
for param in pretrained.parameters():
    param.requires_grad_(False)


#定义下游任务模型
class Model(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.decoder = torch.nn.Linear(768, token.vocab_size, bias=False)
        self.bias = torch.nn.Parameter(torch.zeros(token.vocab_size))
        self.decoder.bias = self.bias

    def forward(self, input_ids, attention_mask, token_type_ids):
        with torch.no_grad():
            out = pretrained(input_ids=input_ids,
                             attention_mask=attention_mask,
                             token_type_ids=token_type_ids)

        # out = self.decoder(out.last_hidden_state[:, 15])
        out = out.last_hidden_state.numpy()
        return out


def get_corpus_from_wiki(key_word, tile):
    # tile = 'enp1p41242'
    sen_data = utils.load_from_disk("./filter/" + key_word + '_st_' + tile)

    return sen_data


def get_corpus_from_text(key_word, corpus_path):
    sen_data = utils.read_coupus_from_txt(key_word, corpus_path)
    return sen_data


def refresh_word_embeddings(input_ids, outs, stat_emb, target_embeds, key_word):
    # stat_emb = {}
    for i, ids in enumerate(input_ids):
        last_word = ''
        last_word_v = []
        for j, id in enumerate(ids):
            mor = token.decode([id])
            if mor.startswith("##"):
                last_word = last_word+mor[2:]
                last_word_v.append(outs[i][j])
                # print("---", mor)
                # print("---", last_word)
            else:
                if j > 0:
                    ems = np.array(last_word_v)
                    avg = np.mean(ems, axis=0)
                    if last_word in stat_emb:
                        stat_emb[last_word].append(avg)
                    else:
                        stat_emb[last_word] = [avg]
                    if key_word == last_word:
                        target_embeds.append(avg)

                last_word = mor
                last_word_v.append(outs[i][j])

    return stat_emb


def avg_stat_emb(stat_emb):
    print("origin stat len ", len(stat_emb))
    emb_threshold = 4
    del_keys = []
    for key in stat_emb:
        if len(stat_emb[key]) < emb_threshold:
            del_keys.append(key)
    for key in del_keys:
        del stat_emb[key]
    for key in stat_emb:
        ems = np.array(stat_emb[key])
        avg = np.mean(ems, axis=0)
        stat_emb[key] = avg
    print("after del stat len ", len(stat_emb))
    return stat_emb


def gen_embed(key_word, sen_data, savename,):
    dataset = Dataset(sen_data)

    # 数据加载器
    loader = torch.utils.data.DataLoader(dataset=dataset,
                                         batch_size=16,
                                         collate_fn=collate_fn,
                                         shuffle=False,
                                         drop_last=True)

    model = Model()

    target_embeds = []
    stat_emb = {}
    for i, (input_ids, attention_mask, token_type_ids) in enumerate(loader):
        if i % 50 == 0:
            print("loop i ", i)
        out = model(input_ids=input_ids,
                    attention_mask=attention_mask,
                    token_type_ids=token_type_ids)

        refresh_word_embeddings(input_ids, out, stat_emb, target_embeds, key_word)

        # for j, ids in enumerate(input_ids):
        #     # words = token.decode(ids)
        #     for k, id in enumerate(ids):
        #         if id == vocab[key_word]:
        #             # print(ids[k])
        #             # print(token.decode(ids[k]))
        #             target_embeds.append(out[j][k])

    stat_emb = avg_stat_emb(stat_emb)
    print("target_embeds len ", len(target_embeds))
    utils.save_to_disk(savename, target_embeds)
    utils.save_to_disk("stat_emb"+savename, stat_emb)


def gen_embed_for_wiki(key_word, tile, language):
    sen_data = get_corpus_from_wiki(key_word, tile)
    # tile = 'enp1p41242'
    gen_embed(key_word=key_word, sen_data=sen_data, savename=key_word + "_"+language+"_wikiembeds.pkl")


def gen_embed_for_ccoha(key_word, corpus_path, language, p=''):
    sen_data = get_corpus_from_text(key_word, corpus_path)
    gen_embed(key_word=key_word, sen_data=sen_data, savename=key_word + "_"+language+"_ccohaembeds.pkl"+p)


print("xiaohema finished")

if __name__ == "__main__":
    # key_word = 'Maus'
    # language = 'de'
    # corpus_path = './data/semeval2020_ulscd_ger/corpus1/lemma/dta.txt'
    # gen_embed_for_ccoha(key_word, corpus_path, language, p='01')

    # key_word = 'Maus'
    # language = 'de'
    # tile = 'dep1p297012'
    # gen_embed_for_wiki(key_word, tile, language)

    # language = 'swi'
    # corpus_path = './data/swi/kubhist2a.txt'
    # gen_embed_for_ccoha(key_word, corpus_path, language, p='01')

    language = 'swi'
    tile = 'svp153416p666977'
    gen_embed_for_wiki(key_word, tile, language)





