import torch
import utils
from datasets import load_dataset
from transformers import BertTokenizer
from transformers import BertModel
from transformers import AdamW

#加载字典和分词工具

token = BertTokenizer.from_pretrained('bert-base-german-cased')
# token = BertTokenizer.from_pretrained('bert-base-uncased')

print(token)
key_word = 'Maus'

vocab = token.get_vocab()
print("vocab mouse id = ", vocab['Maus'])


class Dataset(torch.utils.data.Dataset):
    def __init__(self, sen_data):
        print("origin sen len ", len(sen_data))
        sen_data_filter = []
        for sen in sen_data:
            idx = sen.find(key_word)
            if idx + len(key_word) < len(sen):
                i = idx + len(key_word)
                if sen[i] == ' ' or sen[i] == '.':
                    sen_data_filter.append(sen)
        sen_data = sen_data_filter
        print("filter sen len ", len(sen_data))
        self.dataset = sen_data

    def __len__(self):
        return len(self.dataset)

    def __getitem__(self, i):
        text = self.dataset[i]
        return text


mask_id = 1
max_length = 50


def collate_fn(data):
    #编码
    data = token.batch_encode_plus(batch_text_or_text_pairs=data,
                                   truncation=True,
                                   padding='max_length',
                                   max_length=80,
                                   return_tensors='pt',
                                   return_length=True)

    #input_ids:编码之后的数字
    #attention_mask:是补零的位置是0,其他位置是1
    input_ids = data['input_ids']
    attention_mask = data['attention_mask']
    token_type_ids = data['token_type_ids']

    #把第mask_id个词固定替换为mask
    labels = input_ids[:, mask_id].reshape(-1).clone()
    input_ids[:, mask_id] = token.get_vocab()[token.mask_token]

    #print(data['length'], data['length'].max())

    return input_ids, attention_mask, token_type_ids, labels


# load BertModel
pretrained = BertModel.from_pretrained('bert-base-german-cased')


class Model(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.decoder = torch.nn.Linear(768, token.vocab_size, bias=False)
        self.bias = torch.nn.Parameter(torch.zeros(token.vocab_size))
        self.decoder.bias = self.bias

    def forward(self, input_ids, attention_mask, token_type_ids):
        out = pretrained(input_ids=input_ids,
                         attention_mask=attention_mask,
                         token_type_ids=token_type_ids)

        out = self.decoder(out.last_hidden_state[:, 15])

        return out


def get_corpus_from_wiki(key_word, tile):
    # tile = 'enp1p41242'
    sen_data = utils.load_from_disk("./filter/" + key_word + '_st_' + tile)

    return sen_data


def get_corpus_from_text(key_word, corpus_path):
    sen_data = utils.read_coupus_from_txt(key_word, corpus_path)
    return sen_data


def train(key_word, sen_data, savename,):
    dataset = Dataset(sen_data)

    # 数据加载器
    loader = torch.utils.data.DataLoader(dataset=dataset,
                                         batch_size=16,
                                         collate_fn=collate_fn,
                                         shuffle=False,
                                         drop_last=True)

    model = Model()
    model.train()

    optimizer = AdamW(model.parameters(), lr=5e-4)
    criterion = torch.nn.CrossEntropyLoss()

    for epoch in range(5):
        for i, (input_ids, attention_mask, token_type_ids,
                labels) in enumerate(loader):
            out = model(input_ids=input_ids,
                        attention_mask=attention_mask,
                        token_type_ids=token_type_ids)

            loss = criterion(out, labels)
            loss.backward()
            optimizer.step()
            optimizer.zero_grad()

            if i % 50 == 0:
                out = out.argmax(dim=1)
                accuracy = (out == labels).sum().item() / len(labels)

                print(epoch, i, loss.item(), accuracy)


def gen_embed_for_wiki(key_word, tile, language):
    sen_data = get_corpus_from_wiki(key_word, tile)
    # tile = 'enp1p41242'
    gen_embed(key_word=key_word, sen_data=sen_data, savename=key_word + "_"+language+"_wikiembeds.pkl")


def gen_embed_for_ccoha(key_word, corpus_path, language, p=''):
    sen_data = get_corpus_from_text(key_word, corpus_path)
    gen_embed(key_word=key_word, sen_data=sen_data, savename=key_word + "_"+language+"_ccohaembeds.pkl"+p)


print("xiaohema finished")

if __name__ == "__main__":
    # key_word = 'Maus'
    # language = 'de'
    # corpus_path = './data/semeval2020_ulscd_ger/corpus1/lemma/dta.txt'
    # gen_embed_for_ccoha(key_word, corpus_path, language, p='01')

    key_word = 'Maus'
    language = 'de'
    tile = 'dep1p297012'
    gen_embed_for_wiki(key_word, tile, language)





