import torch
import utils
import re
from datasets import load_dataset
from transformers import BertTokenizer, AutoModel, AutoTokenizer
from transformers import BertModel

#加载字典和分词工具

BERT_MODEL = 'bert-base-german-cased'

token = BertTokenizer.from_pretrained(BERT_MODEL)
print(token)

vocab = token.get_vocab()
key_word = 'Maus'
print("vocab ", key_word, " id = ", vocab[key_word])


def get_corpus_from_wiki(key_word, tile):
    # tile = 'enp1p41242'
    sen_data = utils.load_from_disk("./filter/" + key_word + '_st_' + tile)

    return sen_data


def get_corpus_from_text(key_word, corpus_path):
    sen_data = utils.read_coupus_from_txt(key_word, corpus_path)
    return sen_data


def count_words(sen_data):
    marks = [' ', "'", ',', '.', '"', "(", ")", ":", "!", "?", '*', ';', '[', ']', '{', '}', '@', '#', '^', '%',
             '-', '–', '-', '+', '/', '\\', '<', '>', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' '$', '&']
    word_dict = {}
    for sentence in sen_data:
        words = sentence.split()
        key_word_in = False
        for w in words:
            has_marks = False
            for m in marks:
                if m in w:
                    has_marks = True
                    break
            if has_marks:
                continue
            if w in word_dict:
                word_dict[w] = word_dict[w]+1
                if w == key_word:
                    key_word_in = True
            else:
                word_dict[w] = 0
                if w == key_word:
                    key_word_in = True
        # if not key_word_in:
        #     print(sentence)
    return word_dict


def stat_word_dict(key_word, word_dict):
    print("total count word num in word_dict ", len(word_dict))
    print("key word counts ", word_dict[key_word])
    in_vocab = 0
    out_vocabs = {}
    for key in word_dict:
        if key in vocab:
            in_vocab += 1
        else:
            # print(key)
            out_vocabs[key] = word_dict[key]
            continue
    print("in_vocab ", in_vocab)
    print("out_vocab ", len(word_dict)-in_vocab)


# 按照 '.', ';' 再次切分成短句
def split_sen(sen_data):
    splits = ['.', ';']
    sen_set = sen_data
    s_sen_set = []
    for s in splits:
        for lsen in sen_set:
            ssens = lsen.split(s)
            for sen in ssens:
                s_sen_set.append(sen)
        sen_set = s_sen_set
        s_sen_set = []

    sen_data = sen_set
    return sen_data


def filter_sen_data(sen_data, key_word):
    print("origin sen data len ", len(sen_data))
    sen_data = split_sen(sen_data)
    print("split sen data len ", len(sen_data))
    sen_data_filter = []
    max_len = 0
    total_len = 0
    enable_end = [' ', '.', '"', ")", "'", ',', ":", "!", "?", '*', ';', ']', '}']
    for sen in sen_data:
        idx = sen.find(key_word)
        if idx + len(key_word) < len(sen):
            i = idx + len(key_word)
            if i<len(sen) and sen[i] in enable_end:
                # max_len = len(sen.split()) if max_len < len(sen.split()) else max_len
                # total_len += len(sen.split())
                sen_data_filter.append(sen)

    sen_data = sen_data_filter
    print("filter sen data len ", len(sen_data))
    # print("max sen len ", max_len)
    # print("avg sen len ", total_len/len(sen_data))
    return sen_data


def gen_words_for_wiki(key_word, tile, language):
    sen_data = get_corpus_from_wiki(key_word, tile)
    sen_data = filter_sen_data(sen_data, key_word)
    word_dict = count_words(sen_data)
    stat_word_dict(key_word, word_dict)


def gen_words_for_ccoha(key_word, corpus_path, language, p=''):
    sen_data = get_corpus_from_text(key_word, corpus_path)
    sen_data = filter_sen_data(sen_data, key_word)
    word_dict = count_words(sen_data)
    stat_word_dict(key_word, word_dict)


if __name__ == "__main__":
    # key_word = 'Maus'
    # language = 'de'
    # corpus_path = './data/semeval2020_ulscd_ger/corpus1/lemma/dta.txt'
    # gen_words_for_ccoha(key_word, corpus_path, language, p='01')

    key_word = 'Maus'
    language = 'de'
    tile = 'dep1p297012'
    gen_words_for_wiki(key_word, tile, language)





