# 导入相关库
import matplotlib.pyplot as plt
from sklearn.datasets import load_iris
from sklearn.decomposition import PCA
import utils
from sklearn.cluster import KMeans
from scipy.spatial.distance import cdist
import numpy as np


def test_plt_PCA():
    key_word = 'mouse'
    target_embeds = utils.load_from_disk("mouse_target_embeds.pkl")
    X = target_embeds

    # normalize
    # normalize 后PCA形状没变，尺度变了
    # normalize后，cosine distance的Kmean和Euclidean distance的kmean等价。
    X = np.array(X)
    nm = np.sqrt((X ** 2).sum(axis=1))[:, None]
    X = X / nm

    # 查看原始数据
    print(X[0:3])

    # 调用PCA，n_components=2
    pca = PCA(n_components=2)
    pca = pca.fit(X)
    X_dr = pca.transform(X)

    # 查看降维后的数据
    print(X_dr[0:3])

    # 鸢尾花三种类型可视化
    colors = ['red', 'black', 'orange']

    plt.figure()
    # for i in [0, 1, 2]:
    #     plt.scatter(X_dr[y == i, 0] ,X_dr[y == i, 1]
    #                 ,alpha=.7
    #                 ,c=colors[i] ,label=iris.target_names[i])

    plt.scatter(X_dr[:, 0], X_dr[:, 1], alpha=.7, c=colors[0], s=4)

    plt.legend()
    plt.title('PCA of IRIS dataset')
    plt.show()

    distortions = []
    K = range(1, 10)
    # X = np.array(X)
    # X = X_dr
    for k in K:
        # KMeans 是基于euclidean dis计算，而不是cos dis计算
        kmeanModel = KMeans(n_clusters=k).fit(X)
        distortions.append(sum(np.min(cdist(X, kmeanModel.cluster_centers_, 'euclidean'), axis=1)) / X.shape[0])

    plt.plot(K, distortions, 'bx-')
    plt.xlabel('k')
    plt.ylabel('Distortion')
    plt.title('The Elbow Method showing the optimal k')
    plt.show()


def test_kmeans():
    x1 = np.array([3, 1, 1, 2, 1, 6, 6, 6, 5, 6, 7, 8, 9, 8, 9, 9, 8])
    x2 = np.array([5, 4, 5, 6, 5, 8, 6, 7, 6, 7, 1, 2, 1, 2, 3, 2, 3])

    plt.plot()
    plt.xlim([0, 10])
    plt.ylim([0, 10])
    plt.title('Dataset')
    plt.scatter(x1, x2)
    plt.show()

    plt.plot()
    X = np.array(list(zip(x1, x2))).reshape(len(x1), 2)
    colors = ['b', 'g', 'r']
    markers = ['o', 'v', 's']

    distortions = []
    K = range(1, 10)
    for k in K:
        kmeanModel = KMeans(n_clusters=k).fit(X)
        distortions.append(sum(np.min(cdist(X, kmeanModel.cluster_centers_, 'euclidean'), axis=1)) / X.shape[0])

    plt.plot(K, distortions, 'bx-')
    plt.xlabel('k')
    plt.ylabel('Distortion')
    plt.title('The Elbow Method showing the optimal k')
    plt.show()


def test_kmeans_mostsim():
    index_voc, voc_index, embeddings = utils.load_from_disk("en_static_embed.pkl")
    target_embeds = utils.load_from_disk("mouse_target_embeds_c01.pkl")
    X = target_embeds

    # normalize
    # normalize 后PCA形状没变，尺度变了
    # normalize后，cosine distance的Kmean和Euclidean distance的kmean等价。
    X = np.array(X)
    nm = np.sqrt((X ** 2).sum(axis=1))[:, None]
    X = X / nm

    kmeanModel = KMeans(n_clusters=3).fit(X)
    print(kmeanModel.cluster_centers_.shape)
    for embed in kmeanModel.cluster_centers_:
        print('+++++++++++++++++++++++')
        utils.most_sim_words(embed, 8, voc_index, embeddings)

# test_kmeans()
test_plt_PCA()

# test_kmeans_mostsim()


