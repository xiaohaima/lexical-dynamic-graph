import utils
import numpy as np
import semantic_tree as st
from scipy.optimize import linear_sum_assignment
from itertools import combinations, permutations
import matplotlib.pyplot as plt
import matplotlib.cm as cm


def clear_sim_words(keyword, embed, simsize, static_embeds):
    index_voc, voc_index, embeddings = static_embeds
    special_tokens = [keyword, "[PAD]", "[CLS]", "[SEP]", "[UNK]", "[MASK]"]
    stop_words = utils.stop_words(language="en")
    sizegap = 20
    words = utils.most_sim_words(embed, simsize + sizegap, voc_index, embeddings, willprint=False)
    poly_sims = []
    for i, w in enumerate(words):
        # print('id: {} w: {} sim: {}'.format(i, w[0], w[1]))
        if w[0] in special_tokens:
            continue
        if w[0] in stop_words:
            continue
        if len(poly_sims) < simsize:
            poly_sims.append(w[0])
    return poly_sims


def get_most_sim_words(keyword, poly_embeds, simsize, static_embeds):
    tree_nodes = []
    for embed in poly_embeds:
        poly_sims = clear_sim_words(keyword, embed, simsize, static_embeds)
        tree_nodes.append(poly_sims)
    return tree_nodes


def polysemy_reduce(keyword, poly_embeds, static_embeds, check_simsize=10, merge_t=0.65):
    if len(poly_embeds) == 1:
        return poly_embeds
    merge = True
    while merge and len(poly_embeds) > 1:
        sim_words = get_most_sim_words(keyword, poly_embeds, check_simsize, static_embeds)
        combs = list(combinations(range(len(poly_embeds)), 2))
        sims = []
        for comb in combs:
            p1 = poly_embeds[comb[0]]
            p2 = poly_embeds[comb[1]]
            sim = polysemy_similarity(sim_words[comb[0]], sim_words[comb[1]], static_embeds)
            print("sim: {} sim_ws1: {} sim_ws2: {}".format(sim, sim_words[comb[0]], sim_words[comb[1]]))
            sims.append(sim)
        simmax = np.max(sims)
        if simmax > merge_t:
            id = np.argmax(sims)
            comb = combs[id]
            merge_embedding = (poly_embeds[comb[0]] + poly_embeds[comb[0]]) / 2
            new_poly_embeds = [merge_embedding]
            for i, p in enumerate(poly_embeds):
                if i != comb[0] and i != comb[1]:
                    new_poly_embeds.append(p)
            poly_embeds = new_poly_embeds
        else:
            merge = False

    return poly_embeds


def polysemy_cloud_cover(keyword, poly_embeds, cloud, static_embedding, check_simsize):
    print(len(poly_embeds))
    psim_words_set = get_most_sim_words(keyword, poly_embeds, check_simsize, static_embedding)
    print("psim_words_set : ", psim_words_set)
    cloud_sims = []
    low_sim_points = []
    for emb in cloud:
        csim_words = clear_sim_words(keyword, emb, check_simsize, static_embedding)
        sims = []
        for psim_words in psim_words_set:
           sim = polysemy_similarity(csim_words, psim_words, static_embedding)
           sims.append(sim)
        cloud_sims.append(np.max(sims))
        if np.max(sims) < 0.65:
            low_sim_points.append((np.max(sims), csim_words, emb))
    print("cloud_sims max ", np.max(cloud_sims))
    print("cloud_sims min ", np.min(cloud_sims))
    if len(low_sim_points) < 5:
        return []
    # print(low_sim_points[:][1])
    stat_low_frequence_sms(low_sim_points, static_embedding)
    lfq_poly_embeds = extract_low_frequence_sm(low_sim_points, static_embedding, keyword, check_simsize)
    return lfq_poly_embeds


def stat_low_frequence_sms(low_sim_points, static_embedding):
    print("low_frequence point nums", len(low_sim_points))
    """
    n_points = len(low_sim_points)
    sim_mat = np.zeros((n_points, n_points))
    for i in range(n_points):
        for j in range(n_points):
            if i >= j:
                continue
            i_words = low_sim_points[i][1]
            j_words = low_sim_points[j][1]
            sim = polysemy_similarity(i_words, j_words, static_embedding)
            sim_mat[i,j] = sim
    print("low_frequence sim max", np.max(sim_mat))
    sim_mat_r = np.ravel(sim_mat)
    # sims = [sim_mat_r[s] if sim_mat_r[s] > 0.001 else np.nan for s in range(sim_mat_r.shape[0])]
    sims = []
    for s in range(sim_mat_r.shape[0]):
        if sim_mat_r[s] > 0.001:
            sims.append(sim_mat_r[s])
    print("low_frequence sim min", np.min(sims))
    print("low_frequence sim arg max", np.argmax(sim_mat))
    print("low_frequence sim max matrix index", np.unravel_index(np.argmax(sim_mat), sim_mat.shape))
    index = np.unravel_index(np.argmax(sim_mat), sim_mat.shape)
    print("sim max words set 0: ", low_sim_points[index[0]][1])
    print("sim max words set 1: ", low_sim_points[index[1]][1])
    """


def merge_sm(low_fq_sms, i, j, keyword, check_simsize, static_embedding):
    n_points = len(low_fq_sms)
    new_low_fq_sms = []
    com_embed = (low_fq_sms[i][1] + low_fq_sms[j][1]) / 2
    com_simwords = clear_sim_words(keyword, com_embed, check_simsize, static_embedding)
    com_num = low_fq_sms[i][2] + low_fq_sms[j][2]
    new_low_fq_sms.append((com_simwords, com_embed, com_num))
    for k in range(n_points):
        if k != i and k != j:
            new_low_fq_sms.append(low_fq_sms[k])
    return new_low_fq_sms


def merge_sms(low_fq_sms, static_embedding, keyword, check_simsize, merge_t=0.65):
    # for sm_point in low_fq_sms:
    n_points = len(low_fq_sms)
    sim_mat = np.zeros((n_points, n_points))
    for i in range(n_points):
        for j in range(n_points):
            if i >= j:
                continue
            i_words = low_fq_sms[i][0]
            j_words = low_fq_sms[j][0]
            sim = polysemy_similarity(i_words, j_words, static_embedding)
            # sim_mat[i, j] = sim
            if sim > merge_t:
                print("merge ", sim)
                low_fq_sms = merge_sm(low_fq_sms, i, j, keyword, check_simsize, static_embedding)
                return low_fq_sms, True
    # if np.max(sim_mat) > merge_t:
    #     print("max(sim_mat)  > merge_t", np.max(sim_mat))
    #     index = np.unravel_index(np.argmax(sim_mat), sim_mat.shape)
    #     new_low_fq_sms = []
    #     for i in range(n_points):
    #         if i != index[0] and i != index[1]:
    #             new_low_fq_sms.append(low_fq_sms[i])
    #
    #     com_embed = (low_fq_sms[index[0]][1] + low_fq_sms[index[1]][1])/2
    #     com_simwords = clear_sim_words(keyword, com_embed, check_simsize, static_embedding)
    #     com_num = low_fq_sms[index[0]][2] + low_fq_sms[index[1]][2]
    #     new_low_fq_sms.append((com_simwords, com_embed, com_num))
    #     return new_low_fq_sms, True

    return low_fq_sms, False


def extract_low_frequence_sm(low_sim_points, static_embedding, keyword, check_simsize, lfq_t=5):
    low_fq_sms = []
    for sc_point in low_sim_points:
        low_fq_sms.append((sc_point[1],sc_point[2],1))

    merge = True
    while merge:
        low_fq_sms, merge = merge_sms(low_fq_sms, static_embedding, keyword, check_simsize, merge_t=0.70)

    print("low_fq_sms merged ", [(point[0], point[2]) for point in low_fq_sms])
    lfq_poly_embeds = []
    for point in low_fq_sms:
        if point[2] > lfq_t:
            lfq_poly_embeds.append(point[1])
    return lfq_poly_embeds


def get_polysemy_embedding(keyword, cloud, static_embedding, check_simsize=12):
    poly_embeds = st.polysemy_branch(cloud)
    poly_embeds = polysemy_reduce(keyword, poly_embeds, static_embedding, check_simsize=check_simsize)
    # test polysemy_cloud_cover
    poly_embeds = [emb for emb in poly_embeds]
    # poly_embeds =
    lfq_poly_embeds = polysemy_cloud_cover(keyword, poly_embeds, cloud, static_embedding, check_simsize)
    poly_embeds.extend(lfq_poly_embeds)
    poly_embeds = polysemy_reduce(keyword, poly_embeds, static_embedding, check_simsize=check_simsize)
    return poly_embeds


def eva_polysim_linear_assignment(polyset1, polyset2, static_embeds, rte_match=False):
    index_voc, voc_index, embeddings = static_embeds
    weights = np.zeros((len(polyset1), len(polyset2)))
    for i, w_row in enumerate(polyset1):
        for j, w_col in enumerate(polyset2):
            weight = utils.word_cosine_similarity(w_row, w_col, voc_index, embeddings)
            weights[i, j] = weight
    row_ind, col_ind = linear_sum_assignment(weights, maximize=True)
    sum = weights[row_ind, col_ind].sum()
    mean = weights[row_ind, col_ind].mean()
    # print("keyword weights : ", weights)
    # print("keyword weights sum {} mean {}", sum, mean)
    if rte_match:
        return mean, weights, row_ind, col_ind
    else:
        return mean


def plt_greedy_assignment_weight_matrix(evt1, evt2, row_ind, col_ind, static_embeds):
    index_voc, voc_index, embeddings = static_embeds
    polyset1 = []
    polyset2 = []
    for m in range(len(row_ind)):
        polyset1.append(evt1[row_ind[m]])
    for n in range(len(col_ind)):
        polyset2.append(evt2[col_ind[n]])
    weights = np.zeros((len(polyset1), len(polyset2)))
    for i, w_row in enumerate(polyset1):
        for j, w_col in enumerate(polyset2):
            weight = utils.word_cosine_similarity(w_row, w_col, voc_index, embeddings)
            weights[i, j] = weight

    plt_weights(weights, "Subtree nodes ")


def eva_polysim_greedy_assignment(polyset1, polyset2, static_embeds, rte_match=False):
    index_voc, voc_index, embeddings = static_embeds
    weights = np.zeros((len(polyset1), len(polyset2)))
    for i, w_row in enumerate(polyset1):
        for j, w_col in enumerate(polyset2):
            weight = utils.word_cosine_similarity(w_row, w_col, voc_index, embeddings)
            weights[i, j] = weight
    row_ind = []
    col_ind = []
    weights_cp = weights.copy()
    for m in range(min(len(polyset1), len(polyset2))):
        arg_max = np.argmax(weights)
        arg_max = np.unravel_index(arg_max, weights.shape)
        # print(arg_max)
        row_ind.append(arg_max[0])
        col_ind.append(arg_max[1])
        weights[arg_max[0], :] = 0
        weights[:, arg_max[1]] = 0

    mean = weights_cp[row_ind, col_ind].mean()
    if rte_match:
        return mean, weights_cp, row_ind, col_ind
    else:
        return mean


def polysemy_similarity(polyset1, polyset2, static_embeds, polyembed1=None, polyembed2=None, alpha=0.1):
    polytree_sim = eva_polysim_linear_assignment(polyset1, polyset2, static_embeds)
    if polyembed1 is not None and polyembed2 is not None:
        polysim = utils.cosine_similarity(polyembed1, polyembed2)
        polytree_sim = alpha*polysim + (1-alpha)*polytree_sim
    return polytree_sim


def eva_polysim_jaccard_index(polyset1, polyset2):
    polyset1 = set(polyset1)
    polyset2 = set(polyset2)
    union = polyset1.union(polyset2)
    intersection = polyset1.intersection(polyset2)
    jaccard_index = len(intersection)/len(union)
    return jaccard_index


def semantic_change_from_similarity_matrix(sim_matrix, sc_t=0.6, synonyms_t=0.65):
    a = sim_matrix.shape[0]
    b = sim_matrix.shape[1]
    for i in range(a):
        simmax = np.max(sim_matrix[i, :])
        if simmax < sc_t:
            return 1
    for i in range(b):
        simmax = np.max(sim_matrix[:, i])
        if simmax < sc_t:
            return 1
    return 0


def evaluate_tree_similarity(evolves):
    semantic_change = {}
    for keyword in evolves:
        print("Exec evaluate_tree_similarity keyword: ", keyword)
        static_embeds = evolves[keyword]['static_emb']

        evt_sets = evolves[keyword]['psim_words']
        evts1 = evt_sets[0]
        evts2 = evt_sets[1]
        print("evts1: ", evts1)
        print("evts2: ", evts2)
        sim_matrix = np.zeros((len(evts1), len(evts2)))
        jaccard_indexs = np.zeros((len(evts1), len(evts2)))
        for m, evt1 in enumerate(evts1):
            for n, evt2 in enumerate(evts2):
                mean = eva_polysim_linear_assignment(evt1, evt2, static_embeds)
                jaccard_index = eva_polysim_jaccard_index(evt1, evt2)
                sim_matrix[m,n] = mean
                jaccard_indexs[m,n] = jaccard_index
                # print("keyword W ", weights)
        print("sim_matrix ", sim_matrix)
        print("jaccard_indexs ", jaccard_indexs)
        plt_weights(sim_matrix, title="Semantic change detection for word '{}'".format(keyword))
        sc = semantic_change_from_similarity_matrix(sim_matrix, sc_t=0.66)
        # sc = semantic_change_from_similarity_matrix(sim_matrix, sc_t=0.62)
        print("Semantic Change result: ", sc)
        semantic_change[keyword] = sc

    utils.save_to_disk('./corpus/en/semevalSC_r/semantic_change_results', semantic_change)


def test_in_similarity_matrix(sc_tree, static_embeds):
    # sim words 内部是从同一个polysemy求得，即便有离群值也不是辨别很清晰。
    # 本身每个词也都是在文本中获得的，也并非真的离群值。
    # 但是在跨语言匹配中（任意两个不同文本统计来的结果），sim特别低的行和列确实可以剔除一下，
    # 在有限表示集合中，并不是每个周围的sim词都有高度相关的翻译。关联度很低的可以去掉在linear assign。
    sim_matrix = np.zeros((len(sc_tree), len(sc_tree)))
    jaccard_indexs = np.zeros((len(sc_tree), len(sc_tree)))
    for m, evt1 in enumerate(sc_tree):
        for n, evt2 in enumerate(sc_tree):
            mean, weights, row_ind, col_ind =\
                eva_polysim_linear_assignment(evt1, evt2, static_embeds, rte_match=True)
            jaccard_index = eva_polysim_jaccard_index(evt1, evt2)
            sim_matrix[m, n] = mean
            jaccard_indexs[m, n] = jaccard_index

            plt_weights(evt1, evt2, weights)


def plt_weights(weights, title=""):
    # print("weights", weights)
    plt.matshow(weights)
    plt.colorbar()
    plt.title(title +" similarity matrix")
    plt.show()


def crosslingual_similarity_matrix(meaning_emb1, meaning_emb2):
    sim_matrix = np.zeros((meaning_emb1.shape[0], meaning_emb2.shape[0]))
    for m in range(meaning_emb1.shape[0]):
        for n in range(meaning_emb2.shape[0]):
            sim_matrix[m, n] = utils.cosine_similarity(meaning_emb1[m,:], meaning_emb2[n,:])

    plt_weights(sim_matrix, title="Semantic embedding")


def plt_matches(match_words, row_ind, col_ind, weights):
    mlen = len(match_words)
    ls = []
    rs = []
    pad = 10
    text_pad = 5
    plt.figure(figsize=(10, 10))
    max_height = (mlen+2) * pad
    # sm = cm.ScalarMappable(norm=weights[row_ind, col_ind])
    import matplotlib
    nm = matplotlib.colors.Normalize(vmin=np.min(weights), vmax=np.max(weights), clip=False)
    sm = cm.ScalarMappable(norm=nm)
    # print(sm)
    # rgba = sm.to_rgba(0.8, alpha=None, bytes=False, norm=True)
    # color = [[rgba[0], rgba[1], rgba[2]]]
    # print(color)

    for i in range(mlen):
        ls.append([4*pad, max_height - (i+1) * pad])
        rs.append([4*pad+pad*mlen/2, max_height - (i + 1) * pad])
        plt.scatter(ls[-1][0], ls[-1][1], alpha=0.9, c='#1E90FF', s=16, zorder=2)
        plt.scatter(rs[-1][0], rs[-1][1], alpha=0.9, c='#1E90FF', s=16, zorder=2)
        plt.text(ls[-1][0]-text_pad, ls[-1][1], match_words[i][0], fontsize=10, color="blue", style="italic", weight="light",
                 verticalalignment='center', horizontalalignment='right', rotation=0, alpha=0.5)
        plt.text(rs[-1][0]+text_pad, rs[-1][1], match_words[i][1], fontsize=10, color="blue", style="italic", weight="light",
                 verticalalignment='center', horizontalalignment='left', rotation=0, alpha=0.5)

    for i in range(mlen):
        # print(mlen)
        for j in range(mlen):
            w = weights[i,j]
            lid = row_ind.index(i)
            rid = col_ind.index(j)
            # print("lid ", lid, "rid ", rid)
            xs = [ls[lid][0], rs[rid][0]]
            ys = [ls[lid][1], rs[rid][1]]
            # alpha = 1 if i == lid and j == rid else 0.1
            alpha = 0.8 if lid == rid else 0.1
            rgba = sm.to_rgba(w, alpha=alpha)
            # print(w)
            # print(rgba)
            # color = [[rgba[0], rgba[1], rgba[2]]]
            plt.plot(xs, ys, c=rgba, zorder=1)
        # if i == 0:
        #     break

    plt.xlim((0, 8*pad+pad*mlen/2))
    plt.ylim((0, (mlen + 2) * pad))

    plt.colorbar(mappable=sm)
    plt.show()
    return


def select_multilingual_match(sc_tree1, sc_tree2, row_inds, col_inds, weights_set, sim_matrix):
    print("sc_tree1", sc_tree1)
    print("sc_tree2", sc_tree2)
    print("sim_matrix", sim_matrix)
    print(row_inds)
    print(col_inds)
    plt_weights(sim_matrix, title="Semantic close node embedding")
    print("sim_matrix", sim_matrix)
    sim_matrix_cp = sim_matrix.copy()
    match_pairs = []
    while sim_matrix.sum() > 0.001:
        arg_max = np.argmax(sim_matrix)
        print("arg_max before unravel_index", arg_max)
        arg_max = np.unravel_index(arg_max, sim_matrix.shape)
        evt1 = sc_tree1[arg_max[0]]
        evt2 = sc_tree2[arg_max[1]]
        row_ind = row_inds[arg_max[0]][arg_max[1]]
        col_ind = col_inds[arg_max[0]][arg_max[1]]
        weights = weights_set[arg_max[0]][arg_max[1]]
        match_words = []
        for id in range(min(len(row_ind), len(col_ind))):
            match_words.append((evt1[row_ind[id]], evt2[col_ind[id]], weights[row_ind[id], col_ind[id]]))
        print("arg_max", arg_max)
        print("match_words", match_words)
        plt_matches(match_words, row_ind, col_ind, weights)
        sim_matrix[arg_max[0], :] = 0
        sim_matrix[:, arg_max[1]] = 0

    return


def semantic_match(sc_tree1, sc_tree2, static_embeds):
    # test_in_similarity_matrix(sc_tree1, static_embeds)
    # test_in_similarity_matrix(sc_tree2, static_embeds)

    sim_matrix1 = np.zeros((len(sc_tree1), len(sc_tree2)))
    sim_matrix2 = np.zeros((len(sc_tree1), len(sc_tree2)))
    jaccard_indexs = np.zeros((len(sc_tree1), len(sc_tree2)))
    row_inds1 = [[[] for evt2 in enumerate(sc_tree2)] for evt1 in enumerate(sc_tree1)]
    col_inds1 = [[[] for evt2 in enumerate(sc_tree2)] for evt1 in enumerate(sc_tree1)]
    row_inds2 = [[[] for evt2 in enumerate(sc_tree2)] for evt1 in enumerate(sc_tree1)]
    col_inds2 = [[[] for evt2 in enumerate(sc_tree2)] for evt1 in enumerate(sc_tree1)]
    weights_set1 = [[None for evt2 in enumerate(sc_tree2)] for evt1 in enumerate(sc_tree1)]
    weights_set2 = [[None for evt2 in enumerate(sc_tree2)] for evt1 in enumerate(sc_tree1)]
    for m, evt1 in enumerate(sc_tree1):
        for n, evt2 in enumerate(sc_tree2):
            mean, weights, row_ind, col_ind =\
                eva_polysim_linear_assignment(evt1, evt2, static_embeds, rte_match=True)
            jaccard_index = eva_polysim_jaccard_index(evt1, evt2)
            sim_matrix1[m, n] = mean
            jaccard_indexs[m, n] = jaccard_index
            row_inds1[m][n] = row_ind
            col_inds1[m][n] = col_ind
            weights_set1[m][n] = weights

            # print("evt1", evt1)
            # print("evt2", evt2)
            # # plt_weights(evt1, evt2, weights)
            #
            # print("row_ind", row_ind)
            # print("col_ind", col_ind)
            # match_words = []
            # for id in range(min(len(row_ind), len(col_ind))):
            #     # print
            #     match_words.append((evt1[row_ind[id]], evt2[col_ind[id]], weights[row_ind[id], col_ind[id]]))
            # print("match_words", match_words)
            # print("mean", mean)
            # print("weights", weights[row_ind, col_ind])

            mean, weights, row_ind, col_ind = \
                eva_polysim_greedy_assignment(evt1, evt2, static_embeds, rte_match=True)
            sim_matrix2[m, n] = mean
            row_inds2[m][n] = row_ind
            col_inds2[m][n] = col_ind
            weights_set2[m][n] = weights
            # plt_greedy_assignment_weight_matrix(evt1, evt2, row_ind, col_ind, static_embeds)
            
            # print("row_ind", row_ind)
            # print("col_ind", col_ind)
            # match_words = []
            # for id in range(min(len(row_ind), len(col_ind))):
            #     # print
            #     match_words.append((evt1[row_ind[id]], evt2[col_ind[id]], weights[row_ind[id], col_ind[id]]))
            # print("greedy match_words", match_words)
            # print("greedy mean", mean)

    select_multilingual_match(sc_tree1, sc_tree2, row_inds2, col_inds2, weights_set2, sim_matrix2)


def embedding_mean(static_embeddings):
    index_voc, voc_index, embeddings = static_embeddings
    special_tokens = ["[PAD]", "[CLS]", "[SEP]", "[UNK]", "[MASK]"]
    embed_set = []
    for word, id in voc_index.items():
        if word in special_tokens:
            # print("continue ", word)
            continue
        embed_set.append(embeddings[id])
    embed_set = np.array(embed_set)
    mean_embedding = np.mean(embed_set, axis=0)

    embed_sims = []
    for word, id in voc_index.items():
        if word in special_tokens:
            continue
        embed_sims.append(utils.cosine_similarity(mean_embedding, embeddings[id]))
    print(" embed_sims avg ", np.mean(embed_sims))
    return mean_embedding


def eva_mdiff_sim(m_diff, s_embeddings, t_membedding, strike=0):
    index_voc, voc_index, embeddings = s_embeddings
    special_tokens = ["[PAD]", "[CLS]", "[SEP]", "[UNK]", "[MASK]"]
    embed_sims = []
    for word, id in voc_index.items():
        if word in special_tokens:
            continue
        embed_sims.append(utils.cosine_similarity(t_membedding, embeddings[id]+strike*m_diff))
    print("eva_mdiff_sim embed_sims strike ", strike, " avg ", np.mean(embed_sims))


def cross_lingual_embedding_mean_sim(embeddings1, embeddings2):
    m1 = embedding_mean(embeddings1)
    m2 = embedding_mean(embeddings2)
    mean_sim = utils.cosine_similarity(m1, m2)
    m_diff = m1 - m2
    eva_mdiff_sim(m_diff, embeddings2, m1, strike=0)
    eva_mdiff_sim(m_diff, embeddings2, m1, strike=1)

    m_diff = m2 - m1
    eva_mdiff_sim(m_diff, embeddings1, m2, strike=0)
    eva_mdiff_sim(m_diff, embeddings1, m2, strike=1)
    print("mean_sim ", mean_sim)
    return mean_sim






