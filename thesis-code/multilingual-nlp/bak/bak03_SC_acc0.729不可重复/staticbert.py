import torch
import utils
from tqdm import tqdm
from transformers import BertTokenizer
from transformers import BertModel


class Dataset(torch.utils.data.Dataset):
    def __init__(self, voc_data):
        data = []
        for key in voc_data:
            data.append(key)
        self.dataset = data

    def __len__(self):
        return len(self.dataset)

    def __getitem__(self, i):
        text = self.dataset[i]
        return text


class Model(torch.nn.Module):
    def __init__(self, bert_model):
        super().__init__()
        self.bert_model = bert_model

    def forward(self, input_ids, attention_mask, token_type_ids):
        with torch.no_grad():
            out = self.bert_model(input_ids=input_ids,
                                  attention_mask=attention_mask,
                                  token_type_ids=token_type_ids)
        out = out.last_hidden_state.numpy()
        return out


def bert_static_embedding(language, prbert_model, save_path=None):
    token = BertTokenizer.from_pretrained(prbert_model)
    vocab = token.get_vocab()
    dataset = Dataset(vocab)

    def collate_fn(data_raw):
        data = token.batch_encode_plus(batch_text_or_text_pairs=data_raw,
                                       truncation=True,
                                       padding='max_length',
                                       max_length=3,
                                       return_tensors='pt',
                                       return_length=True)
        input_ids = data['input_ids']
        attention_mask = data['attention_mask']
        token_type_ids = data['token_type_ids']
        return input_ids, attention_mask, token_type_ids

    loader = torch.utils.data.DataLoader(dataset=dataset,
                                         batch_size=16,
                                         collate_fn=collate_fn,
                                         shuffle=False,
                                         drop_last=True)
    p_model = BertModel.from_pretrained(prbert_model)

    for param in p_model.parameters():
        param.requires_grad_(False)

    model = Model(bert_model=p_model)

    index_voc = {}
    voc_index = {}
    embed = {}
    for i, (input_ids, attention_mask, token_type_ids) in enumerate(loader):
        if i % 50 == 0:
            print("loop i ", i)
        out = model(input_ids=input_ids,
                    attention_mask=attention_mask,
                    token_type_ids=token_type_ids)
        for j, ids in enumerate(input_ids):
            word = token.decode([ids[1]])
            idsnp = ids.numpy()
            id = idsnp[1]
            embed[id] = out[j][1]
            index_voc[id] = word
            voc_index[word] = id

    static_embed = (index_voc, voc_index, embed)
    if save_path is not None:
        utils.save_to_disk(save_path, static_embed)
    else:
        utils.save_to_disk(language + "_static_embed.pkl", static_embed)

    return static_embed


if __name__ == '__main__':
    # bert_static_embedding(language='swi',
    #                       prbert_model='KB/bert-base-swedish-cased',
    #                       save_path=None)
    # bert_static_embedding(language='de',
    #                       prbert_model='bert-base-german-cased',
    #                       save_path='./corpus/de/static_embed.pkl')

    # bert_static_embedding(language='en',
    #                       prbert_model='bert-base-uncased',
    #                       save_path='./corpus/en/static_embed.pkl')

    BERT_MODEL = 'bert-base-multilingual-cased'
    bert_static_embedding(language='multi',
                          prbert_model=BERT_MODEL,
                          save_path='./corpus/multi/static_embed.pkl')


