import rundata as rd
import numpy as np
import semantic_tree as st
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import seaborn as sns
import utils

plt.rcParams.update({
    "text.usetex": True,
    "font.family": "sans-serif",
    "font.size": 22,
    'text.latex.preamble': r'\usepackage{amsmath}'
})

def scale_to_center_kmeans(X_dr, scale, kmeans, k):
    for cid in range(k):
        cidx = kmeans.labels_ == cid
        points = []
        for pid in range(cidx.shape[0]):
            if cidx[pid]:
                points.append(pid)
        # center = kmeans.cluster_centers_[cid]
        clus = [X_dr[pid, :] for pid in points]
        center = np.mean(clus, axis=0)
        for pid in points:
            point = X_dr[pid]
            newpoint = center + scale*(point-center)
            X_dr[pid] = newpoint

    return X_dr


def kmeans_example(scale_to_center=0.2, k=8):
    language = 'en'
    keyword = 'bit'
    cache_path = './data/{}/words/{}/'.format(language, keyword)
    rte = rd.check_cache(cache_path)
    cloud1, stat_emb1, cloud2, stat_emb2 = rte
    X = cloud2
    X = np.array(X)
    nm = np.sqrt((X ** 2).sum(axis=1))[:, None]
    X = X / nm

    # k = st.find_k_elbow_rule(X, K_max=10, eva=3)
    # k=8
    print("optimal k = ", k)
    kmeans = KMeans(n_clusters=k).fit(X)
    kmeans.cluster_centers_

    pca = PCA(n_components=2)
    pca = pca.fit(X)
    X_dr = pca.transform(X)
    X_dr = scale_to_center_kmeans(X_dr, scale_to_center, kmeans, k)

    # token id for meaning 'data unit' of word bit.
    data_bit_pid = [27, 29, 418, 209, 284, 305, 586, 240, 288, 430, 599, 501, 350, 389, 101, 177, 600]
    data_bit_idx = np.ones(X.shape[0], dtype='bool')
    for dbid in data_bit_pid:
        data_bit_idx[dbid] = False
    colors = sns.color_palette()

    plt.figure(figsize=(10, 8))
    for cid in range(k):
        cidx = kmeans.labels_ == cid
        cpoints = X_dr[np.logical_and(cidx, data_bit_idx)]
        print(cpoints)
        print(cidx)
        plt.scatter(cpoints[:, 0], cpoints[:, 1], alpha=1, c=colors[cid], s=30)

    for dbid in data_bit_pid:
        color = colors[kmeans.labels_[dbid]]
        plt.scatter(X_dr[dbid, 0], X_dr[dbid, 1], alpha=1, c=color, s=180, marker='+', label="bit - \'data unit\'")

    plt.title("$\mathcal{C}_w^{t}$ (w = Bit)")
    # plt.legend()
    plt.xlabel("$k$-means ($k$ = {})".format(k), size=30)
    plt.show()


def kmeans_example_timeindepedent():
    language = 'en'
    keyword = 'bit'
    cache_path = './data/{}/words/{}/'.format(language, keyword)
    rte = rd.check_cache(cache_path)
    cloud1, stat_emb1, cloud2, stat_emb2 = rte
    X = np.concatenate((cloud2, cloud1), axis=0)
    X = np.array(X)
    nm = np.sqrt((X ** 2).sum(axis=1))[:, None]
    X = X / nm

    k = st.find_k_elbow_rule(X, K_max=10, eva=3)
    k=8
    print("optimal k = ", k)
    kmeans = KMeans(n_clusters=k).fit(X)
    kmeans.cluster_centers_

    pca = PCA(n_components=2)
    pca = pca.fit(X)
    X_dr = pca.transform(X)

    data_bit_pid = [27, 29, 418, 209, 284, 305, 586, 240, 288, 430, 599, 501, 350, 389, 101, 177, 600]
    data_bit_idx = np.ones(X.shape[0], dtype='bool')
    for dbid in data_bit_pid:
        data_bit_idx[dbid] = False
    colors = sns.color_palette()

    plt.figure(figsize=(10, 8))
    for cid in range(k):
        cidx = kmeans.labels_ == cid
        cpoints = X_dr[np.logical_and(cidx, data_bit_idx)]
        print(cpoints)
        print(cidx)
        plt.scatter(cpoints[:, 0], cpoints[:, 1], alpha=1, c=colors[cid], s=30)

    for dbid in data_bit_pid:
        color = colors[kmeans.labels_[dbid]]
        plt.scatter(X_dr[dbid, 0], X_dr[dbid, 1], alpha=1, c=color, s=180, marker='+', label="bit - \'data unit\'")
    
    
    plt.title("k = {}".format(k))
    # plt.legend()
    plt.show()


def cloudprior_example(scale_to_center=0.3):
    language = 'en'
    keyword = 'bit'
    cache_path = './data/{}/words/{}/'.format(language, keyword)
    rte = rd.check_cache(cache_path)
    cloud1, stat_emb1, cloud2, stat_emb2 = rte
    X = cloud2
    X = np.array(X)
    nm = np.sqrt((X ** 2).sum(axis=1))[:, None]
    X = X / nm
    pca = PCA(n_components=2)
    pca = pca.fit(X)
    X_dr = pca.transform(X)
    data_bit_pid = [27, 29, 418, 209, 284, 305, 586, 240, 288, 430, 599, 501, 350, 389, 101, 177, 600]

    sct = 0.6
    rt = 0.69
    k = 14
    time = 2
    # hf_path = './data/{}/words/{}/cloud_sim_matrix/sct{}/rt{}/k{}_cloud_hfsc_t{}.pkl'.\
    #     format(language, keyword, sct, rt, k, time)
    # lf_path = './data/{}/words/{}/cloud_sim_matrix/sct{}/rt{}/k{}_cloud_lfsc_t{}.pkl'.\
    #     format(language, keyword, sct, rt, k, time)
    #
    # hf_semantic = utils.load_from_disk(hf_path)
    # # lf_semantic = utils.load_from_disk(lf_path)
    # colors = sns.color_palette(n_colors=len(hf_semantic))
    # plt.figure(figsize=(10, 8))
    # for colorid, clu in enumerate(hf_semantic):
    #     for pid in clu.points:
    #         color = colors[colorid]
    #         if pid in data_bit_pid:
    #             plt.scatter(X_dr[pid, 0], X_dr[pid, 1], alpha=1, c=color, s=28, marker='x',
    #                         label="bit - \'data unit\'")
    #             continue
    #         plt.scatter(X_dr[pid, 0], X_dr[pid, 1], alpha=0.7, color=color, s=8)
    # plt.title("The second clustering stage, clusters number = {}".format(len(hf_semantic)))
    # plt.show()

    rt_clusters_pth = './data/{}/words/{}/cloud_sim_matrix/rt{}/k{}_rt_clusters_t{}.pkl'. \
        format(language, keyword, rt, k, time)
    clusters = utils.load_from_disk(rt_clusters_pth)
    scale_to_center_cloudprior(X_dr, scale_to_center, clusters)
    colors = sns.color_palette(n_colors=len(clusters))
    plt.figure(figsize=(10, 8))
    for colorid, clu in enumerate(clusters):
        for pid in clu.points:
            color = colors[colorid]
            if pid in data_bit_pid:
                plt.scatter(X_dr[pid, 0], X_dr[pid, 1], alpha=1, c=color, s=180, marker='+',
                            label="bit - \'data unit\'")
                continue
            plt.scatter(X_dr[pid, 0], X_dr[pid, 1], alpha=1, color=color, s=30)
#    plt.title("The first clustering stage, clusters number = {}".format(len(clusters)))
    plt.title("$\mathcal{C}_w^{t}$ (w = Bit)")
    
    plt.xlabel("Our Clustering (n\_clusters = {}, 1st round)".format(k), size=30)
#    plt.legend()
    plt.show()


def plt_hf_clusters(language, keyword, cloud, sct, rt, k, time, data_bit=False, colors=None, scale_to_center=0.5):
    X = cloud
    X = np.array(X)
    nm = np.sqrt((X ** 2).sum(axis=1))[:, None]
    X = X / nm
    pca = PCA(n_components=2)
    pca = pca.fit(X)
    X_dr = pca.transform(X)
    data_bit_pid = [27, 29, 418, 209, 284, 305, 586, 240, 288, 430, 599, 501, 350, 389, 101, 177, 600]

    hf_path = './data/{}/words/{}/cloud_sim_matrix/sct{}/rt{}/k{}_cloud_hfsc_t{}.pkl'. \
        format(language, keyword, sct, rt, k, time)

    hf_semantic = utils.load_from_disk(hf_path)
    scale_to_center_cloudprior(X_dr, scale_to_center, hf_semantic)
    if colors is None:
        colors = sns.color_palette(n_colors=len(hf_semantic))
    plt.figure(figsize=(10, 8))
    for colorid, clu in enumerate(hf_semantic):
        for pid in clu.points:
            color = colors[colorid]
            if data_bit and pid in data_bit_pid:
                plt.scatter(X_dr[pid, 0], X_dr[pid, 1], alpha=1, c=color, s=180, marker='+',
                            label="bit - \'data unit\'")
                continue
            plt.scatter(X_dr[pid, 0], X_dr[pid, 1], alpha=1, color=color, s=30)
#    plt.title("Time period {} clustering result, clusters number = {}".format(time, len(hf_semantic)))
    
    if time == 1:
        plt.title("$\mathcal{C}_w^{t-1}$ (w = Bit)")
    if time == 2:
        plt.title("$\mathcal{C}_w^{t}$ (w = Bit)")
#    plt.title("``Bit'' at time ${}$".format(tlabel))
    
#    plt.xlim([-0.1, 0.1])
    
    plt.xlabel("Our Clustering (n\_clusters = {}, 2nd round)".format(len(hf_semantic)), size=30)
    
    plt.show()


def scale_to_center_cloudprior(X_dr, scale, clusters, pgap=0):
    for clu in clusters:
        clus = [X_dr[pid, :] for pid in clu.points]
        center = np.mean(clus, axis=0)
        for pid in clu.points:
            pid = pid+pgap
            point = X_dr[pid,:]
            newpoint = center + scale * (point - center)
            X_dr[pid] = newpoint

    return X_dr


def split_cnetroid(X_dr, hf_semantic1, hf_semantic2, cloud1, cloud2):
    # for clu in hf_semantic1:
    #     clus = [X_dr[pid, :] for pid in clu.points]
    #     center = np.mean(clus, axis=0)
    #     plt.scatter(center[0], center[1], alpha=1, color='r', s=50, marker='*')
    
    # for clu in hf_semantic2:
    #     clus = [X_dr[pid, :] for pid in clu.points]
    #     center = np.mean(clus, axis=0)
    #     plt.scatter(center[0], center[1], alpha=1, color='r', s=50, marker='*')
    
    shift_v = [0.18, 0.06]
    pgap = len(cloud2)
    for clu in hf_semantic1:
        for pid in clu.points:
            pid += pgap
            X_dr[pid] = X_dr[pid] + shift_v

    shift_v = [[-0.02, -0.08], [0, -0.05], ]
    pgap = 0 
    for i, clu in enumerate(hf_semantic2):
        for pid in clu.points:
            pid += pgap
            X_dr[pid] = X_dr[pid] + shift_v[i]


def cloudprior_example_timeindepedent(scale_to_center=0.5, split_c=False):
    language = 'en'
    keyword = 'bit'
    cache_path = './data/{}/words/{}/'.format(language, keyword)
    rte = rd.check_cache(cache_path)
    cloud1, stat_emb1, cloud2, stat_emb2 = rte
    sct = 0.6
    rt = 0.69
    k = 14

    X = np.concatenate((cloud2, cloud1), axis=0)
    X = np.array(X)
    nm = np.sqrt((X ** 2).sum(axis=1))[:, None]
    X = X / nm
    pca = PCA(n_components=2)
    pca = pca.fit(X)
    X_dr = pca.transform(X)

    data_bit_pid = [27, 29, 418, 209, 284, 305, 586, 240, 288, 430, 599, 501, 350, 389, 101, 177, 600]

    hf_path1 = './data/{}/words/{}/cloud_sim_matrix/sct{}/rt{}/k{}_cloud_hfsc_t{}.pkl'. \
        format(language, keyword, sct, rt, k, 1)
    hf_semantic1 = utils.load_from_disk(hf_path1)
    hf_path2 = './data/{}/words/{}/cloud_sim_matrix/sct{}/rt{}/k{}_cloud_hfsc_t{}.pkl'. \
        format(language, keyword, sct, rt, k, 2)
    hf_semantic2 = utils.load_from_disk(hf_path2)

    scale_to_center_cloudprior(X_dr, scale_to_center, hf_semantic1, pgap=len(cloud2))
    scale_to_center_cloudprior(X_dr, scale_to_center, hf_semantic2)

    colors = sns.color_palette(n_colors=len(hf_semantic1)+len(hf_semantic2))
#    colors = sns.color_palette()
    plt.figure(figsize=(10, 8))
    if split_c:
        split_cnetroid(X_dr, hf_semantic1, hf_semantic2, cloud1, cloud2)

    for colorid, clu in enumerate(hf_semantic2):
        for pid in clu.points:
            color = colors[colorid]
            if pid in data_bit_pid:
                plt.scatter(X_dr[pid, 0], X_dr[pid, 1], alpha=1, c=color, s=180, marker='+',
                            label="bit - \'data unit\'")
                continue
            plt.scatter(X_dr[pid, 0], X_dr[pid, 1], alpha=1, color=color, s=30)

    for colorid, clu in enumerate(hf_semantic1):
        colorid = colorid + len(hf_semantic2)
        for pid in clu.points:
            pid = pid + len(cloud2)
            color = colors[colorid]
            plt.scatter(X_dr[pid, 0], X_dr[pid, 1], alpha=1, color=color, s=30)

#    plt.title("Two time period clustering result, clusters number = {}".format(len(hf_semantic1)+len(hf_semantic2)))
    
    plt.title("$\mathcal{C}_w^{t-1} \cup \mathcal{C}_w^{t}$ (w = Bit)")
    
    plt.xlabel("Our Clustering (n\_clusters = {}, 2nd round)".format(len(hf_semantic1)+len(hf_semantic2)), size=30)
    datapath = './data/ablation/bit/'
    plt.savefig(datapath+'cloud.pdf')
    plt.savefig(datapath+'cloud.png')
    plt.show()

#    plt_hf_clusters(language, keyword, cloud1, sct, rt, k, time=1,
#                    colors=colors[len(hf_semantic2):], scale_to_center=scale_to_center)
#    plt_hf_clusters(language, keyword, cloud2, sct, rt, k, time=2,
#                    data_bit=True, colors=colors, scale_to_center=scale_to_center)





# test 1
#kmeans_example(scale_to_center=0.5, k=8)

# test 2
#cloudprior_example(scale_to_center=0.4)

# test 3
cloudprior_example_timeindepedent(scale_to_center=0.35, split_c=True)
# cloudprior_example_timeindepedent(scale_to_center=0.05)


# discard test
# kmeans_example_timeindepedent()