import utils
import numpy as np
import semantic_tree as st
import bertcloud as bc
import treesim as ts


def check_cache(cache_path):
    files = utils.find_files(cache_path)
    if len(files) != 4:
        return None
    cloud1 = utils.load_from_disk(cache_path+'cloud1')
    cloud2 = utils.load_from_disk(cache_path + 'cloud2')
    stat_emb1 = utils.load_from_disk(cache_path + 'stat_emb1')
    stat_emb2 = utils.load_from_disk(cache_path + 'stat_emb2')
    return cloud1, stat_emb1, cloud2, stat_emb2


def save_cache(cloud1, stat_emb1, cloud2, stat_emb2, cache_path):
    utils.save_to_disk(cache_path + 'cloud1', cloud1)
    utils.save_to_disk(cache_path + 'stat_emb1', stat_emb1)
    utils.save_to_disk(cache_path + 'cloud2', cloud2)
    utils.save_to_disk(cache_path + 'stat_emb2', stat_emb2)


def init_static_embedding():
    index_voc = {}
    voc_index = {}
    embeddings = {}
    static_embeds = (index_voc, voc_index, embeddings)
    return static_embeds


def gen_run_data(keywords, pmodel, corpus1, corpus2, language, static_embedding=None, separate=False):
    data1 = utils.load_from_disk(corpus1)
    data2 = utils.load_from_disk(corpus2)
    for keyword in keywords:
        sendata1 = data1[keyword]
        sendata2 = data2[keyword]
        # res_file = open("sendata1.txt", 'w', encoding='utf-8')
        # res_file.write('\n'.join(sendata1))
        # res_file.close()
        # res_file = open("sendata2.txt", 'w', encoding='utf-8')
        # res_file.write('\n'.join(sendata2))
        # res_file.close()

        if len(sendata1) < 40 or len(sendata2) < 40:
            continue
        print("Exec semantic tree for word ", keyword)
        cache_path = './data/{}/words/{}/'.format(language, keyword)
        rte = check_cache(cache_path)
        if rte is None:
            cloud1, stat_emb1 = bc.bert_cloud(keyword=keyword, sendata=sendata1, prbert_model=pmodel)
            cloud2, stat_emb2 = bc.bert_cloud(keyword=keyword, sendata=sendata2, prbert_model=pmodel)
            save_cache(cloud1, stat_emb1, cloud2, stat_emb2, cache_path)
        else:
            cloud1, stat_emb1, cloud2, stat_emb2 = rte

        X = np.array(cloud1)
        nm = np.sqrt((X ** 2).sum(axis=1))[:, None]
        cloud1 = X / nm
        X = np.array(cloud2)
        nm = np.sqrt((X ** 2).sum(axis=1))[:, None]
        cloud2 = X / nm

        if not separate:
            static_embeds = init_static_embedding()
            if static_embedding is not None:
                static_embeds = utils.load_from_disk(static_embedding)
            static_embeds = st.update_static_embeds(static_embeds, stat_emb1)
            static_embeds = st.update_static_embeds(static_embeds, stat_emb2)
            yield keyword, cloud1, cloud2, static_embeds
        else:
            static_embeds1 = init_static_embedding()
            static_embeds2 = init_static_embedding()
            static_embeds = init_static_embedding()
            s_embeds1 = st.update_static_embeds(static_embeds1, stat_emb1)
            s_embeds2 = st.update_static_embeds(static_embeds2, stat_emb2)
            static_embeds = st.update_static_embeds(static_embeds, stat_emb1)
            static_embeds = st.update_static_embeds(static_embeds, stat_emb2)
            yield keyword, cloud1, cloud2, s_embeds1, s_embeds2, static_embeds


def get_evolution_set(run_data, simsize=12, separate=True):
    if separate:
        keyword, cloud1, cloud2, static_embeds1, static_embeds2, static_embeds = run_data
        # poly_embeds1 = st.get_polysemy(keyword, cloud1, static_embeds1, check_simsize=simsize)
        # poly_embeds2 = st.get_polysemy(keyword, cloud2, static_embeds2, check_simsize=simsize)
        poly_embeds1 = ts.get_polysemy_embedding(keyword, cloud1, static_embeds1, check_simsize=simsize)
        poly_embeds2 = ts.get_polysemy_embedding(keyword, cloud2, static_embeds2, check_simsize=simsize)
        evolve1 = ts.get_most_sim_words(keyword, poly_embeds1, simsize, static_embeds1)
        evolve2 = ts.get_most_sim_words(keyword, poly_embeds2, simsize, static_embeds2)
        return keyword, static_embeds, evolve1, evolve2
    else:
        keyword, cloud1, cloud2, static_embeds = run_data
        poly_embeds1 = st.get_polysemy(keyword, cloud1, static_embeds, check_simsize=simsize)
        poly_embeds2 = st.get_polysemy(keyword, cloud2, static_embeds, check_simsize=simsize)
        evolve1 = ts.get_most_sim_words(keyword, poly_embeds1, simsize, static_embeds)
        evolve2 = ts.get_most_sim_words(keyword, poly_embeds2, simsize, static_embeds)
        return keyword, static_embeds, evolve1, evolve2


def get_evolution(keywords, pmodel, corpus1, corpus2, language, static_embedding=None, simsize=12, separate=False):
    f = gen_run_data(keywords, pmodel, corpus1, corpus2, language, static_embedding=static_embedding, separate=separate)
    evolves = {}
    for run_data in f:
        keyword, static_embeds, evolve1, evolve2 = \
            get_evolution_set(run_data, simsize=simsize, separate=separate)
        evolves[keyword] = {"static_emb": static_embeds, "psim_words": (evolve1, evolve2)}
        # evolves[keyword] = {"psim_words": (evolve1, evolve2)}
    return evolves


def plt_evolve_trees(keywords, pmodel, corpus1, corpus2, language, static_embedding=None, simsize=8):
    f = gen_run_data(keywords, pmodel, corpus1, corpus2, language, static_embedding=static_embedding)
    for run_data in f:
        keyword, cloud1, cloud2, static_embeds = run_data
        title = 'Semantic evolve tree for English word ' + keyword
        st.plt_evolve_tree(keyword, cloud1, cloud2, static_embeds, simsize=simsize, title=title)


def evolve_trees_en_semeval(will_plt=False):
    keywords = ['bit',]
    pmodel = 'bert-base-multilingual-cased'  # 'bert-base-uncased'
    corpus1 = './corpus/en/semevalSC_r/data_ccoha1'
    corpus2 = './corpus/en/semevalSC_r/data_ccoha2'
    language = 'en'
    static_embedding = './data/en/static_embed.pkl'  # './data/multi/static_embed.pkl'
    evolves = get_evolution(keywords, pmodel, corpus1, corpus2,
                            language, static_embedding=None, simsize=12)
    for kw in evolves:
        print(evolves[kw]["psim_words"])
    # utils.save_to_disk('./en_evolve_set', evolves)
    if will_plt:
        plt_evolve_trees(keywords, pmodel, corpus1, corpus2, language, static_embedding=None, simsize=8)


def semeval_evolution(will_plt=False):
    # keywords = utils.load_from_disk('./corpus/en/semevalSC_r/keywords')
    keywords = ["bit",]
    print(keywords)
    # keywords = ["attack", "plane"]
    # keywords = ["attack", "plane", "edge", "stab", "tip", "thump"]
    pmodel = 'bert-base-multilingual-cased'  # 'bert-base-uncased'
    corpus1 = './corpus/en/semevalSC_r/data_ccoha1'
    corpus2 = './corpus/en/semevalSC_r/data_ccoha2'
    language = 'en'
    static_embedding = './data/en/static_embed.pkl'  # './data/multi/static_embed.pkl'
    evolves = get_evolution(keywords, pmodel, corpus1, corpus2,
                            language, static_embedding=None, simsize=12, separate=True)
    print(evolves['bit']["psim_words"])
    # utils.save_to_disk('./en_semeval_evolve_set_sep', evolves)
    if will_plt:
        plt_evolve_trees(keywords, pmodel, corpus1, corpus2, language, static_embedding=None, simsize=8)


def merge_static_embedding(static_embeds, static_embeds2):
    index_voc, voc_index, embeddings = static_embeds
    idxs = [key for key in index_voc]
    voc_len = np.max(idxs) if len(idxs) > 0 else 0
    voc_len += 1
    print("voc_len start ", voc_len)
    index_voc2, voc_index2, embeddings2 = static_embeds2
    for word in voc_index2:
        if word not in voc_index:
            id = voc_index2[word]
            voc_index[word] = voc_len
            index_voc[voc_len] = word
            embeddings[voc_len] = embeddings2[id]
            voc_len += 1
    print("voc_len end ", voc_len)
    static_embeds = (index_voc, voc_index, embeddings)
    return static_embeds


def test_select_sen():
    keyword = 'bit'
    sen_file = 'select_sen.txt'
    with open(sen_file, "r", encoding="utf-8") as corpus_data:
        corpus_raw = corpus_data.read().split('\n')
    sel_sens = corpus_raw
    pmodel = 'bert-base-multilingual-cased'  # 'bert-base-uncased'
    cloud, stat_emb = bc.bert_cloud(keyword=keyword, sendata=sel_sens, prbert_model=pmodel)
    # print(cloud)
    # print(stat_emb)
    static_embeds = init_static_embedding()
    # static_embeds = st.update_static_embeds(static_embeds, stat_emb)

    # load
    evolves = utils.load_from_disk('./en_semeval_evolve_set_sep')
    # static_embeds = st.update_static_embeds(static_embeds, evolves[keyword]['static_emb'])
    static_embeds = merge_static_embedding(static_embeds, evolves[keyword]['static_emb'])
    index_voc, voc_index, embeddings = static_embeds
    for c in cloud:
        ws = utils.most_sim_words(c, 12, voc_index, embeddings, willprint=False)
        ws_w = [w[0] for w in ws]
        print(ws_w)


if __name__ == '__main__':
    # evolve_trees_en_semeval(will_plt=True)
    # test_select_sen()

    semeval_evolution(will_plt=False)



