import numpy as np
import utils
import rundata as rd
import semantic_tree as st
import semeval_task2 as st2
import cloudprior as cp
from scipy.spatial import distance
from cloudprior import CloudCluster, get_cloud_data, neighbor_similarity
from semeval import evaluate_semeval_task1



def stat_nbs_semantic_sim(k, ek, language='en'):
    keywords_file = './corpus/{}/semeval_task/keywords.pkl'.format(language)
    keywords = utils.load_from_disk(keywords_file)
    sims = {}
    for keyword in keywords:
        cache_path = './data/{}/words/{}/'.format(language, keyword)
        stat_emb1 = utils.load_from_disk(cache_path + 'stat_emb1')
        stat_emb2 = utils.load_from_disk(cache_path + 'stat_emb2')
        static_embeds1 = rd.init_static_embedding()
        static_embeds2 = rd.init_static_embedding()
        static_embeds = rd.init_static_embedding()
        s_embeds1 = st.update_static_embeds(static_embeds1, stat_emb1)
        s_embeds2 = st.update_static_embeds(static_embeds2, stat_emb2)
        static_embeds = st.update_static_embeds(static_embeds, stat_emb1)
        static_embeds = st.update_static_embeds(static_embeds, stat_emb2)
        scores, s_mean, s_std = utils.statistic_neighbor_distance(static_embeds, k=k, p_n=12, ek=ek)
        print("scores mean {} std {} ".format(s_mean, s_std))
        # sims[keyword] = (s_mean, s_std, scores)
        sims[keyword] = {'s_mean':s_mean, 's_std':s_std, 'scores':scores}
    
    return sims
                    


if __name__ == '__main__':
    language='en'
    k=14
    # eks = [1,2,3]
    eks = [1,3, k]
    for ek in eks:
        cache_p = './data/t_policy/{}/k{}/ek{}/sims_stat.pkl'.format(language,k,ek)
        sims_stat = utils.load_from_disk(cache_p)
        if sims_stat is None:
            sims_stat = stat_nbs_semantic_sim(k=k, ek=ek, language=language)
            utils.save_to_disk(cache_p, sims_stat)
        s_means = []
        s_scores = []
        for w in sims_stat:
            print("w {} scores mean {} std {} ".format(w, sims_stat[w]['s_mean'], sims_stat[w]['s_std']))
            s_means.append(sims_stat[w]['s_mean'])
            s_scores += sims_stat[w]['scores']
        s_scores = np.array(s_scores)
        s_means = np.array(s_means)
        print("s_scores mean {} std {} ".format(np.mean(s_scores), np.std(s_scores)))
        print("s_means mean {} std {} ".format(np.mean(s_means), np.std(s_means)))
        

