imageio==2.33.1
ipykernel==6.29.2
ipython==8.15.0
ipywidgets==8.1.1
jupyter==1.0.0
jupyter-console==6.6.3
jupyter-events==0.9.0
jupyter-lsp==2.2.2
jupyter_client==8.6.0
jupyter_core==5.3.1
jupyter_server==2.12.5
jupyter_server_terminals==0.5.2
jupyterlab==4.1.1
jupyterlab-widgets==3.0.9
jupyterlab_pygments==0.3.0
jupyterlab_server==2.25.3
latex==0.7.0
multiprocess==0.70.14
nltk==3.8.1
numpy==1.22.0
pylint==2.13.9
scikit-image==0.22.0
scikit-learn==1.3.0
scipy==1.8.1
seaborn==0.12.2
sentence-splitter==1.4
simplemma==0.9.1
tensorboard==2.15.1
tensorboard-data-server==0.7.2
torch==2.0.1
torchaudio==2.0.2
torchvision==0.15.2
tqdm==4.66.1