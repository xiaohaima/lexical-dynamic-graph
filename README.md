# Lexical Semantic Dynamic Graph - Lexical Semantic Change Analysis Tool


This package is designed to detect semantic changes within language and across languages from text corpora.

**_This project is currently under development!_**
 
## Instruction

The notebook `semantic_dynamic.ipynb` shows the code for constructing the semantic dynamic graph of a specific target word.