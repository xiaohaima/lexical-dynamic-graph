import matplotlib.pyplot as plt
import numpy as np
from sklearn.decomposition import PCA
import utils
from sklearn.cluster import KMeans
from sensecluster import CloudCluster
import seaborn as sns

def get_subsemantic_clouds(hf_clu, cloud, size=10):
    X = []
    sizes = []
    for clu in hf_clu:
        clu_cloud = []
        nbs_size = len(clu.points) if len(clu.points) < size else size
        for i in range(nbs_size):
            clu_cloud.append(cloud[clu.points[i]])
        X += clu_cloud
        sizes.append(nbs_size)
    pca = PCA(n_components=2)
    pca = pca.fit(X)
    X_dr = pca.transform(X)
    subsemantic_clouds = []
    startid = 0
    for sz in sizes:
        subsemantic_clouds.append(X_dr[startid:startid + sz])
        startid = startid + sz

    return subsemantic_clouds, X_dr

def prepare_sense_graph(hf_clu, cloud, semantic_change, clu_size=10, k=2):
    pca = PCA(n_components=2)
    pca = pca.fit(cloud)
    cloud_dr = pca.transform(cloud)
    cloud_ = cloud_dr
    subsemantic_clouds, X_dr = get_subsemantic_clouds(hf_clu, cloud, size=clu_size)

    c_ = np.mean(X_dr, axis=0)
    subse_cloud_ = subsemantic_clouds
    subse_ = [np.mean(clu_cloud_, axis=0) for clu_cloud_ in subsemantic_clouds]
    subse_nbs_ = []
    for clu_cloud_ in subsemantic_clouds:
        kmeans = KMeans(n_clusters=k).fit(clu_cloud_)
        subse_nbs_.append(kmeans.cluster_centers_)
    subse_nbs = []
    for clu in hf_clu:
        nbs = clu.c_neighbors[0:k]
        subse_nbs.append(nbs)

    stree = SenseGraph(c_, subse_, subse_nbs_, subse_cloud_, X_dr, cloud_, subse_nbs, k, clu_size, semantic_change)
    return stree


class SenseGraph:
    def __init__(self, c_, subse_:list, subse_nbs_:list, subse_cloud_:list,
                 se_cloud_, cloud_, subse_nbs:list, k, clu_size, semantic_change):
        self.c_ = c_
        self.subse_ = subse_
        self.subse_nbs_ = subse_nbs_
        self.subse_cloud_ = subse_cloud_
        self.se_cloud_ = se_cloud_
        self.cloud_ = cloud_
        self.subse_nbs = subse_nbs
        self.k = k
        self.clu_size = clu_size
        self.semantic_change = semantic_change

def max_clu_line_len(subse_nbs_, clu_center):
    max_l = 0
    for sub_nbs_c_ in subse_nbs_:
        for i in range(sub_nbs_c_.shape[0]):
            point = sub_nbs_c_[i, :]
            l = np.linalg.norm(point - clu_center)
            max_l = l if max_l < l else max_l

    return max_l

def nbs_point_position_assign(point, clu_center, max_l, X, Y, c_):
    # Y horizontal
    lim_max = np.max(Y)
    lim_min = np.min(Y)
    # # X vertical
    # lim_max_ = np.max(X)
    # lim_min_ = np.min(X)
    new_point = np.array([point[0], point[1]])
    while np.linalg.norm(new_point - clu_center) < max_l/6:
        tmp_point = 2 * (new_point - clu_center) + clu_center
        new_point = tmp_point

    return new_point


def plot_semantic_tree(stree, t, ax3, X, Y, word, bg_alpha=0.1, labeled=False):
    def Z(X, Y):
        return X * 0 + Y * 0 + t

    ax3.plot_surface(X=Z(X, Y), Y=Y, Z=X, rstride=10, cstride=10, antialiased=True, color=(0.1, 0.2, 0.5, bg_alpha))

    # draw center
    c_ = stree.c_
    if labeled:
        ax3.scatter(t, c_[0], c_[1], s=80, c='black', alpha=0.7, marker='s', facecolors='None')
    else:
        ax3.scatter(t, c_[0], c_[1], s=80, c='black', alpha=0.7, marker='s', label='centroid of $\mathcal{C}_w$', facecolors='None')
    ax3.text(t, c_[0]-0.02, c_[1] + 0.03, s=word, c='black', alpha=0.7)

    for cid, clu_center in enumerate(stree.subse_):
        if labeled:
            ax3.scatter(t, clu_center[0], clu_center[1], c='black', alpha=0.7, s=80, marker='o')
        else:
            ax3.scatter(t, clu_center[0], clu_center[1], c='black', alpha=0.7, s=80, marker='o', label='centroid of meaning group')
        sub_nbs_c_ = stree.subse_nbs_[cid]
        # plot line
        x = [t, t]
        y = [c_[0], clu_center[0]]
        z = [c_[1], clu_center[1]]
        ax3.plot3D(xs=x, ys=y, zs=z, color='grey', alpha=0.7)
        max_l = max_clu_line_len(stree.subse_nbs_, clu_center)
        for i in range(sub_nbs_c_.shape[0]):
            # for mark sc change branch
            sc_wset = []
            nb_w = stree.subse_nbs[cid][i]
            point = sub_nbs_c_[i, :]
            point = nbs_point_position_assign(point, clu_center, max_l, X, Y, c_)
            nbs_c_color = 'blue' if stree.semantic_change[cid] else 'black'
            if labeled:
                    ax3.scatter(t, point[0], point[1], alpha=0.7, color=nbs_c_color, s=80, marker='^')
            else:
                ax3.scatter(t, c_[0], c_[1], alpha=0.7, color='black', s=80, marker='^',
                                label='centroid of neighbors')
                ax3.scatter(t, point[0], point[1], alpha=0.7, color=nbs_c_color, s=80, marker='^')

            labeled = True
            if nb_w in sc_wset:
                ax3.text(t, point[0]-0.05, point[1]+0.04, s=nb_w, c='orange', alpha=0.7)
            else:
                ax3.text(t, point[0]-0.05, point[1]+0.04, s=nb_w, c='black', alpha=0.7)
            # plot line
            x = [t, t]
            y = [point[0], clu_center[0]]
            z = [point[1], clu_center[1]]
            ax3.plot3D(xs=x, ys=y, zs=z, c='grey', alpha=0.7)

def plt_sense_dynamic_graph(sense_clu1, cloud1, sense_clu2, cloud2, similarity, k, clu_size=25):
    word = 'mouse'
    change1 = np.sum(similarity.astype(np.int32), axis=1) == 0
    change2 = np.sum(similarity.astype(np.int32), axis=0) == 0
    graph1 = prepare_sense_graph(sense_clu1, cloud1, change1, clu_size=clu_size, k=k)
    graph2 = prepare_sense_graph(sense_clu2, cloud2, change2, clu_size=clu_size, k=k)
    print(graph1.subse_nbs)
    print(graph2.subse_nbs)
    # return
    cloud1 = graph1.se_cloud_
    cloud2 = graph2.se_cloud_
    cloud = np.concatenate((cloud1, cloud2), axis=0)
    # draw plane
    ax_max = np.max(cloud)
    ax_min = np.min(cloud)
    lim_max = ax_max + (ax_max - ax_min) / 16
    lim_min = ax_min - (ax_max - ax_min) / 16
    # vertical
    lim_max_x = ax_max + (ax_max - ax_min) / 8
    lim_min_x = ax_min - (ax_max - ax_min) / 32
    # horizontal
    lim_max_y = ax_max + (ax_max - ax_min) / 8
    lim_min_y = ax_min - (ax_max - ax_min) / 8

    plt.rcParams.update({
        "text.usetex": True,
        "font.family": "sans-serif",
        "font.size": 24,
        # 'text.latex.preamble': r'\usepackage{amsfonts}'
    })

    fig3 = plt.figure(figsize=(18, 18))
    ax3 = plt.subplot(projection='3d')
    ax3.set_xlim(lim_min, lim_max)
    ax3.set_ylim(lim_min, lim_max)
    ax3.set_zlim(lim_min, lim_max)
    x = np.arange(lim_min_x, lim_max_x, (ax_max - ax_min) / 50)
    y = np.arange(lim_min_y, lim_max_y, (ax_max - ax_min) / 50)
    X, Y = np.meshgrid(x, y)  # Change the coordinate vector (x, y) into the coordinate matrix (X, Y)

    t1 = lim_min + (lim_max - lim_min) / 4
    plot_semantic_tree(graph1, t1, ax3, X, Y, word, labeled=False)

    t2 = lim_min + ((lim_max - lim_min) / 4) * 4
    plot_semantic_tree(graph2, t2, ax3, X, Y, word, labeled=True)

    x = [t1, t2]
    y = [graph2.c_[0], graph1.c_[0]]
    z = [graph2.c_[1], graph1.c_[1]]
    ax3.plot3D(xs=x, ys=y, zs=z, color='black', alpha=0.9)

    ax3.text(t1, lim_min, lim_min, s='t1', fontsize=10,)
    ax3.text(t2, lim_min, lim_min, s='t2', fontsize=10,)

    ax3.grid(False)
    ax3.set_xticks([])
    ax3.set_yticks([])
    ax3.set_zticks([])
    plt.axis('off')
    ax3.view_init(elev=8, azim=-48)

    plt.legend(frameon=False, loc=(0.18, 0.15), ncol=3, columnspacing=0, handletextpad=0)
    save_path = './.cache/images/'
    plt.savefig(save_path+"mouse.jpg", bbox_inches='tight')
    plt.savefig(save_path+"mouse.pdf", bbox_inches='tight')
    plt.show()
